#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
/*functia care sterge trailing whitespace inlocuind ultimul whitespace gasit cu
\n iar urmatrul cu \0 */
void rm_whitespace_trail(char original[][1000], int original_line) {
  int i, j;
  for (i = 0; i < original_line; i++) {
    for (j = strlen(original[i]) - 1; j >= 0; j--) {
      if (isspace(original[i][j])) {
      original[i][j] = '\n';
      original[i][j + 1] = '\0';
      }
      else 
      break;
    }
  }
}

/*functia de aliniere la stanga numara cate caractere de whitespace sant la 
inceput de linie si apoi muta linia cu atatea pozitii spre stanga */
void align_left(char original[][1000], int start, int end) {
  int i, j, k = 0;
  for (i = start; i <= end; i++) {
    for (j = 0; j < strlen(original[i]); j++) {
      if (original[i][0] != '\n') {
        if (isspace(original[i][j])) {
        k++;
        }
        else 
        break;
      }
    }
    for (j = k; j <= strlen(original[i]); j++) {
      original[i][j - k] = original[i][j];
    }
    k = 0;
  }
}

/*functie de aliniere la dreapta */
void align_right(char original[][1000], int start, int end, int original_line) {
  int i, j, b = 0, max, a = 0;
/*  sterg trailing whitespace*/
  for (i = start; i <= end; i++) {
    for (j = strlen(original[i]); j >= 0; j--) {
      if (original[i][j] == ' ') {
        original[i][j] = original[i][j + 1];
      }
      if (original[i][j] != ' ' && original[i][j] != '\0' &&
       original[i][j] != '\n')
        break;
    }
    j = 0;
    do {
      if (original[i][j] == '\n') {
        original[i][j + 1] = '\0';
      }
      j++;
    } while (original[i][j] != '\n');
    j++;
    original[i][j] = '\0';
  }

/*  aflu linia de lungime maxima*/
  max = 0;
  for (i = 0; i <= original_line; i++) {
    for (j = 0; j <= strlen(original[i]); j++){
      if (original[i][j] != '\n') {
        b++;
      }
    }
    if (b > max) {
      max = b;
    }
    b = 0;
  }
/*  misca toate liniile la dreapta pana lungime ei nu devine egale cu cea maxima 
  dupa care se opreste*/
  for (i = start; i <= end; i++) {
    if (original[i][0] != '\n') {
      while (b != max) {
        a++;
        b = 0;
        for (j = 0; j <= strlen(original[i]); j++){
          if (original[i][j] != '\n') {
            b++;
          }
        }
        if (b != max) {
          for (j = strlen(original[i]) + 1; j > 0; j--){
            original[i][j] = original[i][j - 1];
          }
          original[i][a - 1] = ' ';
        }
      }
    }
    a = 0;
    b = 0;
  }
}

/*functie de centrare*/
void center(char original[][1000], int start, int end, int original_line) {
  int i, j, b = 0, max, a = 0;
/*  sterge trailing whitespace*/
  for (i = start; i <= end; i++) {
    for (j = strlen(original[i]); j >= 0; j--) {
      if (original[i][j] == ' ') {
        original[i][j] = original[i][j + 1];
      }
      if (original[i][j] != ' ' && original[i][j] != '\0' &&
        original[i][j] != '\n')
        break;
    }
    j = 0;
    do {
      if (original[i][j] == '\n') {
        original[i][j + 1] = '\0';
      }
      j++;
    } while (original[i][j] != '\n');
    j++;
    original[i][j] = '\0';
  }
/*  gaseste lungimea maxima a liniilor*/
  max = 0;
  for (i = 0; i < original_line; i++) {
    for (j = 0; j <= strlen(original[i]); j++){
      if (original[i][j] != '\n') {
        b++;
      }
    }
    if (b > max) {
      max = b;
    }
    b = 0;
  }
/*  muta spre dreapta linia pana cand numarul spatiilor la inceput nu devine 
  egal cu diferenta dintre max si lungimea liniei*/
  for (i = start; i <= end; i++) {
    if (original[i][0] != '\n') {
      while ((max - b) - a > 1) {
        a++;
        b = 0;
        for (j = 0; j <= strlen(original[i]); j++){
          if (original[i][j] != '\n') {
            b++;
          }
        }
        if (b != max) {
          for (j = strlen(original[i]) + 1; j > 0; j--){
            original[i][j] = original[i][j - 1];
          }
          original[i][a - 1] = ' ';
        }
      }
    }
    a = 0;
    b = 0;
  }
}

/*functia care adauga paragrafe*/
void paragraph(char original[][1000], int len, int start, int end) {
  int i, j, b = 0, x;
  b = len;
  x = 0;
/*  adauga len spatii la prima linie din paragraf*/
  for (i = start; i <= end; i++) {
    if ((original[i - 1][0] == '\n' && original[i][0] != '\n') ||
    (i == 0 && original[i][0] != '\n')) {
      while (b) {
        for (j = strlen(original[i]) + 1; j > x; j--) {
          original[i][j] = original[i][j - 1];
        }
        for (j = 0; j <= x; j++) {
          original[i][j] = ' ';
        }
        x++;
        b--;
      }
    }
    x = 0;
    b = len;      
  }
}

/*functia listelor*/
void ls_data(char original[][1000], char type, char spec, int start, int end) {
  int i, j, b = 0, x, nr = 0;
  char s[2];

  align_left(original, start, end);
/*  in functie de tipul listei indeplinesc diferite cerinte*/
  switch (type) {
    case 'n':
      for (i = start; i <= end; i++) {
/*        transform numarul liniei intr-un string*/
        sprintf(s, "%d", nr + 1);
        b = 3;
        if ((nr + 1) >= 10) {
          b = 4;
        }
        x = 0;    
/*        eliberez b spatii in fata tratand si cazul cand numarul e mai mare ca 
        9*/
        while (b) {
          for (j = strlen(original[i]) + 1; j > x; j--) {
            original[i][j] = original[i][j - 1];
          }
          for (j=0; j<=x; j++) {
            original[i][j] = ' ';
          }
          x++;
          b--;
        }
        if ((nr + 1) < 10) {  
          original[i][1] = spec;
          original[i][0] = s[0];
        }  
        else {
          original[i][2] = spec;
          original[i][0] = s[0];
          original[i][1] = s[1];
        }
        nr++;
      }
    break;
    case 'a':
      for (i = start; i <= end; i++) {
        b = 3; 
        x = 0;    
/*          eliberez 3 spatii in fata randului adaugand litere succesive cu 
          ajutorul codului ASCII*/
          while (b) {
            for (j = strlen(original[i]) + 1; j > x; j--) {
              original[i][j] = original[i][j - 1];
            }
            for (j=0; j<=x; j++) {
              original[i][j] = ' ';
            }
            x++;
            b--;
          }
        original[i][1] = spec;
        original[i][0] = 97 + nr;
        nr++;
      }
    break;
    case 'A':
      for (i = start; i <= end; i++) {
        b = 3; 
        x = 0;    
/*          eliberez 3 spatii in fata randului adaugand litere succesive cu 
          ajutorul codului ASCII*/
          while (b) {
            for (j = strlen(original[i]) + 1; j > x; j--) {
              original[i][j] = original[i][j - 1];
            }
            for (j=0; j<=x; j++) {
              original[i][j] = ' ';
            }
            x++;
            b--;
          }
        original[i][1] = spec;
        original[i][0] = 65 + nr;
        nr++;
      }
    break;
    case 'b':
      for (i = start; i <= end; i++) {
        b = 2; 
        x = 0;    
/*          eliberez 2 spatii in fata randului adaugand caracterul special*/
          while (b) {
            for (j = strlen(original[i]) + 1; j > x; j--) {
              original[i][j] = original[i][j - 1];
            }
            for (j=0; j<=x; j++) {
              original[i][j] = ' ';
            }
            x++;
            b--;
          }
        original[i][0] = spec;
      }
    break;
  }
}

/*functia care ordoneaza elementele unei linii*/
void ordonated_list(char original[][1000], char ord, int start, int end) {
  int i, j;
  char aux[1000];

/*  in functie de caracterul order efectuez instructiunile*/
  switch (ord) {
    case 'a':
/*      efectuez sortarea prin selectie cu min*/
      for (i = start; i <= (end - 1); i++) {
        for (j = i + 1; j <= end; j++) {
          if (strcmp(original[j], original[i]) < 0) {
            strcpy(aux, original[i]);
            strcpy(original[i], original[j]);
            strcpy(original[j], aux);
          }
        }
      }
    break;
    case 'z':
/*      efectuez sortarea prin selectie cu max*/
      for (i = start; i <= (end - 1); i++) {
        for (j = i + 1; j <= end; j++) {
          if (strcmp(original[j], original[i]) > 0) {
            strcpy(aux, original[i]);
            strcpy(original[i], original[j]);
            strcpy(original[j], aux);
          }
        }
      }
    break;
  }
}

/*functia care efectueaza wrap-ul*/
int wrap(char original[][1000], int max_line, int *original_line) {
  int i, j, k, y, start, end, col, ln;
  char aux[1000000], copy[1000][1000];

  // 
  // align_left(original, 0, *original_line - 1);
  
  start = 0;
  ln = 0;
  col = 0;
/*  inscriu elementele unui paragraf intr-un vector unidimensional inlocuind
  caracterul \n ce spatiu*/
  while (start != *original_line) {
    k = 0;
    y = 0;
    for (i = start; i < *original_line; i++) {
      if (i != start) {
        align_left(original, i, i);
      }
      for (j = 0; j <= strlen(original[i]); j++) {
        if (original[i][j] != '\0' && original[i][j] != '\n') {
          aux[k] = original[i][j];
          k++;
        }
        if (original[i][j] == '\n') {
          aux[k] = ' ';
          k++;
        }
      }
      if (original[i][0] == '\n' || original[i + 1][0] == '\0') {
        end = i + 1;
      }
      if (original[i][0] == '\n') 
        break;
    }
    aux[k] = '\0';

    y = 0;
/*    adaug \n pe pozitiile ce se incadreaza in limita lungimii de wrap*/
    while (y < strlen(aux)) {
      if ((y + max_line) > strlen(aux))
        break;
      y = max_line + y;
      if (aux[y] != ' ') {
        while (aux[y] != ' ') {
            y--;
            if (y < 0) {
              printf("Cannot wrap!\n");
              return 1;
            }
        }
        if (y != strlen(aux) - 1) {
          aux[y] = '\n';
          y++;
        }
      }
      else if (y != strlen(aux) - 1){
        aux[y] = '\n';
        y++;
      }  
    }
    aux[strlen(aux)] = '\n';
    aux[strlen(aux) + 1] = '\0';
/*    inscriu fiecare segment intr-o matrice pana la un \n*/
    col = 0;
    for (i = 0; i < strlen(aux) - 1; i++) {
      copy[ln][col] = aux[i];
      col++;
      if (aux[i] == '\n') {
        copy[ln][col] = '\0';
        col = 0;
        ln++;
      }
    }
    memset(aux, 0, 1000000);
    copy[ln + 1][0] = '\n';
    copy[ln + 1][1] = '\0';
    ln += 2;
    start = end;
  }
  ln--;
  copy[ln][0] = '\0';
/*  copiez matricea obtinuta anterior in matricea mea originala*/
  for (i = 0; i <= ln; i++) {
    strcpy(original[i], copy[i]);
    memset(copy[i], 0, strlen(copy[i]));
  }
  *original_line = ln;
  return 0;
}

/*functia care realizeaza justifyl*/
void justify(char original[][1000], int start, int end, int original_line) {
  int i ,j , max, a, k = 0;

  align_left(original, start, end);
/*  fac astfel incat intre toatele cuvintele sa existe un singur spatiu*/
  for (i = start; i <= end; i++) {
    for (j = 0; j < strlen(original[i]); j++) {
      if (original[i][0] != '\n') {
        if (isspace(original[i][j])) {
        k++;
        }
        else 
        break;
      }
    }
    for (j = k; j <= strlen(original[i]); j++) {
      original[i][j - k] = original[i][j];
    }
    k = 0;
  }
/*  gasesc lungimea maxima de linie*/
  max = 0;
  for (i = 0; i < original_line; ++i) {
    if(strlen(original[i]) > max) {
      max = strlen(original[i]);
    }
  }
/*  de fiecare cand dau de un spatiu ce se afla inaintea unui caracter mut cu o 
  pozitie la dreapta tot ce este dupa pana cand nu fac lungimea egala cu max*/
  a = 0;
  for (i = start; i <= end; i++) {
    while (strlen(original[i]) < max && original[i][0] != '\n' &&
     original[i + 1][0] != '\n' && original[i + 1][0] != '\0') {
      for (j=0; j<=strlen(original[i]); j++) {
        if (original[i][j - 1] == ' ' && original[i][j] != ' ') {
          a = j;
          j++;
          for (k = strlen(original[i]) + 1; k > a; k--) {
            original[i][k] = original[i][k - 1];
          }
          original[i][a] = ' ';
          if (strlen(original[i]) == max) 
            break;
        }
        a = 0;
      } 
    }
  } 
}

int main(int argc, char *argv[]) {
  char buf[1000],               // buffer used for reading from the file
      original[1000][1000],     // original text, line by line
      result[1000][1000];       // text obtained after applying operations
  int original_line_count = 0,  // number of lines in the input file
      result_line_count = 0,    // number of lines in the output file
      i;
  int b=0, max_line_length, start_line, end_line, ident_length = -1, count = 0, 
      valid = 1, a = 0, countSeq = 1, j, original_line_copy;
  char operation, list_type, special_character, ordering, str[1000],
        oprations[]="WCLRJPIOwclrjpio", list[]="naAb", **sequences, *find, 
        *p, case1[] = "LlRrCcJj", copy[1000][1000], orderings[] = "az";


  if (argc != 4) {  // invalid number of arguments
    fprintf(stderr,
            "Usage: %s operations_string input_file output_file\n",
            argv[0]);
    return -1;
  }

  // Open input file for reading
  FILE *input_file = fopen(argv[2], "r");

  if (input_file == NULL) {
    fprintf(stderr, "File \"%s\" not found\n", argv[2]);
    return -2;
  }

  // Read data in file line by line
  while (fgets(buf, 1000, input_file)) {
    strcpy(original[original_line_count], buf);
    original_line_count++;
  }

  fclose(input_file);

  original_line_copy = original_line_count;
  for (i = 0; i <= original_line_copy; i++) {
    strcpy(copy[i], original[i]);
  }
/*  realizez o copie a operatiilor*/
  strcpy(str, argv[1]);

/*  numar cate operatii am*/
  find = strchr(str, ',');
  while (find) {
    countSeq++;
    find = strchr(find + 1, ',');
  }
/*  inscriu fiecare operatie intr-o matrice de caractere*/
  sequences = (char **)malloc(countSeq * sizeof(char *));
  p = strtok(str, ",");
  for (i = 0; i < countSeq; i++) {
    sequences[i] = p;
    p = strtok(NULL, ",");
  }
/*  incep sa efectuez fiecare operatie*/      
  for (i = 0; i < countSeq; i++) {
  start_line = 0;
  end_line = original_line_count - 1;
/*    verific daca operatia e valid*/
    if (strlen(sequences[i]) < 20) {
/*      extrag din operatie fiecare element*/
      p = strtok(sequences[i], " ");
      while (p != NULL && valid) {
        b++; 
/*        identific tipul operatiei*/
        if (strlen(p) == 1 && strchr(oprations,p[0]) && b == 1) {
          operation = p[0];
        }
        else if (b == 1) {
          valid = 0;
        }
/*        in functie de operatie extrag ceilalti parametri, daca intalnesc un 
        parametru gresit salvez faptul ca am dat peste un parametru invalid,
        deci intreaga operatie e invalida*/
/*        variabile pentru wrap*/
        if (strchr("wW",operation) && b > 1) {
          for (j = 0; j < strlen(p); j++) {
            if (p[j] >= '0' && p[j] <= '9') {
              a++;
              if (a == strlen(p) && b == 2) {
                  max_line_length = atoi(p);
                  valid = 1;
                  count = 1;
              }
              else if (b == 2) {
                valid = 0;
              }
            }
            else {
              valid = 0;
            }
          }
          a = 0;
          if (b > 2) {
            valid = 0;
          }
        }

/*        variabile pentru left right center justify*/
        if (strchr(case1,operation) && b > 1) {
          for (j = 0; j < strlen(p); j++) {
            if (p[j]>='0' && p[j]<='9') {
              a++;
              if (a == strlen(p) && b == 2) {
                  start_line = atoi(p);
                  valid = 1;
              }
              else if (b == 2) {
                valid = 0;
              }
              if (a == strlen(p) && b == 3) {
                  end_line = atoi(p);
                  valid = 1;
              }
              else if (b == 3) {
                valid = 0;
              }
            }
            else if (b > 1) {
              valid = 0;
            }
          }
          a = 0;
          if (b > 3) {
            valid = 0;
          }
        }

/*        variabilele pentru paragraf*/
        if (strchr("pP",operation) && b > 1) {
          for (j = 0; j < strlen(p); j++) {
            if (p[j]>='0' && p[j]<='9') {
              a++;
              if (a == strlen(p) && b == 2) {
                  ident_length = atoi(p);
                  valid = 1;
                  count = 1;
              }
              else if (b == 2) {
                valid = 0;
              }
              if (a == strlen(p) && b == 3) {
                  start_line = atoi(p);
                  valid = 1;
              }
              else if (b == 3) {
                valid = 0;
              }
              if (a == strlen(p) && b == 4) {
                  end_line = atoi(p);
                  valid = 1;
              }
              else if (b == 4) {
                valid = 0;
              }
            }
            else {
              valid = 0;
            }
          }
          a = 0;
          if (b > 4) {
            valid = 0;
          }
        }

/*        variabile pentru liste*/
        if (strchr("iI",operation) && b > 1) {
          if (strlen(p) == 1 && strchr(list,p[0]) && b == 2) {
            list_type = p[0];
            valid = 1; 
          }
          else if (b == 2) {
            valid = 0;
          }
          if (strlen(p) == 1 && b == 3) {
            special_character = p[0];
            valid = 1;
            count = 1;
          }
          else if (b == 3) {
            valid = 0;
          }
          for (j = 0; j < strlen(p); j++) {
            if (p[j]>='0' && p[j]<='9') {
              a++;
              if (a == strlen(p) && b == 4) {
                  start_line = atoi(p);
                  valid = 1;
              }
              else if (b == 4) {
                valid = 0;
              }
              if (a == strlen(p) && b == 5) {
                  end_line = atoi(p);
                  valid = 1;
              }
              else if (b == 5) {
                valid = 0;
              }
            }
            else if (b > 3) {
              valid = 0;
            }
          }
          a = 0;
          if (b > 5) {
            valid = 0;
          }
        }

/*        variabile pentru liste oedonate*/
        if (strchr("oO",operation) && b > 1) {
          if (strlen(p) == 1 && strchr(list,p[0]) && b == 2) {
            list_type = p[0];
            valid = 1; 
          }
          else if (b == 2) {
            valid = 0;
          }
          if (strlen(p) == 1 && b == 3) {
            special_character = p[0];
          }
          else if (b == 3) {
            valid = 0;
          }
          if (strlen(p) == 1 && b == 4 && strchr(orderings, p[0])) {
            ordering = p[0];
            count = 1;
          }
          else if (b == 4) {
            valid = 0;
          }
          for (j = 0; j < strlen(p); j++) {
            if (p[j] >= '0' && p[j] <= '9') {
              a++;
              if (a == strlen(p) && b == 5) {
                  start_line = atoi(p);
                  valid = 1;
              }
              else if (b == 5) {
                valid = 0;
              }
              if (a == strlen(p) && b == 6) {
                  end_line = atoi(p);
                  valid = 1;
              }
              else if (b == 6) {
                valid = 0;
              }
            }
            else if (b > 4) {
              valid = 0;
            }
          }
          a = 0;
          if (b > 6) {
            valid = 0;
          }
        }
        //printf("%d valid %d\n",b, valid);
        p = strtok(NULL, " ");
      }
    }
    else {
      valid = 0;
    }
/*    mai verific niste cazuri de eroare*/
    if (start_line > end_line || ((strchr("oOiIpPwW",operation) && !count) ||
      start_line > original_line_count - 1 || start_line < 0)) {
      valid = 0;
    }
    if (end_line > original_line_count - 1) {
      end_line = original_line_count - 1;
    }
    b = 0;
    count = 0;
/*    verific validitatea operatie*/
    if (valid) {
/*      scap de whitespace*/
      rm_whitespace_trail(original, original_line_count);
/*      in functie de operatia mea apelez functia potrivita*/
      switch (operation) {
        case 'W':
        case 'w':
            if (wrap(original, max_line_length, &original_line_count)) {
              for (i = 0; i <= original_line_copy; i++) {
                strcpy(original[i], copy[i]);
              }
            }
            else  
              rm_whitespace_trail(original, original_line_count);
        break;

        case 'C':
        case 'c':
          center(original, start_line, end_line, original_line_count);
        break;

        case 'L':
        case 'l':
          align_left(original, start_line, end_line);
        break;

        case 'R':
        case 'r':
          align_right(original, start_line, end_line, original_line_count);
        break;

        case 'J':
        case 'j':
          justify(original, start_line, end_line, original_line_count);
        break;

        case 'P':
        case 'p':
          paragraph(original, ident_length, start_line, end_line);
        break;

        case 'I':
        case 'i':
          ls_data(original, list_type, special_character, start_line, end_line);
        break;

        case 'O':
        case 'o':
          ordonated_list(original, ordering, start_line, end_line);
          ls_data(original, list_type, special_character, start_line, end_line);
        break;
      }
    }
    else {
/*      daca operatia e invalida afisez mesajul Invalid operation!*/
      printf("Invalid operation!\n");
      original_line_count = original_line_copy;
      for (i = 0; i <= original_line_copy; i++) {
        strcpy(original[i], copy[i]);
      }
    }
  }
  free(sequences);

  for (i = 0; i < original_line_count; i++) {
    strcpy(result[i], original[i]);
  }
  result_line_count = original_line_count;

  // Open output file for writing
  FILE *output_file = fopen(argv[3], "w");

  if (output_file == NULL) {
    fprintf(stderr, "Could not open or create file \"%s\"\n", argv[3]);
    return -2;
  }

  // Write result in output file
  for (i = 0; i < result_line_count; i++) {
    fputs(result[i], output_file);
  }

  fclose(output_file);
  return 0;
} 