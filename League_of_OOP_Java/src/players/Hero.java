package players;

import angels.Angel;
import map.GameMap;

import java.util.HashMap;

/**
 * Clasa Hero este o clasa abstracta, care urmeaza sa fie extinsa de fiecare tip de erou.
 * Aceasta contine cea mai mare parte a functionlitatii unui erou.
 */
public abstract class Hero {
    protected GameMap map;
    protected String type;
    protected int lvl;
    protected int xp;
    protected int hpModifier;
    protected int maxHp;
    protected int currentHp;
    protected int firstDamage;
    protected int firstDamageModifier;
    protected int secondDamage;
    protected int secondDamageModifier;
    protected int x;
    protected int y;
    protected int overtimeDamage;
    protected int overtimeRounds;
    protected Hero overtimeHero;
    protected int paralysis;
    protected int id;
    protected boolean isAlive;
    protected HashMap<String, Float> firstRaceModifier;
    protected HashMap<String, Float> initialFirstRace;
    protected HashMap<String, Float> secondRaceModifier;
    protected HashMap<String, Float> initialSecondRace;
    protected String name;

    public Hero(
            final String type,
            final int initialHp,
            final int hpModifier,
            final int firstDamage,
            final int firstDamageModifier,
            final int secondDamage,
            final int secondDamageModifier,
            final int x,
            final int y,
            final GameMap map,
            final HashMap<String, Float> firstRaceModifier,
            final HashMap<String, Float> secondRaceModifier,
            final int id,
            final String name) {
        this.type = type;
        this.lvl = 0;
        this.xp = 0;
        this.maxHp = initialHp;
        this.hpModifier = hpModifier;
        this.currentHp = initialHp;
        this.firstDamage = firstDamage;
        this.firstDamageModifier = firstDamageModifier;
        this.secondDamage = secondDamage;
        this.secondDamageModifier = secondDamageModifier;
        this.isAlive = true;
        this.x = x;
        this.y = y;
        this.overtimeDamage = 0;
        this.overtimeRounds = 0;
        this.paralysis = 0;
        this.firstRaceModifier = firstRaceModifier;
        this.secondRaceModifier = secondRaceModifier;
        this.initialFirstRace = firstRaceModifier;
        this.initialSecondRace = secondRaceModifier;
        this.map = map;
        this.id = id;
        this.name = name;
        this.overtimeHero = null;
    }

    /**
     * Metoda care ridica nivelul unui erou, daca acesta are suficient xp.
     */
    public String lvlUp() {
        String message = "";
        int xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;

        while (xp >= xpLevelUp) {
            lvl++;
            message += lvlUpMessage();
            maxHp += hpModifier;
            currentHp = maxHp;
            firstDamage += firstDamageModifier;
            secondDamage += secondDamageModifier;
            xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;
        }
        return message;
    }

    /**
     * Metoda ce verifica daca eroul si-a omorat adversarul.
     * In caz afirmativ acesta primeste xp conform cu o formula.
     */
    public String getExp(final Hero opponent) {
        if (!opponent.isAlive) {
            xp += Math.max(0, Const.XP_OFFSET - (this.lvl - opponent.lvl) * Const.XP_AMPLIFICATOR);
        }
        return this.lvlUp();
    }

    /**
     * Metoda ce ii scade din viata eroului.
     */
    public void getDamage(final int damage) {
        currentHp -= damage;

        if (currentHp < 0) {
            currentHp = 0;
        }
    }

    /**
     * Metoda ce ii schimba pozitia eroului pe harta conform cu inputul.
     */
    public void move(final char dirrection) {
        // daca eroul este paralizat acesta nu se misca
        if (paralysis > 0) {
            paralysis--;
            return;
        }

        // se misca pe harta pornind de la input
        switch (dirrection) {
            case Const.UP:
                x--;
                break;
            case Const.DOWN:
                x++;
                break;
            case Const.LEFT:
                y--;
                break;
            case Const.RIGHT:
                y++;
                break;
            default:
        }
    }

    /**
     * Metoda ce ma ajuta sa folosesc Double Dispatch.
     */
    public float accept(final float damage) {
        return map.getLandModifier(this, damage);
    }

    /**
     * Metoda ce returneaza damage-ul primii abilitati cu tot cu amplificatori.
     */
    public int useFirstSkill(final Hero opponent) {
        int damage = firstDamage;

        float modifiedDamage;
        modifiedDamage = damage * firstRaceModifier.get(opponent.type);
        modifiedDamage += this.accept(modifiedDamage);
        return Math.round(modifiedDamage);
    }

    /**
     * Metoda ce returneaza damage-ul celei de a doua abilitati cu tot cu amplificatori.
     */
    public int useSecondSkill(final Hero opponent) {
        int damage = secondDamage;

        float modifiedDamage;
        modifiedDamage = damage * secondRaceModifier.get(opponent.type);
        modifiedDamage += this.accept(modifiedDamage);
        return Math.round(modifiedDamage);
    }

    /**
     * Metoda ce ii scoate din viata eroului daca el primeste overtime damage.
     */
    public void getOvertimeDamage() {
        if (overtimeRounds > 0) {
            currentHp -= overtimeDamage;
            overtimeRounds--;
            this.checkHp();
        }
    }

    /**
     * Metoda ce scoate din viata adversarului un damage de la prima si a doua abilitate.
     */
    public void attack(final Hero opponent) {
        opponent.getDamage(this.useFirstSkill(opponent) + this.useSecondSkill(opponent));
    }

    /**
     * Returneaza damage-ul total fara amplificatorii de rasa.
     */
    public int attackWithoutRace() {
        int damage = firstDamage + secondDamage;
        damage += Math.round(map.getLandModifier(this, damage));
        return damage;
    }

    /**
     * Metoda ce verifica daca un erou e mort.
     */
    public void checkHp() {
        if (currentHp <= 0) {
            isAlive = false;
            currentHp = 0;
        }
    }

    /**
     * Metoda ce verifica daca doi eroi se afla pe aceeasi pozitie.
     */
    public boolean samePosition(final Hero opponent) {
        return x == opponent.x && y == opponent.y;
    }

    /**
     * Scimba modificatorul de race.
     */
    protected void changeRaceModifier(final float val) {
        for (String i : firstRaceModifier.keySet()) {
            firstRaceModifier.replace(i, firstRaceModifier.get(i) + val);
        }

        for (String i : secondRaceModifier.keySet()) {
            secondRaceModifier.replace(i, secondRaceModifier.get(i) + val);
        }
    }

    public void interactWithAngel(final Angel angel) {
        angel.giveBuff(this);
    }

    public abstract void changeStrategy();

    /**
     * Mareste hp-ul unui erou.
     */
    public void increaseHp(final int increase) {
        currentHp = Math.min(maxHp, currentHp + increase);
    }

    /**
     * Returneaza tipul si nivelul eroului.
     */
    public String heroMessage() {
        return name + " " + id;
    }

    /**
     * Returneaza un mesaj ce il informeaza pe magician ca jucatorul a fost omorat de un inger.
     */
    public String killedByAngel() {
        return "Player " + heroMessage()
                + " was killed by an angel\n";
    }

    /**
     * Returneaza un mesaj ce il informeaza pe magician ca un jucator a murit.
     */
    public String killedByPlayer(final Hero opponent) {
        return "Player " + heroMessage()
                + " was killed by " + opponent.name + " " + opponent.id + "\n";
    }

    /**
     * Returneaza un mesaj ce il informeaza pe magician ca un jucator si-a arit nivelul.
     */
    public String lvlUpMessage() {
        if (isAlive) {
            return heroMessage() + " reached level " + lvl + "\n";
        } else {
            return "";
        }
    }


    /**
     * Metoda ce suprascrie toString().
     */
    @Override
    public String toString() {
        if (isAlive) {
            return type + " " + lvl + " " + xp + " " + currentHp + " " + x + " " + y;
        } else {
            return type + " dead";
        }
    }

    /**
     * Returneaza un mesaj ce il informeaza pe magician ca un jucator a murit.
     */
    public String reviveMessage() {
        return "Player " + name + " " + id + " was brought to life by an angel\n";
    }


    /**
     * Getter pentru x.
     */
    public int getX() {
        return x;
    }

    /**
     * Gtetter pentru y.
     */
    public int getY() {
        return y;
    }

    /**
     * Getter pentru isAlive.
     */
    public boolean isAlive() {
        return isAlive;
    }

    /**
     * Setter pentru isAlive.
     */
    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    /**
     * Getter pentru modificatorii primului skill.
     */
    public HashMap<String, Float> getFirstRaceModifier() {
        return firstRaceModifier;
    }

    /**
     * Getter pentru modificatorii celui de-al doilea skill.
     */
    public HashMap<String, Float> getSecondRaceModifier() {
        return secondRaceModifier;
    }

    /**
     * Getter pentru modificatorii initiali celui de-al doilea skill.
     */
    public HashMap<String, Float> getInitialFirstRace() {
        return initialFirstRace;
    }

    /**
     * Getter pentru modificatorii initiali celui de-al doilea skill.
     */
    public HashMap<String, Float> getInitialSecondRace() {
        return initialSecondRace;
    }

    public Hero getOvertimeHero() {
        return overtimeHero;
    }

    /**
     * Getter pentru lvl.
     */
    public int getLvl() {
        return lvl;
    }

    /**
     * Getter pentru xp.
     */
    public int getXp() {
        return xp;
    }

    /**
     * Setter pentru xp.
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * Setter pentru currentHp.
     */
    public void setCurrentHp(final int currentHp) {
        this.currentHp = currentHp;
    }

    public void printDamage() {
        System.out.println(this + "\nfirst damage: " + firstDamage + "\nsecond damage:" + secondDamage + "\n");
    }
}
