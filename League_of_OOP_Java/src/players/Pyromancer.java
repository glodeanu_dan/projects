package players;

import angels.Angel;
import map.GameMap;

import java.util.HashMap;

/**
 * Clasa Pyromancer este un erou propriu zis ce extinde clasa abstracta Hero.
 */
public final class Pyromancer extends Hero {
    private int igniteOverDamage;
    private int igniteOverRounds;
    private int igniteOverDamageIncrease;

    public Pyromancer(
            final String type,
            final int initialHp,
            final int hpModifier,
            final int firstDamage,
            final int firstDamageModifier,
            final int secondDamage,
            final int secondDamageModifier,
            final int x,
            final int y,
            final GameMap map,
            final HashMap<String, Float> firstSkillModifier,
            final HashMap<String, Float> secondSkillModifier,
            final int igniteOverDamage,
            final int igniteOverDamageIncrease,
            final int igniteOverRounds,
            final int id) {
        super(
                type,
                initialHp,
                hpModifier,
                firstDamage,
                firstDamageModifier,
                secondDamage,
                secondDamageModifier,
                x,
                y,
                map,
                firstSkillModifier,
                secondSkillModifier,
                id,
                "Pyromancer");
        this.igniteOverDamage = igniteOverDamage;
        this.igniteOverDamageIncrease = igniteOverDamageIncrease;
        this.igniteOverRounds = igniteOverRounds;
    }

    @Override
    public String lvlUp() {
        String message = "";
        int xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;

        while (xp >= xpLevelUp) {
            lvl++;
            message += lvlUpMessage();
            xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;
            maxHp += hpModifier;
            currentHp = maxHp;
            firstDamage += firstDamageModifier;
            secondDamage += secondDamageModifier;
            overtimeDamage += igniteOverDamageIncrease;
        }
        return message;
    }

    @Override
    public float accept(final float damage) {
        return map.getLandModifier(this, damage);
    }

    @Override
    public int useSecondSkill(final Hero opponent) {
        opponent.overtimeHero = this;
        opponent.overtimeRounds = igniteOverRounds;
        int overDamage = Math.round(igniteOverDamage * secondRaceModifier.get(opponent.type));
        overDamage += map.getLandModifier(this, overDamage);
        opponent.overtimeDamage = overDamage;
        return super.useSecondSkill(opponent);
    }

    @Override
    public int attackWithoutRace() {
        int damage = firstDamage + secondDamage;
        damage += Math.round(map.getLandModifier(this, damage));
        return damage;
    }

    public void changeStrategy() {
        if (paralysis > 0) {
            return;
        }

        if (maxHp / Const.PYROMANCER_LOWER_HP_BOUND < currentHp
                && maxHp / Const.PYROMANCER_UPPER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.PYROMANCER_DECREASE_HP);
            super.changeRaceModifier(Const.PYROMANCER_INCREASE_RACE);
        } else if (maxHp / Const.PYROMANCER_LOWER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.PYROMANCER_INCREASE_HP);
            super.changeRaceModifier(Const.PYROMANCER_DECREASE_RACE);
        }
    }

    @Override
    public void interactWithAngel(final Angel angel) {
        angel.giveBuff(this);
    }
}
