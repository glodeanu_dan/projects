package players;

import angels.Angel;
import map.GameMap;

import java.util.HashMap;

/**
 * Clasa Knight este un erou propriu zis ce extinde clasa abstracta Hero.
 */
public final class Knight extends Hero {
    private float executeHp;
    private float executeHpIncrease;
    private float executeHpMax;

    public Knight(
            final String type,
            final int initialHp,
            final int hpModifier,
            final int firstDamage,
            final int firstDamageModifier,
            final int secondDamage,
            final int secondDamageModifier,
            final int x,
            final int y,
            final GameMap map,
            final HashMap<String, Float> firstSkillModifier,
            final HashMap<String, Float> secondSkillModifier,
            final float executeHp,
            final float executeHpIncrease,
            final float executeHpMax,
            final int id) {
        super(
                type,
                initialHp,
                hpModifier,
                firstDamage,
                firstDamageModifier,
                secondDamage,
                secondDamageModifier,
                x,
                y,
                map,
                firstSkillModifier,
                secondSkillModifier,
                id,
                "Knight");
        this.executeHp = executeHp;
        this.executeHpIncrease = executeHpIncrease;
        this.executeHpMax = executeHpMax;
    }

    @Override
    public String lvlUp() {
        String message = "";
        int xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;

        while (xp >= xpLevelUp) {
            if (executeHp < executeHpMax) {
                executeHp += 0.01f;
            }
            lvl++;
            message += lvlUpMessage();
            xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;
            maxHp += hpModifier;
            currentHp = maxHp;
            firstDamage += firstDamageModifier;
            secondDamage += secondDamageModifier;
        }
        return message;
    }

    @Override
    public float accept(final float damage) {
        return map.getLandModifier(this, damage);
    }

    @Override
    public int useFirstSkill(final Hero opponent) {
        int limitHp = Math.round(opponent.maxHp * executeHp);

        if (limitHp >= opponent.currentHp) {
            return opponent.currentHp;
        }

        return super.useFirstSkill(opponent);
    }

    @Override
    public int useSecondSkill(final Hero opponent) {
        opponent.paralysis = Const.KNIGHT_SLAM_STAN;
        return super.useSecondSkill(opponent);
    }

    @Override
    public int attackWithoutRace() {
        int damage = firstDamage + secondDamage;
        damage += Math.round(map.getLandModifier(this, damage));
        return damage;
    }

    public void changeStrategy() {
        if (paralysis > 0) {
            return;
        }

        if (maxHp / Const.KNIGHT_LOWER_HP_BOUND < currentHp
                && maxHp / Const.KNIGHT_UPPER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.KNIGHT_DECREASE_HP);
            super.changeRaceModifier(Const.KNIGHT_INCREASE_RACE);
        } else if (maxHp / Const.KNIGHT_LOWER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.KNIGHT_INCREASE_HP);
            super.changeRaceModifier(Const.KNIGHT_DECREASE_RACE);
        }
    }

    @Override
    public void interactWithAngel(final Angel angel) {
        angel.giveBuff(this);
    }

}
