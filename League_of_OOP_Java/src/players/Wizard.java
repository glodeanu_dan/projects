package players;

import angels.Angel;
import map.GameMap;

import java.util.HashMap;

/**
 * Clasa Wizard este un erou propriu zis ce extinde clasa abstracta Hero.
 */
public final class Wizard extends Hero {
    private int deflectDamageMax;

    public Wizard(
            final String type,
            final int initialHp,
            final int hpModifier,
            final int firstDamage,
            final int firstDamageModifier,
            final int secondDamage,
            final int secondDamageModifier,
            final int x,
            final int y,
            final GameMap map,
            final HashMap<String, Float> firstSkillModifier,
            final HashMap<String, Float> secondSkillModifier,
            final int deflectDamageMax,
            final int id) {
        super(
                type,
                initialHp,
                hpModifier,
                firstDamage,
                firstDamageModifier,
                secondDamage,
                secondDamageModifier,
                x,
                y,
                map,
                firstSkillModifier,
                secondSkillModifier,
                id,
                "Wizard");
        this.deflectDamageMax = deflectDamageMax;
    }

    @Override
    public String lvlUp() {
        String message = "";
        int xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;

        while (xp >= xpLevelUp) {
            lvl++;
            message += lvlUpMessage();
            xpLevelUp = Const.LVL_FORMULA_FIRST + lvl * Const.LVL_FORMULA_SECOND;
            maxHp += hpModifier;
            currentHp = maxHp;
            firstDamage += firstDamageModifier;

            if (secondDamage < deflectDamageMax) {
                secondDamage += secondDamageModifier;
            }
        }
        return message;
    }

    @Override
    public float accept(final float damage) {
        return map.getLandModifier(this, damage);
    }

    /**
     * Calculeaza procentul dintr-o valoare.
     */
    private float computePer(final int num, final int per) {
        float res;
        res = (num * per) / Const.WIZARD_PERCENTAGE;
        return res;
    }

    private float computePer(final float num, final int per) {
        float res;
        res = (num * per) / Const.WIZARD_PERCENTAGE;
        return res;
    }

    @Override
    public int useFirstSkill(final Hero opponent) {
        System.out.println("First skill");
        float hp = Math.min(Const.WIZARD_LIFE_PERCENTAGE * opponent.maxHp,
                opponent.currentHp);
        float damage = this.computePer(hp, firstDamage);
        float modifiedDamage = damage;
        System.out.println("hp: " + hp + " damage: " + damage + " modified damage " + modifiedDamage);
        modifiedDamage += this.accept(modifiedDamage);
        System.out.println("Land modifier " + modifiedDamage);
        modifiedDamage = modifiedDamage * firstRaceModifier.get(opponent.type);
        System.out.println("Race moodifeier " + firstRaceModifier.get(opponent.type));
        System.out.println("Damage " + modifiedDamage + " " + Math.round(modifiedDamage));
        System.out.println();
        return Math.round(modifiedDamage);
    }

    @Override
    public int useSecondSkill(final Hero opponent) {
        int totalDamage = opponent.attackWithoutRace();
        float damage = Math.round(this.computePer(totalDamage, secondDamage));
        float modifiedDamage = damage;
        System.out.println("init: " + totalDamage + " damage: " + damage + " modified damage " + modifiedDamage);
        modifiedDamage += this.accept(modifiedDamage);
        System.out.println("Land modifier " + modifiedDamage);
        modifiedDamage = modifiedDamage * secondRaceModifier.get(opponent.type);
        System.out.println("Race moodifeier " + secondRaceModifier.get(opponent.type));
        System.out.println("Damage " + modifiedDamage + " " + Math.round(modifiedDamage));
        System.out.println();
        return Math.round(modifiedDamage);
    }

    public void changeStrategy() {
        if (paralysis > 0) {
            return;
        }
        if (maxHp / Const.WIZARD_LOWER_HP_BOUND < currentHp
                && maxHp / Const.WIZARD_UPPER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.WIZARD_DECREASE_HP);
            super.changeRaceModifier(Const.WIZARD_INCREASE_RACE);
        } else if (maxHp / Const.WIZARD_LOWER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.WIZARD_INCREASE_HP);
            super.changeRaceModifier(Const.WIZARD_DECREASE_RACE);
        }
    }

    @Override
    public void interactWithAngel(final Angel angel) {
        angel.giveBuff(this);
    }
}
