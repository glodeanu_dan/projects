package players;

import map.GameMap;

import java.util.HashMap;

/**
 * Clasa HeroesFactory creeaza diferite tipuri de eroi.
 * Aceasta este folosita pentru a face mai simplu intelegerea codului.
 */
public class HeroesFactory {
    private final float fpr = 0.8f;
    private final float fpk = 1.2f;
    private final float fpp = 0.9f;
    private final float fpw = 1.05f;
    private final float spr = 0.8f;
    private final float spk = 1.2f;
    private final float spp = 0.9f;
    private final float spw = 1.05f;
    private final float fkr = 1.15f;
    private final float fkk = 1f;
    private final float fkp = 1.1f;
    private final float fkw = 0.8f;
    private final float skr = 0.8f;
    private final float skk = 1.2f;
    private final float skp = 0.9f;
    private final float skw = 1.05f;
    private final float fwr = 0.8f;
    private final float fwk = 1.2f;
    private final float fwp = 0.9f;
    private final float fww = 1.05f;
    private final float swr = 1.2f;
    private final float swk = 1.4f;
    private final float swp = 1.3f;
    private final float sww = 0f;
    private final float frr = 1.2f;
    private final float frk = 0.9f;
    private final float frp = 1.25f;
    private final float frw = 1.25f;
    private final float srr = 0.9f;
    private final float srk = 0.8f;
    private final float srp = 1.2f;
    private final float srw = 1.25f;

    /**
     * Initiaza un HashMap cu cheia typul eroului si valoare modificatorul de rasa a eroului
     */
    HashMap<String, Float> initOneRace(final float r, final float k, final float p, final float w) {
        HashMap<String, Float> map = new HashMap<String, Float>();
        map.put(Const.ROGUE_TYPE, r);
        map.put(Const.KNIGHT_TYPE, k);
        map.put(Const.PYROMANCER_TYPE, p);
        map.put(Const.WIZARD_TYPE, w);
        return map;
    }

    /**
     * Initiaza un erou de un anumit tip.
     */
    public Hero initHero(final String s, final int x, final int y, final GameMap map, final int id) {
        switch (s) {
            case Const.ROGUE_TYPE:
                HashMap<String, Float> backstapRace = initOneRace(frr, frk, frp, frw);
                HashMap<String, Float> paralysisRace = initOneRace(srr, srk, srp, srw);
                return new Rogue(
                        Const.ROGUE_TYPE,
                        Const.ROGUE_INITIAL_HP,
                        Const.ROGUE_LVL_HP,
                        Const.ROGUE_BACKSTAP,
                        Const.ROGUE_BACKSTAP_INCREASE,
                        Const.ROGUE_PARALYSIS,
                        Const.ROGUE_PARALYSIS_INCREASE,
                        x,
                        y,
                        map,
                        backstapRace,
                        paralysisRace,
                        Const.ROGUE_OVERTIME_ROUND,
                        id);
            case Const.KNIGHT_TYPE:
                HashMap<String, Float> executeRace = initOneRace(fkr, fkk, fkp, fkw);
                HashMap<String, Float> slamRace = initOneRace(skr, skk, skp, skw);
                return new Knight(
                        Const.KNIGHT_TYPE,
                        Const.KNIGHT_INITIAL_HP,
                        Const.KNIGHT_LVL_HP,
                        Const.KNIGHT_EXECUTE,
                        Const.KNIGHT_EXECUTE_INCREASE,
                        Const.KNIGHT_SLAM,
                        Const.KNIGHT_SLAM_INCREASE,
                        x,
                        y,
                        map,
                        executeRace,
                        slamRace,
                        Const.KNIGHT_EXECUTE_HP,
                        Const.KNIGHT_EXECUTE_HP_INCREASE,
                        Const.KNIGHT_EXECUTE_HP_MAX,
                        id);
            case Const.WIZARD_TYPE:
                HashMap<String, Float> drainRace = initOneRace(fwr, fwk, fwp, fww);
                HashMap<String, Float> deflectRace = initOneRace(swr, swk, swp, sww);
                return new Wizard(
                        Const.WIZARD_TYPE,
                        Const.WIZARD_INITIAL_HP,
                        Const.WIZARD_LVL_HP,
                        Const.WIZARD_DRAIN_PERCENTAGE,
                        Const.WIZARD_DRAIN_PERCENTAGE_MODIFIER,
                        Const.WIZARD_DEFLECT_PERCENTAGE,
                        Const.WIZARD_DEFLECT_PERCENTAGE_MODIFIER,
                        x,
                        y,
                        map,
                        drainRace,
                        deflectRace,
                        Const.WIZARD_DEFLECT_PERCENTAGE_MAX,
                        id);
            default:
                HashMap<String, Float> fireblastRace = initOneRace(fpr, fpk, fpp, fpw);
                HashMap<String, Float> igniteRace = initOneRace(spr, spk, spp, spw);
                return new Pyromancer(
                        Const.PYROMANCER_TYPE,
                        Const.PYROMANCER_INITIAL_HP,
                        Const.PYROMANCER_LVL_HP,
                        Const.PYROMANCER_FIREBLAST,
                        Const.PYROMANCER_FIREBLAST_INCREASE,
                        Const.PYROMANCER_IGNITE_BASIC,
                        Const.PYROMANCER_IGNITE_BASIC_INCREASE,
                        x,
                        y,
                        map,
                        fireblastRace,
                        igniteRace,
                        Const.PYROMANCER_IGNITE_LITTLE,
                        Const.PYROMANCER_IGNITE_LITTLE_INCREASE,
                        Const.PYROMANCER_IGNITE_ROUNDS,
                        id);
        }
    }
}
