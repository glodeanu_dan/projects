package players;

import angels.Angel;
import map.GameMap;

import java.util.HashMap;

/**
 * Clasa Rogue este un erou propriu zis ce extinde clasa abstracta Hero.
 */
public final class Rogue extends Hero {
    private int round;
    private int paralysisOverRound;

    public Rogue(
            final String type,
            final int initialHp,
            final int hpModifier,
            final int firstDamage,
            final int firstDamageModifier,
            final int secondDamage,
            final int secondDamageModifier,
            final int x,
            final int y,
            final GameMap map,
            final HashMap<String, Float> firstSkillModifier,
            final HashMap<String, Float> secondSkillModifier,
            final int paralysisOverRound,
            final int id) {

        super(
                type,
                initialHp,
                hpModifier,
                firstDamage,
                firstDamageModifier,
                secondDamage,
                secondDamageModifier,
                x,
                y,
                map,
                firstSkillModifier,
                secondSkillModifier,
                id,
                "Rogue");
        this.round = -1;
        this.paralysisOverRound = paralysisOverRound;
    }

    @Override
    public float accept(final float damage) {
        return map.getLandModifier(this, damage);
    }

    @Override
    public int useFirstSkill(final Hero opponent) {
        int crit = 0;
        if (map.checkWoods(this) && round % Const.ROGUE_BACKSTAP_CRIT_ROUND == 0) {
            crit = Math.round(super.useFirstSkill(opponent) / 2f);
        }

        return (super.useFirstSkill(opponent) + crit);
    }

    @Override
    public int useSecondSkill(final Hero opponent) {
        opponent.overtimeHero = this;
        opponent.paralysis = paralysisOverRound;
        opponent.overtimeDamage = super.useSecondSkill(opponent);
        opponent.overtimeRounds = paralysisOverRound;
        return super.useSecondSkill(opponent);
    }

    @Override
    public void attack(final Hero opponent) {
        opponent.getDamage(this.useFirstSkill(opponent) + this.useSecondSkill(opponent));
        paralysisOverRound = Const.ROGUE_OVERTIME_ROUND;
    }

    @Override
    public int attackWithoutRace() {
        int damage = firstDamage + secondDamage;

        if (map.checkWoods(this) && round % Const.ROGUE_BACKSTAP_CRIT_ROUND == 0) {
            damage += Math.round(firstDamage / 2f);
        }

        damage += Math.round(map.getLandModifier(this, damage));
        return damage;
    }

    @Override
    public void move(final char dirrection) {
        round++;
        super.move(dirrection);
    }

    /**
     * Setter pentru paralysisOverRound.
     */
    public void setParalysisOverRound(final int paralysisOverRound) {
        this.paralysisOverRound = paralysisOverRound;
    }

    public void changeStrategy() {
        if (paralysis > 0) {
            return;
        }

        if (maxHp / Const.ROGUE_LOWER_HP_BOUND < currentHp
                && maxHp / Const.ROGUE_UPPER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.ROGUE_DECREASE_HP);
            super.changeRaceModifier(Const.ROGUE_INCREASE_RACE);
        } else if (maxHp / Const.ROGUE_LOWER_HP_BOUND > currentHp) {
            currentHp = Math.round(currentHp * Const.ROGUE_INCREASE_HP);
            super.changeRaceModifier(Const.ROGUE_DECREASE_RACE);
        }
    }

    @Override
    public void interactWithAngel(final Angel angel) {
        angel.giveBuff(this);
    }
}
