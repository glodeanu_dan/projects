package angels;

import players.Hero;

import java.util.HashMap;

public abstract class AbstractAngel {
    protected int x;
    protected int y;
    protected String hero;
    protected String name;

    /**
    * Metoda comuna pentru ingerii care modifica damage-ul.
     */
    protected void changeModifier(final float val, final Hero hero) {


        for (String i : hero.getFirstRaceModifier().keySet()) {
            HashMap<String, Float> modifier = hero.getFirstRaceModifier();
            HashMap<String, Float> initial = hero.getInitialFirstRace();

            if (initial.get(i) == 1f) {
                continue;
            }

            modifier.replace(i, modifier.get(i) + val);
        }

        for (String i : hero.getSecondRaceModifier().keySet()) {
            HashMap<String, Float> modifier = hero.getSecondRaceModifier();
            HashMap<String, Float> initial = hero.getInitialSecondRace();

            if (initial.get(i) == 1f || initial.get(i) == 0f) {
                continue;
            }

            modifier.replace(i, modifier.get(i) + val);
        }
    }

    /**
     * Mesaj care anunta ca un erou a fost ajutat.
     */
    public String getMessage() {
        if (hero != null) {
            return name + " helped " + hero + "\n";
        } else {
            return "";
        }
    }

    /**
     * Mesaj care anunta ca un inger a fost creat.
     */
    public String spawnMessage() {
        return "Angel " + name + " was spawned at "
                + x + " " + y + "\n";
    }

    public final int getX() {
        return x;
    }

    public final int getY() {
        return y;
    }
}
