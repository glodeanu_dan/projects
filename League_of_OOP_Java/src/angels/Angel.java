package angels;

import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

public interface Angel {
    void giveBuff(Hero hero);

    void giveBuff(Knight hero);

    void giveBuff(Pyromancer hero);

    void giveBuff(Rogue hero);

    void giveBuff(Wizard hero);

    String getMessage();

    String spawnMessage();

    int getX();

    int getY();
}
