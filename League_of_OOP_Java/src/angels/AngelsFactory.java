package angels;

public final class AngelsFactory {
    public Angel getAngel(final String input) {
        System.out.println(input);
        String[] aux = input.split(",");
        int x = Integer.parseInt(aux[1]);
        int y = Integer.parseInt(aux[2]);

        System.out.println(aux[0] + " " + x + " " + y);

        switch (aux[0]) {
            case AngelConst.DAMAGE_NAME:
                return new DamageAngel(aux[0], x, y);

            case AngelConst.DARK_NAME:
                return new DarkAngel(aux[0], x, y);

            case AngelConst.DRACULA_NAME:
                return new Dracula(aux[0], x, y);

            case AngelConst.GOOD_NAME:
                return new GoodBoy(aux[0], x, y);

            case AngelConst.LEVEL_NAME:
                return new LevelUpAngel(aux[0], x, y);

            case AngelConst.LIFE_NAME:
                return new LifeGiver(aux[0], x, y);

            case AngelConst.SMALL_NAME:
                return new SmallAngel(aux[0], x, y);

            case AngelConst.SPAWNER_NAME:
                return new Spawner(aux[0], x, y);

            case AngelConst.XP_NAME:
                return new XPAngel(aux[0], x, y);

            default:
                return new TheDoomer(aux[0], x, y);
        }
    }
}
