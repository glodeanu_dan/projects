package angels;

import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

final class Dracula extends AbstractAngel implements Angel {

    public Dracula(final String name, final int x, final int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    @Override
    public void giveBuff(final Hero hero) {
    }

    @Override
    public void giveBuff(final Knight hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.DRACULA_DAM_KNIGHT, hero);
        hero.getDamage(AngelConst.DRACULA_HP_KNIGHT);
        hero.checkHp();
    }

    @Override
    public void giveBuff(final Pyromancer hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.DRACULA_DAM_PYROMANCER, hero);

        hero.getDamage(AngelConst.DRACULA_HP_PYROMANCER);
        hero.checkHp();
    }

    @Override
    public void giveBuff(final Rogue hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.DRACULA_DAM_ROGUE, hero);

        hero.getDamage(AngelConst.DRACULA_HP_ROGUE);
        hero.checkHp();
    }

    @Override
    public void giveBuff(final Wizard hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.DRACULA_DAM_WIZARD, hero);

        hero.getDamage(AngelConst.DRACULA_HP_WIZARD);
        hero.checkHp();
    }

    @Override
    public String getMessage() {
        if (hero != null) {
            return name + " hit " + hero + "\n";
        } else {
            return "";
        }
    }
}
