package angels;

import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

public final class GoodBoy extends AbstractAngel implements Angel {

    public GoodBoy(final String name, final int x, final int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    @Override
    public void giveBuff(final Hero hero) {
    }

    @Override
    public void giveBuff(final Knight hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.GOOD_DAM_KNIGHT, hero);

        hero.increaseHp(AngelConst.GOOD_HP_KNIGHT);
    }

    @Override
    public void giveBuff(final Pyromancer hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.GOOD_DAM_PYROMANCER, hero);

        hero.increaseHp(AngelConst.GOOD_HP_PYROMANCER);
    }

    @Override
    public void giveBuff(final Rogue hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.GOOD_DAM_ROGUE, hero);

        hero.increaseHp(AngelConst.GOOD_HP_ROGUE);
    }

    @Override
    public void giveBuff(final Wizard hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.GOOD_DAM_WIZARD, hero);

        hero.increaseHp(AngelConst.GOOD_HP_WIZARD);
    }
}
