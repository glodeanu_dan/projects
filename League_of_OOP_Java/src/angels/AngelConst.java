package angels;

class AngelConst {
    public static final String DAMAGE_NAME = "DamageAngel";
    public static final float DAMAGE_KNIGHT = 0.15f;
    public static final float DAMAGE_PYROMANCER = 0.2f;
    public static final float DAMAGE_ROGUE = 0.3f;
    public static final float DAMAGE_WIZARD = 0.4f;

    public static final String DARK_NAME = "DarkAngel";
    public static final int DARK_KNIGHT = 40;
    public static final int DARK_PYROMANCER = 30;
    public static final int DARK_ROGUE = 10;
    public static final int DARK_WIZARD = 20;

    public static final String DRACULA_NAME = "Dracula";
    public static final float DRACULA_DAM_KNIGHT = -0.2f;
    public static final int DRACULA_HP_KNIGHT = 60;
    public static final float DRACULA_DAM_PYROMANCER = -0.3f;
    public static final int DRACULA_HP_PYROMANCER = 40;
    public static final float DRACULA_DAM_ROGUE = -0.1f;
    public static final int DRACULA_HP_ROGUE = 35;
    public static final float DRACULA_DAM_WIZARD = -0.4f;
    public static final int DRACULA_HP_WIZARD = 20;

    public static final String GOOD_NAME = "GoodBoy";
    public static final float GOOD_DAM_KNIGHT = 0.4f;
    public static final int GOOD_HP_KNIGHT = 20;
    public static final float GOOD_DAM_PYROMANCER = 0.5f;
    public static final int GOOD_HP_PYROMANCER = 30;
    public static final float GOOD_DAM_ROGUE = 0.4f;
    public static final int GOOD_HP_ROGUE = 40;
    public static final float GOOD_DAM_WIZARD = 0.3f;
    public static final int GOOD_HP_WIZARD = 50;

    public static final String LEVEL_NAME = "LevelUpAngel";
    public static final float LEVEL_KNIGHT = 0.1f;
    public static final float LEVEL_PYROMANCER = 0.2f;
    public static final float LEVEL_ROGUE = 0.15f;
    public static final float LEVEL_WIZARD = 0.25f;

    public static final String LIFE_NAME = "LifeGiver";
    public static final int LIFE_KNIGHT = 100;
    public static final int LIFE_PYROMANCER = 80;
    public static final int LIFE_ROGUE = 90;
    public static final int LIFE_WIZARD = 120;

    public static final String SMALL_NAME = "SmallAngel";
    public static final float SMALL_DAM_KNIGHT = 0.1f;
    public static final int SMALL_HP_KNIGHT = 10;
    public static final float SMALL_DAM_PYROMANCER = 0.15f;
    public static final int SMALL_HP_PYROMANCER = 15;
    public static final float SMALL_DAM_ROGUE = 0.05f;
    public static final int SMALL_HP_ROGUE = 20;
    public static final float SMALL_DAM_WIZARD = 0.1f;
    public static final int SMALL_HP_WIZARD = 25;

    public static final String SPAWNER_NAME = "Spawner";
    public static final int SPAWNER_KNIGHT = 200;
    public static final int SPAWNER_PYROMANCER = 150;
    public static final int SPAWNER_ROGUE = 180;
    public static final int SPAWNER_WIZARD = 120;

    public static final String XP_NAME = "XPAngel";
    public static final int XP_KNIGHT = 45;
    public static final int XP_PYROMANCER = 50;
    public static final int XP_ROGUE = 40;
    public static final int XP_WIZARD = 60;

}
