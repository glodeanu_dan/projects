package angels;

import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

public final class XPAngel extends AbstractAngel implements Angel {

    public XPAngel(final String name, final int x, final int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    @Override
    public void giveBuff(final Hero hero) {

    }

    @Override
    public void giveBuff(final Knight hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        int newXp = hero.getXp() + AngelConst.XP_KNIGHT;
        hero.setXp(newXp);
        hero.lvlUp();
    }

    @Override
    public void giveBuff(final Pyromancer hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        int newXp = hero.getXp() + AngelConst.XP_PYROMANCER;
        hero.setXp(newXp);
        hero.lvlUp();
    }

    @Override
    public void giveBuff(final Rogue hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        int newXp = hero.getXp() + AngelConst.XP_ROGUE;
        hero.setXp(newXp);
        hero.lvlUp();
    }

    @Override
    public void giveBuff(final Wizard hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        int newXp = hero.getXp() + AngelConst.XP_WIZARD;
        hero.setXp(newXp);
        hero.lvlUp();
    }
}
