package angels;

import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

public final class TheDoomer extends AbstractAngel implements Angel {

    public TheDoomer(final String name, final int x, final int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    @Override
    public void giveBuff(final Hero hero) {

    }

    @Override
    public void giveBuff(final Knight hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        hero.setAlive(false);
        hero.setCurrentHp(0);
    }

    @Override
    public void giveBuff(final Pyromancer hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        hero.setAlive(false);
        hero.setCurrentHp(0);
    }

    @Override
    public void giveBuff(final Rogue hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        hero.setAlive(false);
        hero.setCurrentHp(0);
    }

    @Override
    public void giveBuff(final Wizard hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        hero.setAlive(false);
        hero.setCurrentHp(0);
    }

    @Override
    public String getMessage() {
        if (hero != null) {
            return name + " hit " + hero + "\n";
        } else {
            return "";
        }
    }
}
