package angels;

import players.Const;
import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

public final class LevelUpAngel extends AbstractAngel implements Angel {

    public LevelUpAngel(final String name, final int x, final int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    @Override
    public void giveBuff(final Hero hero) {

    }

    private void increaseLvl(final Hero hero) {
        int xpLevelUp = Const.LVL_FORMULA_FIRST + hero.getLvl() * Const.LVL_FORMULA_SECOND;
        hero.setXp(xpLevelUp);
        hero.lvlUp();
    }

    @Override
    public void giveBuff(final Knight hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.LEVEL_KNIGHT, hero);

        increaseLvl(hero);
    }

    @Override
    public void giveBuff(final Pyromancer hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.LEVEL_PYROMANCER, hero);

        increaseLvl(hero);
    }

    @Override
    public void giveBuff(final Rogue hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.LEVEL_ROGUE, hero);

        increaseLvl(hero);
    }

    @Override
    public void giveBuff(final Wizard hero) {
        if (!hero.isAlive()) {
            this.hero = null;
            return;
        }
        this.hero = hero.heroMessage();

        changeModifier(AngelConst.LEVEL_WIZARD, hero);

        increaseLvl(hero);
    }
}
