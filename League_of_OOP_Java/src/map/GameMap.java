package map;

import main.Input;
import players.Const;
import players.Hero;
import players.Knight;
import players.Pyromancer;
import players.Rogue;
import players.Wizard;

/**
 * Clasa singletone care stocheaza mapa jocului.
 */
public final class GameMap {
    private static GameMap mapInstance = null;
    private static int n;
    private static int m;
    private static char[][] lands;

    private GameMap(final Input input) {
        this.n = input.getN();
        this.m = input.getM();
        lands = new char[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                lands[i][j] = input.getLand(i, j);
            }
        }
    }

    /**
     * Metoda ce returneaza o instanta a clasei.
     * Daca clasa nu a fost inca instantiata aceasta e creata.
     */
    public static GameMap getInstance(final Input input) {
        if (mapInstance == null) {
            mapInstance = new GameMap(input);
        }
        return mapInstance;
    }

    /**
     * Returneaza bonusul de teren pentru Pyromancer.
     */
    public float getLandModifier(final Pyromancer hero, final float damage) {
        float bonus = 0f;
        int i = hero.getX();
        int j = hero.getY();

        if (lands[i][j] == Const.PYROMANCER_LAND) {
            bonus = damage * Const.PYROMANCER_LAND_MODIFIER;
        }

        return bonus;
    }

    /**
     * Returneaza bonusul de teren pentru Knight.
     */
    public float getLandModifier(final Knight hero, final float damage) {
        float bonus = 0f;
        int i = hero.getX();
        int j = hero.getY();
        if (lands[i][j] == Const.KNIGHT_LAND) {
            bonus = damage * Const.KNIGHT_LAND_MODIFIER;
        }

        return bonus;
    }

    /**
     * Verifica daca un Rogue se afla in mediul Woods.
     */
    public boolean checkWoods(final Rogue hero) {
        int i = hero.getX();
        int j = hero.getY();

        return lands[i][j] == Const.ROGUE_LAND;
    }

    /**
     * Returneaza bonusul de teren pentru Rogue.
     */
    public float getLandModifier(final Rogue hero, final float damage) {
        float bonus = 0f;
        int i = hero.getX();
        int j = hero.getY();

        if (lands[i][j] == Const.ROGUE_LAND) {
            hero.setParalysisOverRound(Const.ROGUE_OVERTIME_BONUS_ROUND);
            bonus += damage * Const.ROGUE_LAND_MODIFIER;
        }

        return bonus;
    }

    /**
     * Returneaza bonusul de teren pentru Wizard.
     */
    public float getLandModifier(final Wizard hero, final float damage) {
        float bonus = 0f;
        int i = hero.getX();
        int j = hero.getY();

        if (lands[i][j] == Const.WIZARD_LAND) {
            bonus = damage * Const.WIZARD_LAND_MODIFIER;
        }

        return bonus;
    }

    /**
     * Returneaza bonusul de teren pentru Hero, necesar pentru Double Dispatch.
     */
    public float getLandModifier(final Hero hero, final float damage) {
        float bonus = 0f;

        return bonus;
    }
}
