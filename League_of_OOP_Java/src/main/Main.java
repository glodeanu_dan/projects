package main;

public final class Main {

    public static void main(final String[] args) {
        FileManager fileManager = new FileManager(args[0], args[1]);
        Input input = fileManager.read();
        TheGreatMagician magician = TheGreatMagician.getInstance();

        GameRunner game = new GameRunner(input);
        game.addObserver(magician);
        game.run();
        game.writeResult(fileManager);
    }
}
