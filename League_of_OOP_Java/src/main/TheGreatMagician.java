package main;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

public final class TheGreatMagician implements Observer {
    private static TheGreatMagician magician = null;
    public LinkedList<Object> actions;

    private TheGreatMagician() {
        actions = new LinkedList<>();
    }

    public static TheGreatMagician getInstance() {
        if (magician == null) {
            magician = new TheGreatMagician();
        }

        return magician;
    }

    @Override
    public void update(final Observable o, final Object arg) {
        actions.addLast(arg);
    }

    public LinkedList<Object> getActions() {
        return actions;
    }
}
