package main;

import fileio.FileSystem;
import players.Hero;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Clasa FileManger are rolul de a realiza scrierea si citirea din fisiere.
 * Denumirea fisierelor de in si out sunt primite ca parametru in constructor.
 */
public final class FileManager {
    private String inputPath;
    private String outputPath;

    FileManager(final String inputPath, final String outputPath) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
    }

    /**
     * Metoda care citeste din fisierul de input si returneaza o variabila de tip Input.
     * Aceasta continand toate informatiile utile.
     */
    public Input read() {
        int i;
        int n = 0;
        int m = 0;
        LinkedList<String> lands = new LinkedList<String>();
        int heroesNum = 0;
        LinkedList<String> heroes = new LinkedList<String>();
        LinkedList<Integer> coords = new LinkedList<Integer>();
        int rounds = 0;
        LinkedList<String> moves = new LinkedList<String>();
        LinkedList<Integer> angelsNum = new LinkedList<Integer>();
        LinkedList<String[]> angels = new LinkedList<String[]>();

        try {
            FileSystem fs = new FileSystem(inputPath, outputPath);
            // se citesc dimensiunile terenului
            n = fs.nextInt();
            m = fs.nextInt();
            // se citeste harta
            for (i = 0; i < n; i++) {
                lands.addLast(fs.nextWord());
            }
            // se citeste numarul de eroi
            heroesNum = fs.nextInt();
            // se citeste numele eroului si pozitiile sale initiale
            for (i = 0; i < heroesNum; i++) {
                heroes.addLast(fs.nextWord());
                coords.addLast(fs.nextInt());
                coords.addLast(fs.nextInt());
            }
            // se citeste numarul de runde
            rounds = fs.nextInt();
            // se citesc miscarile in fiecare runda
            for (i = 0; i < rounds; i++) {
                moves.addLast(fs.nextWord());
            }

            for (i = 0; i < rounds; i++) {
                angelsNum.add(i, fs.nextInt());
                String[] angel = new String[angelsNum.get(i)];

                for (int j = 0; j < angelsNum.get(i); j++) {
                    angel[j] = fs.nextWord();
                }

                angels.add(i, angel);
            }

            fs.close();

        } catch (Exception e1) {
            e1.printStackTrace();
        }
        // e generata variabila te tip Input
        return new Input(n, m, lands, heroesNum, heroes, coords, rounds, moves,
                angelsNum, angels);
    }

    /**
     * Metoda in care se scrie scorul final al jocului in fisierul de out.
     */
    public void write(final ArrayList<Hero> heroes) {
        TheGreatMagician magician = TheGreatMagician.getInstance();
        try {
            FileWriter fileWriter = new FileWriter(outputPath);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            // sunt scrise actiunile din fiecare runda
            for (Object actions : magician.getActions()) {
                printWriter.print(actions);
            }

            // e scris fiecare erou pe o linie noua
            for (Hero hero : heroes) {
                printWriter.print(hero.toString() + "\n");
            }

            printWriter.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
