package main;

import java.util.LinkedList;

/**
 * Clasa Input stocheaza toate datele necesare pentru crearea elmentelor din joc.
 */
public final class Input {
    private int n;
    private int m;
    private LinkedList<String> lands;
    private int heroesNum;
    private LinkedList<Integer> angelsNum;
    private LinkedList<String[]> angels;
    private LinkedList<String> heroes;
    private LinkedList<Integer> coords;
    private int rounds;
    private LinkedList<String> moves;

    Input(
            final int n,
            final int m,
            final LinkedList<String> lands,
            final int heroesNum,
            final LinkedList<String> heroes,
            final LinkedList<Integer> coords,
            final int rounds,
            final LinkedList<String> moves,
            final LinkedList<Integer> angelsNum,
            final LinkedList<String[]> angels) {
        this.n = n;
        this.m = m;
        this.lands = lands;
        this.heroesNum = heroesNum;
        this.heroes = heroes;
        this.coords = coords;
        this.rounds = rounds;
        this.moves = moves;
        this.angelsNum = angelsNum;
        this.angels = angels;
    }

    /**
     * Getter pentru n.
     */
    public int getN() {
        return n;
    }

    /**
     * Getter pentru m.
     */
    public int getM() {
        return m;
    }

    /**
     * Getter pentru lands.
     */
    public LinkedList<String> getLands() {
        return lands;
    }

    /**
     * Getter pentru heroesNum.
     */
    public int getHeroesNum() {
        return heroesNum;
    }

    /**
     * Getter pentru heroes.
     */
    public LinkedList<String> getHeroes() {
        return heroes;
    }

    /**
     * Getter pentru coords.
     */
    public LinkedList<Integer> getCoords() {
        return coords;
    }

    /**
     * Getter pentru rounds.
     */
    public int getRounds() {
        return rounds;
    }

    /**
     * Getter pentru moves.
     */
    public LinkedList<String> getMoves() {
        return moves;
    }

    /**
     * Returneaza tipul de teren pentru o anumita pozitie..
     */
    public char getLand(final int i, final int j) {
        String line = lands.get(i);
        return line.charAt(j);
    }

    public LinkedList<Integer> getAngelsNum() {
        return angelsNum;
    }

    public LinkedList<String[]> getAngels() {
        return angels;
    }
}
