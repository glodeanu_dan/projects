package main;

import angels.Angel;
import angels.AngelsFactory;
import map.GameMap;
import players.Hero;
import players.HeroesFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;

/**
 * In clasa GameRunner se intampla toate actiunile din joc si interactiunea dintre eroi.
 */
public final class GameRunner extends Observable {
    private ArrayList<Hero> heroes;
    private GameMap map;
    private int heroesNum;
    private LinkedList<Integer> coords;
    private int rounds;
    private LinkedList<String> moves;
    private LinkedList<LinkedList<Angel>> angels;

    GameRunner(final Input input) {
        HeroesFactory factory = new HeroesFactory();
        heroes = new ArrayList<Hero>();
        map = GameMap.getInstance(input);
        heroesNum = input.getHeroesNum();
        coords = input.getCoords();
        rounds = input.getRounds();
        moves = input.getMoves();

        for (int i = 0; i < heroesNum; i++) {
            heroes.add(
                factory.initHero(input.getHeroes().get(i), coords.get(2 * i), coords.get(2 * i + 1),
                        map, i));
        }

        AngelsFactory angelsFactory = new AngelsFactory();
        angels = new LinkedList<LinkedList<Angel>>();

        for (int i = 0; i < rounds; i++) {
            angels.add(i, new LinkedList<Angel>());
            for (int j = 0; j < input.getAngelsNum().get(i); j++) {
                angels.get(i).addLast(angelsFactory.getAngel(input.getAngels().get(i)[j]));
            }
        }
    }

    /**
     * Metoda ce misca eroii spre noile lor coordonate si le da overtime damge.
     */
    private String moveHeroes(final int round) {
        String info = "";
        for (int i = heroesNum - 1; i >= 0; i--) {
            boolean isAlive = heroes.get(i).isAlive();
            heroes.get(i).getOvertimeDamage();
            heroes.get(i).changeStrategy();
            heroes.get(i).move(moves.get(round).charAt(i));
            if (isAlive != heroes.get(i).isAlive()) {
                info += heroes.get(i).killedByPlayer(heroes.get(i).getOvertimeHero());
            }
        }
        return info;
    }

    /**
     * Metoda unde este rulat, propriu-zis, intreg jocul.
     */
    public void run() {
        Hero first;
        Hero second;
        String info = new String();
        // for ce ruleaza toate rundele
        for (int i = 0; i < rounds; i++) {
            info = "~~ Round " + (i + 1) + " ~~\n";

            info += this.moveHeroes(i);
            /*
             * urmatoarele doua foruri itereaza prin lista de jucatori si daca ei sunt
             * diferiti acestia se ataca unul pe celalalt
             */

            for (int j = 0; j < heroesNum; j++) {
                first = heroes.get(j);
                for (int k = j + 1; k < heroesNum; k++) {
                    second = heroes.get(k);
                    if (first.samePosition(second) && first.isAlive() && second.isAlive()) {
                        first.attack(second);
                        System.out.println("Round " + (i+1));
                        System.out.println(second);
                        System.out.println("attacked " + first);
                        second.attack(first);
                        System.out.println("attacked " + first);
                        first.checkHp();
                        second.checkHp();

                        if (!second.isAlive()) {
                            info += second.killedByPlayer(first);
                        }
                        if (!first.isAlive()) {
                            info += first.killedByPlayer(second);
                        }

                        info += first.getExp(second);
                        info += second.getExp(first);

                        if (!second.isAlive() && !first.isAlive()) {
                            second.setXp(0);
                            first.setXp(0);
                        }
                    }
                }
            }

            for (Angel angel : angels.get(i)) {
                info += angel.spawnMessage();
                for (Hero hero : heroes) {

                    if (angel.getX() == hero.getX() &&
                            angel.getY() == hero.getY()) {
                        boolean isAlive = hero.isAlive();
                        int lvl = hero.getLvl();

                        hero.interactWithAngel(angel);

                        info += angel.getMessage();

                        if (hero.getLvl() > lvl) {
                            info += hero.lvlUpMessage();
                        }

                        if (!hero.isAlive() && isAlive) {
                            info += hero.killedByAngel();
                        } else if (hero.isAlive() && !isAlive) {
                            info += hero.reviveMessage();
                        }

                    }

                }
            }
            info += "\n";
            setChanged();
            notifyObservers(info);
        }
        info = "~~ Results ~~\n";
        setChanged();
        notifyObservers(info);
    }

    /**
     * scrie lista de eroi in fisierul de output.
     */
    public void writeResult(final FileManager manager) {
        manager.write(heroes);
    }
}
