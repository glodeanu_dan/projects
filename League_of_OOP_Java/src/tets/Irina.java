package tets;

import java.util.Scanner;

public class Irina extends Student {


    Irina(String name, int nota) {
        super(name, nota);
    }
}

interface SpaceRock {}
class Asteroid implements SpaceRock {}
class Meteor implements SpaceRock {}
interface SpaceShip {
    void avoid(Asteroid asteroid);
    void avoid(Meteor meteor);
}
class Shuttle implements SpaceShip {
    @Override
    public void avoid(Asteroid asteroid) {
        System.out.println("Shuttle avoided an Asteroid");
    }
    @Override
    public void avoid(Meteor meteor) {
        System.out.println("Shuttle avoided a Meteor");
    }
}
class Rocket implements SpaceShip {
    @Override
    public void avoid(Asteroid asteroid) {
        System.out.println("Rocket avoided an Asteroid");
    }
    @Override
    public void avoid(Meteor meteor) {
        System.out.println("Rocket avoided a Meteor");
    }
}
class SpaceShipFactory {
    public static SpaceShip create(String input) {
        if (input.compareTo("shuttle")==0)
            return new Shuttle();
        else
            return new Rocket();
    }
}
class SpaceRockFactory {
    public static SpaceRock create(String input) {
        if (input.compareTo("asteroid")==0)
            return new Asteroid();
        else
            return new Meteor();
    }
}
class Solution {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        SpaceShip spaceShip =
                SpaceShipFactory.create(sc.nextLine());
        SpaceRock spaceRock =
                SpaceRockFactory.create(sc.nextLine());
/******** inceput solutie student ********/
//        try {
//        } catch(Exception e) {
//        }
//        try {
        spaceShip.avoid((Meteor)spaceRock);
        spaceShip.avoid((Asteroid)spaceRock);
//        } catch(Exception e) {
//        }
/******** sfarsit solutie student ********/
    }
}