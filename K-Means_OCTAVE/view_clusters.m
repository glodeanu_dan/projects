function view_clusters(points, centroids)
	cluster = {};
  symbs = {'r', 'g','y','b', 'k', 'c'}; 

    for j = 1:size(centroids, 1)
      cluster{j} = [];
    endfor
 
    for i = 1:size(points, 1) 
      dis = [];
      for j = 1:size(centroids, 1)
        dis = [dis, norm(points(i,:) - centroids(j,:))];
      endfor
      [z,idx] = min(dis);
      points(i,:);
      cluster{idx} = [cluster{idx}; points(i,:)];
    endfor 
    
    scatter3(cluster{1}(:,1),cluster{1}(:,2),cluster{1}(:,3), symbs{1});
    hold on
    for i = 2:size(centroids, 1)
      scatter3(cluster{i}(:,1),cluster{i}(:,2),cluster{i}(:,3), symbs{i});
    end
    
end

