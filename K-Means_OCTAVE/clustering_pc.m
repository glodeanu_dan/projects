function [clusters] = clustering_pc(points,NC)

   error = 0;
   [n, m] = size(points);
   dist = zeros(1,NC);
   clusters = zeros(NC,m);


   for i=1:NC-1
      clusters(i,:) = points(i,:);
   end

   clusters(NC,:) = mean(points(NC:n,:));
   cmp = 1 + error;
   while (cmp > error)
      class = zeros(NC,m);
      Nclass = zeros(NC,1);

      for i=1:n
         for j=1:NC
            dist(j) = norm(points(i,:)-clusters(j,:))^2;
         end
         [value, indx] = min(dist);
         class(indx,:) = class(indx,:) + points(i,:);
         Nclass(indx) = Nclass(indx) + 1;
      end

      for i=1:NC
         class(i,:) = class(i,:) / Nclass(i);
      end
      cmp = 0;

      for i=1:NC
         cmp = norm(class(i,:)-clusters(i,:)); 
      end

      clusters = class;
   end
end
