function cost = compute_cost_pc(points, centroids)
	cost = 0; 

	for i = 1:size(points, 1) 
		all_costs = [];
		for j = 1:size(centroids, 1)
			all_costs = [all_costs, norm(points(i,:) - centroids(j,:))];
		endfor
		[val, index] = min(all_costs);
		cost = cost + val;
	endfor
  
end

