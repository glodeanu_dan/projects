function view_cost_vs_nc(file_points)
	load(file_points);
	centroids = [];
	cost = [];

	for i=1:10 
		centroids = clustering_pc(points, i);
		cost = [cost compute_cost_pc(points, centroids)];
	endfor
	plot(cost);
end

