============================================================================
Nume: Glodeanu
Prenume: Dan
Grupa: 311 CD
Materie: Metode Numerice
============================================================================

read_input_data:
	Citesc informatia din fisier folosind load.

clustering_pc:
	Extrag dimensiunile matricii points, initializez variabilele. Am setat
	eroarea pe 0 pentru ca while sa se opreasca cand centroizii de la pasul 
	anterior sa fie identici cu cei de la pasul curent. Calculez distanta de
	la punctele mele pana la centroizii alesi si gasesc indexul valorii 
	minime. Recalculez pozitia centroizilor, determin daca sunt diferiti de 
	cei precedenti.

view_clusters
	Initializez o lista suficient de lunga cu culori si celule pentru a 
	pastra matricile cu punctele asociate fiecarui cluster. Asociez 
	punctele clusterurilor. Le reprezint grafic utilizand functia scatter3
	si un for.

compute_cost_pc 
	Calculez pe rand valoarea minima de la un punct pana la clutere. Adun
	distanta cea mai mica la cost.

view_cost_vs_nc
	Utilizand un for apelez functia clustering_pc de zece ori, apeland si
	compute_cost_pc rezultatele careia le stochez intr-un vector. Reprezint
	grafic acest vector.