function task2()
   path = 'in/images/image3.gif';
   a = double(imread(path));
   [m, n] = size(a);
   [U, S, V] = svd(a);
   VT = V';
   
   figure(1);
   x = [];
   y = [];
   for i = 1:size(S,1)
     x = [x i];
     y = [y S(i,i)];
   end
   sort(y);
   plot(x, y);
   
   figure(2);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     s1 = 0;
     s2 = 0;
     for i = 1:k
       s1 = s1 + S(i,i);
     end
     for i = 1:min(m,n)
       s2 = s2 + S(i,i);
     end
     
     x = [x k];
     y = [y s1/s2];
   end
   plot(x, y);
   
   figure(3);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     sum = 0;
     [U, S, V] = svd(a);
     A_k = U(:, 1:k) * S(1:k, 1:k) * VT(1:k,:);
     for i = 1:m
       for j = 1:n
         sum = sum + ((a(i,j) - A_k(i,j))^2 / (m * n));
       end
     end
     x = [x k];
     y = [y sum];
   end
   plot(x, y);
   
   figure(4);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     aux = (m * k + n * k + k) / (m * n);
     x = [x, k];
     y = [y, aux];
   end
   plot(x, y);
end