function task5()
  path = 'in/images/image3.gif';
  a = double(imread(path));
  [m, n] = size(a);
  
  figure(1);
  [A_k S] = task3(path, 1);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     x = [x k];
     [A_k S] = task3(path, k);
   end
   y = [y diag(S)];
   plot(y);
   
   figure(2);
   x = [];
   y = [];
   [A_k S] = task3(path, min(m,n));
   for k = [1:19 20:20:99 100:30:min(m,n)]
     s1 = 0;
     s2 = 0;
     for i = 1:k
       s1 = s1 + S(i,i);
     end
     for i = 1:min(m,n)
       s2 = s2 + S(i,i);
     end
     x = [x k];
     y = [y s1/s2];
   end
   plot(x, y);
   
   figure(3);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     sum = 0;
     [A_k S] = task3(path, k);
     for i = 1:m
       for j = 1:n
         sum = sum + ((a(i,j) - A_k(i,j))^2 / (m * n));
       end
     end
     x = [x k];
     y = [y sum];
   end
   plot(x, y);
   
   figure(4);
   x = [];
   y = [];
   for k = [1:19 20:20:99 100:30:min(m,n)]
     aux = (2 * k + 1) / n;
     x = [x, k];
     y = [y, aux];
   end
   plot(x, y);
end