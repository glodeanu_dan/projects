function A_k = task1(image, k)
  a = double(imread(image));
  [U, S, V] = svd(a);
    
  VT = V';
  A_k = U(:, 1:k) * S(1:k, 1:k) * VT(1:k,:);
  
end