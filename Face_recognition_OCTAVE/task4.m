function [A_k S] = task4(image, k)
  a = double(imread(image));
  [m, n] = size(a);
  x = [];

  for i = 1:m
    s = 0;
    for j = 1:n
      s = s + a(i,j);
    end
    x(i) = s / n;
  end
  
  x = x';
  
  for i = 1:m
      a(i, 1:n) = a(i, 1:n) - x(i);
  end
  
  Z = (a * a') / (n - 1);
  
  [V S] = eig(Z);
  
  aux = min(size(V, 1), size(a, 1));
  %for i = 1:aux
  %  W(i,j=1:k) = V(i,j=1:k);
  %end
  
  for i = aux:-1:aux+1-k
    W(:,aux-i+1) = V(:,i);
  end
  
  Y = W' * a;
  
  A_k = W * Y + x;
endfunction