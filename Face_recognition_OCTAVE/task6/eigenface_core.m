function [m A eigenfaces pr_img] = eigenface_core(database_path)
  T = [];
  for k = 1:10
    x = 1;
    file = strcat(database_path,'/',int2str(k),'.jpg');
    a = double(rgb2gray(imread(file)));
    [m n] = size(a);
    
    for i = 1:m
      for j = 1:n
        T(x,k) = a(i,j);
        x = x + 1;
      end
    end
   
  end
  m = [];
  [x y] = size(T);
  
  for i = 1:x;
    s  = 0;
    for j = 1:y
      s = s + T(i,j);
    end
    m = [m; s/y];
  end
  
  A = T - m;
  [V S] = eig(A'*A);
  
  new = [];
  [x y] = size(V);

  for j=1:y
    if S(j,j) > 1
      new = [new V(:,j)];
    end
  end
  V = new;
  
  eigenfaces = A * V;
  
  pr_img = eigenfaces' * A;
end