function [min_dist output_img_index] = face_recognition(image_path, m, A, eigenfaces, pr_img)
  T = [];
  k = 1;
  a = double(rgb2gray(imread(image_path)));
  [x y] = size(a);
    
  for i = 1:x
    for j = 1:y
      T(k,1) = a(i,j);
      k = k + 1;
    end
  end
  A_im = T - m;
  
  pr_test = eigenfaces' * A_im;
  
  [x y] = size(pr_img);
  dis = [];
  for i = 1:x
    dis = [dis norm(pr_img(:,i) - pr_test)];
  endfor

  [min_dist output_img_index] = min(dis);

end