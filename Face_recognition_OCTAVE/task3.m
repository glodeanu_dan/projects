function [A_k S] = task3(image, k)
  a = double(imread(image));
  [m, n] = size(a);
  x = [];
  
  for i = 1:m
    s = 0;
    for j = 1:n
      s = s + a(i,j);
    end
    x(i) = s / n;
  end
  
  x = x';
  
  for i = 1:m
      a(i, 1:n) = a(i, 1:n) - x(i);
  end
  
  Z = a' / sqrt(n - 1);
  
  [U, S, V] = svd(Z);
 
  aux = min(size(V, 1), size(a, 1));
  for i = 1:aux
    W(i,j=1:k) = V(i,j=1:k);
  end
  
  Y = W' * a;

  A_k = W * Y + x;
endfunction