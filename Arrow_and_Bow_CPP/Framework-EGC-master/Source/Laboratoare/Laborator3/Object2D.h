#pragma once

#include <string>

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

namespace Object2D
{

	// Create square with given bottom left corner, length and color
	Mesh* CreateSquare(std::string name, float width, float height, glm::vec3 color);
	Mesh* CreateBaloon(std::string name, glm::vec3 color);
	Mesh* CreateBuff(std::string name, glm::vec3 color);
	Mesh* CreateStar(std::string name);
	Mesh* CreatePlayer(std::string name);
	Mesh* CreateArrow(std::string name);
	Mesh* CreateBow(std::string name);
}

