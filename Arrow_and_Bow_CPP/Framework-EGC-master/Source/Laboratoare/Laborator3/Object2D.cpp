#include <Core/Engine.h>
#include "Object2D.h"
#include "iostream"
using namespace std;

Mesh* Object2D::CreateSquare(std::string name, float width, float height, glm::vec3 color)
{
	//glm::vec3 color = { 0.8, 0.2, 0.2 };
	std::vector<VertexFormat> vertices =
	{
		VertexFormat({0, 0, 1}, color),
		VertexFormat({width, 0, 1}, color),
		VertexFormat({0, height, 1}, color),
		VertexFormat({width, height, 1}, color)
	};

	Mesh* square = new Mesh(name);
	std::vector<unsigned short> indices = 
	{ 
		0, 1, 2,
		2, 1, 3
	};

	square->InitFromData(vertices, indices);
	return square;
}


Mesh* Object2D::CreateStar(std::string name)
{
	glm::vec3 color1 = { 0.75, 0.75, 0.75 };
	glm::vec3 color2 = { 0.5, 0.5, 0.5 };
	glm::vec3 color3 = { 0.25, 0.25, 0.25 };

	std::vector<VertexFormat> vertices =
	{
		VertexFormat({9, 9, 1}, color3),
		VertexFormat({9, 9, 1}, color2),

		VertexFormat({9, 18, 1}, color3),
		VertexFormat({7, 11, 1}, color1),

		VertexFormat({7, 11, 1}, color2),
		VertexFormat({0, 9, 1}, color2),

		VertexFormat({0, 9, 1}, color3),
		VertexFormat({7, 7, 1}, color1),

		VertexFormat({7, 7, 1}, color2),
		VertexFormat({9, 0, 1}, color2),

		VertexFormat({9, 0, 1}, color3),
		VertexFormat({11, 7, 1}, color1),

		VertexFormat({11, 7, 1}, color2),
		VertexFormat({18, 9, 1}, color2),

		VertexFormat({18, 9, 1}, color3),
		VertexFormat({11, 11, 1}, color1),

		VertexFormat({11, 11, 1}, color2),
		VertexFormat({9, 18, 1}, color2),

	};

	Mesh* star = new Mesh(name);
	std::vector<unsigned short> indices =
	{
		0, 2, 3,
		1, 4, 5,
		0, 6, 7,
		1, 8, 9,
		0, 10, 11,
		1, 12, 13,
		0, 14, 15,
		1, 16, 17
	};

	star->InitFromData(vertices, indices);
	return star;
}

Mesh* Object2D::CreateArrow(std::string name)
{
	glm::vec3 white = { 0.9, 0.9, 0.9 };
	glm::vec3 silver = { 0.6, 0.6, 0.6 };
	glm::vec3 wood = { 0.4, 0.11, 0.11 };
	glm::vec3 lightSilver = { 0.75, 0.75, 0.75 };

	std::vector<VertexFormat> vertices =
	{
		VertexFormat({0, 0, 1}, white),
		VertexFormat({4, 0, 1}, white),
		VertexFormat({5, 1, 1}, white),

		VertexFormat({1, 1, 1}, wood),
		VertexFormat({1, 2, 1}, wood),

		VertexFormat({0, 3, 1}, white),
		VertexFormat({4, 3, 1}, white),
		VertexFormat({5, 2, 1}, white),

		VertexFormat({24, 1, 1}, wood),
		VertexFormat({24, 2, 1}, wood),

		VertexFormat({24, 0.5, 1}, silver),
		VertexFormat({24, 2.5, 1}, silver),
		VertexFormat({27, 1.5, 1}, lightSilver),

		VertexFormat({1, 2, 1}, white),
		VertexFormat({1, 1, 1}, white),

	};

	Mesh* arrow = new Mesh(name);
	std::vector<unsigned short> indices =
	{
		0, 1, 2,
		14, 0, 2,

		5, 13, 7,
		5, 7, 6,

		4, 3, 8,
		4, 8, 9,

		11, 10, 12,
	};

	arrow->InitFromData(vertices, indices);
	return arrow;
}

Mesh* Object2D::CreateBaloon(std::string name, glm::vec3 color)
{
	std::vector<VertexFormat> vertices = {};
	int ox = 10, oy = 12;
	
	vertices.push_back(VertexFormat(glm::vec3(10, 12, 1), color + glm::vec3(0.3, 0.3, 0.3)));
	for (GLfloat i = 0; i <= 2.0f * M_PI; i += 2.0f * M_PI / 360) {
		vertices.push_back(VertexFormat(glm::vec3(ox * cos(i) + ox, oy * sin(i) + oy, 1), color));
	}

	Mesh* baloon = new Mesh(name);
	std::vector<unsigned short> indices = {};
	for (int i = 1; i < vertices.size(); i++) {
		if (i % 2) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else {
			indices.push_back(i);
		}
	}

	for (int i = 1; i < vertices.size(); i++) {
		if (i % 2 == 0) {
			indices.push_back(0);
			indices.push_back(i);
		}
		else {
			indices.push_back(i);
		}
	}

	vertices.push_back(VertexFormat(glm::vec3(ox, 1, 1), color));
	vertices.push_back(VertexFormat(glm::vec3(ox - 1, -1.5, 1), color));
	vertices.push_back(VertexFormat(glm::vec3(ox + 1, -1.5, 1), color));
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 1);


	glm::vec3 black = { 0, 0, 0 };
	vertices.push_back(VertexFormat(glm::vec3(ox - 0.25, -1.5, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 2, -8, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox + 0.25, -1.5, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 1.5, -8, 1), black));

	indices.push_back(vertices.size() - 4);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 1);

	vertices.push_back(VertexFormat(glm::vec3(ox - 2, -8, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 0.25, -16, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 1.5, -8, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox + 0.25, -16, 1), black));

	indices.push_back(vertices.size() - 4);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 1);

	vertices.push_back(VertexFormat(glm::vec3(ox - 0.25, -16, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 2, -24, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox + 0.25, -16, 1), black));
	vertices.push_back(VertexFormat(glm::vec3(ox - 1.5, -24, 1), black));

	indices.push_back(vertices.size() - 4);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 2);
	indices.push_back(vertices.size() - 3);
	indices.push_back(vertices.size() - 1);



	baloon->InitFromData(vertices, indices);
	return baloon;
}

Mesh* Object2D::CreateBow(std::string name)
{
	std::vector<VertexFormat> vertices = {};
	int step = 360;
	float r1 = 7, r2 = 5, d = 0.3;
	glm::vec3 black = glm::vec3(0.3, 0.3, 0.3);

	for (GLfloat i = 3 * M_PI / 2; i <= 5 * M_PI / 2; i += M_PI / step) {
		vertices.push_back(VertexFormat(glm::vec3(r2 * cos(i), r1 * sin(i), 1), black));
	}

	for (GLfloat i = 3 * M_PI / 2; i <= 5 * M_PI / 2; i += M_PI / step) {
		vertices.push_back(VertexFormat(glm::vec3((r2 - d) * cos(i), (r1 - d) * sin(i), 1), black));
	}

	Mesh* bow = new Mesh(name);
	std::vector<unsigned short> indices = {};

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + vertices.size() / 2);
	}

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i + vertices.size() / 2);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	bow->SetDrawMode(GL_LINE_STRIP);
	bow->InitFromData(vertices, indices);
	return bow;
}

Mesh* Object2D::CreateBuff(std::string name, glm::vec3 color)
{
	std::vector<VertexFormat> vertices = {};
	int step = 360;
	float r1 = 10, d = 4;

	for (GLfloat i = 0; i <= 2 * M_PI; i += M_PI / step) {
		vertices.push_back(VertexFormat(glm::vec3(r1 * cos(i), r1 * sin(i), 1), color));
	}

	for (GLfloat i = 0; i <= 2 * M_PI; i += M_PI / step) {
		vertices.push_back(VertexFormat(glm::vec3((r1 - d) * cos(i), (r1 - d) * sin(i), 1), color));
	}

	Mesh* buff = new Mesh(name);
	std::vector<unsigned short> indices = {};

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + vertices.size() / 2);
	}

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i + vertices.size() / 2);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i + vertices.size() / 2);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	for (int i = 0; i < vertices.size() / 2; i += 2) {
		indices.push_back(i + 1);
		indices.push_back(i);
		indices.push_back(i + vertices.size() / 2);
	}

	buff->SetDrawMode(GL_LINE_STRIP);
	buff->InitFromData(vertices, indices);
	return buff;
}

Mesh* Object2D::CreatePlayer(std::string name)
{
	glm::vec3 red = { 0.74, 0.2, 0.2 };
	glm::vec3 mlastina = { 0.21, 0.43, 0.12 };
	glm::vec3 lime = { 0.32, 0.65, 0.12 };
	glm::vec3 green = { 0.16, 0.52, 0 };
	glm::vec3 nude = { 0.96, 0.89, 0.65 };
	glm::vec3 brown = {0.25, 0.13, 0.11 };

	std::vector<VertexFormat> vertices =
	{
		VertexFormat({2, 0, 1}, brown),
		VertexFormat({4, 0, 1}, brown),
		VertexFormat({2, 2, 1}, brown),
		VertexFormat({4, 2, 1}, brown),

		VertexFormat({2, 2, 1}, mlastina),
		VertexFormat({4, 2, 1}, mlastina),
		VertexFormat({2, 9, 1}, mlastina),
		VertexFormat({4, 9, 1}, mlastina),

		VertexFormat({1, 9, 1}, mlastina),
		VertexFormat({5, 9, 1}, mlastina),
		VertexFormat({1, 11, 1}, mlastina),
		VertexFormat({5, 11, 1}, mlastina),

		VertexFormat({1, 20, 1}, green),
		VertexFormat({1, 11, 1}, green),
		VertexFormat({5, 11, 1}, green),
		VertexFormat({5, 20, 1}, green),

		VertexFormat({3, 19, 1}, lime),
		VertexFormat({2, 17, 1}, lime),
		VertexFormat({4, 15, 1}, lime),
		VertexFormat({4, 17, 1}, lime),
		VertexFormat({7, 18, 1}, lime),
		VertexFormat({6, 19, 1}, lime),

		VertexFormat({5, 19, 1}, mlastina),
		VertexFormat({5, 17.5, 1}, mlastina),
		VertexFormat({11, 17.5, 1}, mlastina),
		VertexFormat({11, 19, 1}, mlastina),

		VertexFormat({2, 20, 1}, nude),
		VertexFormat({4, 20, 1}, nude),
		VertexFormat({2, 21, 1}, nude),
		VertexFormat({4, 21, 1}, nude),

		VertexFormat({0, 26, 1}, nude),
		VertexFormat({0, 21, 1}, nude),
		VertexFormat({6, 21, 1}, nude),
		VertexFormat({6, 26, 1}, nude),

		VertexFormat({0, 26, 1}, green),
		VertexFormat({1, 31, 1}, green),
		VertexFormat({7, 26, 1}, green),

		VertexFormat({1, 27, 1}, red),
		VertexFormat({0, 28, 1}, red),
		VertexFormat({0, 31, 1}, red),
	};

	Mesh* player = new Mesh(name);
	std::vector<unsigned short> indices =
	{
		39, 38, 37,

		0, 1, 2,
		2, 1, 3,

		6, 4, 5,
		6, 5, 7,

		10, 8, 9,
		10, 9, 11,

		16, 17, 18,
		16, 18, 19,
		19, 18, 21,
		21, 18, 20,

		22, 23, 24,
		22, 24, 25,

		28, 26, 27,
		28, 27, 29,

		30, 31, 32,
		30, 32, 33,

		12, 13, 14,
		12, 14, 15,

		35, 34, 36
	};

	player->InitFromData(vertices, indices);
	return player;
}
