#pragma once
#include <TextRenderer/TextRenderer.h>


#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>
typedef struct Baloon {
	float x, y, scale;
	bool isRed, isAlive;
} Baloon;

typedef struct Star {
	float x, y;
	int orientation;
	bool isAlive;
} Star;

typedef struct Arrow {
	float x, y, step, angle;
} Arrow;

typedef struct Buff {
	float x, y, scale, dx;
	bool isAlive;
	int type;
} Buff;

class Tema1 : public SimpleScene
{
public:


	Tema1();
	~Tema1();

	void Init() override;

private:
	void FrameStart() override;
	void Update(float deltaTimeSeconds) override;
	void FrameEnd() override;

	void OnInputUpdate(float deltaTime, int mods) override;
	void OnKeyPress(int key, int mods) override;
	void OnKeyRelease(int key, int mods) override;
	void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
	void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
	void OnWindowResize(int width, int height) override;

	void nextStar(float delta);
	void nextBaloon(float delta);
	void nextArrow(float delta);
	void nextBuffs(float delta);


protected:
	glm::mat3 modelMatrix;
	float
		playerStep = 100,
		arrowStep = 100,
		starStep = 100,
		ballonStepUp = 50,
		ballonStepDown = 100,
		angularStep = 0.8,
		playerHeight,
		starWidth = 18,
		baloonHeight,
		arrowAngle = 0,
		power = 0,
		maxPower = 100,
		defaultPower = 150,
		playerX = 15,
		playerY,
		arrowX,
		arrowY,
		starX,
		scalePlayer = 4,
		scaleStar = 3,
		scaleArrow,
		delay = 0,
		delayConst = 1.5f,
		arrowLen,

		baloonScale = 2,
		baloonRadiusY = 12,
		baloonRadiusX = 10,
		baloonScaleStep = 0.2,
		baloonMinScale = 0.4,
		baloonTail = 24,
		time = 0,
		bowRadius = 7,

		arrowBuff = 0,
		moveBuff = 0,
		buffRadius = 10,
		buffScale = 2;
	int
		alive = 3,
		upperBound = 20,
		lowerBound = 100,
		nBaloon = 15,
		nStar = 5,
		nBuff = 3,
		starRange = 500,
		score = 0,
		starBonus = 10,
		baloonBonus = 50,
		buffBonus = 200,
		baloonPenalty = 75;

	TextRenderer* Text;
	glm::ivec2 resolution;
	bool
		onPress = false,
		first = true,
		second = true;
	std::vector<Baloon> baloon;
	std::vector<Star> star;
	std::vector<Arrow> arrows;
	std::vector<Buff> buffs;
};