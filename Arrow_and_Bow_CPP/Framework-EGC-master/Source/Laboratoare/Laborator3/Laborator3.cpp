#include "Laborator3.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"
using namespace std;

Tema1::Tema1()
{
}

Tema1::~Tema1()
{
}

void Tema1::Init()
{		
	resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	playerHeight = scalePlayer * 31;
	playerY = (float)resolution.y / 2;
	arrowY = (float)resolution.y / 2;
	arrowX = 6 * scalePlayer;
	arrowY = 18 * scalePlayer;
	arrowLen = 27 * 1.5f;

	//tmp 
	starX = resolution.x;
	

	Mesh* sqaure1 = Object2D::CreateSquare("sqaure1", 50, 50, { 0.8, 0.2, 0.2 });
	AddMeshToList(sqaure1);

	Mesh* sqaure2 = Object2D::CreateSquare("sqaure2", resolution.x, 100, { 0.5, 0.5, 0.5 });
	AddMeshToList(sqaure2);

	Mesh* sqaure3 = Object2D::CreateSquare("sqaure3", 50, 50, { 0.2, 0.2, 0.2 });
	AddMeshToList(sqaure3);

	Mesh* star1 = Object2D::CreateStar("star1");
	AddMeshToList(star1);

	Mesh* arrow1 = Object2D::CreateArrow("arrow1");
	AddMeshToList(arrow1);

	Mesh* player1 = Object2D::CreatePlayer("player1");
	AddMeshToList(player1);

	Mesh* baloon1 = Object2D::CreateBaloon("baloon1", { 0.7f, 0, 0 });
	AddMeshToList(baloon1);

	Mesh* baloon2 = Object2D::CreateBaloon("baloon2", { 0.7f, 0.7f, 0 });
	AddMeshToList(baloon2);

	Mesh* bow = Object2D::CreateBow("bow");
	AddMeshToList(bow);

	Mesh* buff1 = Object2D::CreateBuff("buff1", { 0.3f, 0.9f, 0.9f });
	AddMeshToList(buff1);

	Mesh* buff2 = Object2D::CreateBuff("buff2", { 0.7f, 0, 0.7f });
	AddMeshToList(buff2);

	Mesh* buff3 = Object2D::CreateBuff("buff3", { 0.3f, 0.9f, 0.3f });
	AddMeshToList(buff3);
}

void Tema1::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0.78, 0.86, 0.94, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

bool checkColision(float r1, float r2, float x1, float x2, float y1, float y2) {
	float dx, dy, dist;
	dx = x1 - x2;
	dy = y1 - y2;
	dist = sqrt(dx * dx + dy * dy);

	if (dist < r1 + r2) {
		return true;
	}

	return false;
}

void Tema1::nextStar(float delta) {
	std::vector<Star> copyStar;

	for (int i = 0; i < star.size(); i++) {
		
		if (star[i].isAlive) {
			star[i].x -= starStep * delta;
		}
		else {
			star[i].x += starStep / 2 * delta;
			star[i].y -= ballonStepDown * delta;
		}

		for (int j = 0; j < arrows.size(); j++) {
			float x = abs(arrows[j].x + arrowLen * cos(arrows[j].angle));
			float y = abs(arrows[j].y + arrowLen * sin(arrows[j].angle));

			if (checkColision(0, starWidth * scaleStar / 2, x, star[i].x, y, star[i].y) && star[i].isAlive) {
				score += starBonus;
				star[i].isAlive = false;
				star[i].orientation = -1;
			}
		}

		if (checkColision(bowRadius * scalePlayer, starWidth * scaleStar / 2, playerX + arrowX, star[i].x, playerY + arrowY, star[i].y) && star[i].isAlive) {
			star[i].isAlive = false;
			star[i].orientation = -1;
			alive--;
		}

		if ((star[i].isAlive && star[i].x + starWidth / 2 > 0)
			|| (!star[i].isAlive && star[i].y + starWidth / 2 > lowerBound)) {
			copyStar.push_back(star[i]);
		}
	}

	star = copyStar;

	if (star.size() < nStar) {
		Star elem;
		elem.isAlive = true;
		elem.orientation = 1;
		elem.x = resolution.x + (rand() % resolution.x);
		elem.y = lowerBound + starWidth * scaleStar / 2 + (rand() % (resolution.y - upperBound - lowerBound - (int)(starWidth * scaleStar / 2)));
		star.push_back(elem);
	}
}

void Tema1::nextBaloon(float delta) {
	std::vector<Baloon> copyBaloon;

	for (int i = 0; i < baloon.size(); i++) {

		if (baloon[i].isAlive) {
			baloon[i].y += ballonStepUp * delta;
		}
		else {
			baloon[i].y -= ballonStepDown * delta;
			baloon[i].scale = max(baloonMinScale, baloon[i].scale - baloonScale * delta);
		}

		for (int j = 0; j < arrows.size(); j++) {
			float x = abs(arrows[j].x + arrowLen * cos(arrows[j].angle));
			float y = abs(arrows[j].y + arrowLen * sin(arrows[j].angle));

			if (checkColision(0, baloonScale * baloonRadiusY, x, baloon[i].x + baloonScale * baloonRadiusX, y, 
				baloon[i].y + baloonScale * baloonRadiusY) && baloon[i].isAlive) {
				if (baloon[i].isRed) {
					score += baloonBonus;
				}
				else {
					score = max(0, score - baloonPenalty);
				}
				baloon[i].isAlive = false;
			}

		}


		if ((!baloon[i].isAlive && baloon[i].y + 2 * baloonRadiusY * baloonScale > lowerBound)
			|| (baloon[i].isAlive && baloon[i].y - baloonRadiusY * baloonScale - baloonTail * baloonScale < resolution.y)) {
			copyBaloon.push_back(baloon[i]);
		}
	}

	baloon = copyBaloon;

	if (baloon.size() < nBaloon) {
		Baloon elem;
		elem.isAlive = true;
		elem.x = resolution.x / 3 - 2 * baloonRadiusX + (rand() % ( 2 * resolution.x / 3));
		elem.y = -(rand() % (2 * resolution.y));
		elem.isRed = rand() % 2;
		elem.scale = 1;
		baloon.push_back(elem);
	}
}

void Tema1::nextArrow(float delta) {
	std::vector<Arrow> copyArrows;

	for (int i = 0; i < arrows.size(); i++) {
		arrows[i].x += abs(delta * arrows[i].step * cos(arrows[i].angle));
		arrows[i].y += (delta * arrows[i].step * sin(arrows[i].angle));
		arrows[i].angle = max(-M_PI / 2, arrows[i].angle - 0.0025 * (defaultPower + maxPower) / arrows[i].step);

		if (arrows[i].x + 300 < resolution.x) {
			copyArrows.push_back(arrows[i]);
		}
	}

	arrows = copyArrows;
}

void Tema1::nextBuffs(float delta) {
	std::vector<Buff> copyBuffs;

	for (int i = 0; i < buffs.size(); i++) {
		if (!buffs[i].isAlive) {
			buffs[i].scale = max(0, buffs[i].scale - 0.05);
		}
		else {
			buffs[i].y += ballonStepUp * delta;
			buffs[i].x -= buffs[i].dx;
			buffs[i].dx = 40 * sin(buffs[i].y / 20);
			buffs[i].x += buffs[i].dx;
		}

		for (int j = 0; j < arrows.size(); j++) {
			float x = abs(arrows[j].x + arrowLen * cos(arrows[j].angle));
			float y = abs(arrows[j].y + arrowLen * sin(arrows[j].angle));

			if (checkColision(0, buffScale * buffRadius, x, buffs[i].x, y,
				buffs[i].y) && buffs[i].isAlive) {
				score += buffBonus;
				buffs[i].isAlive = false;

				if (buffs[i].type == 0) {
					arrowBuff = 5;
				}
				else if (buffs[i].type == 1) {
					alive = min(3, alive + 1);
				}
				else {
					moveBuff = 10;
				}
			}

		}

		if (buffs[i].scale != 0 || buffs[i].y + buffScale * buffRadius < resolution.y) {
			copyBuffs.push_back(buffs[i]);
		}
	}

	buffs = copyBuffs;

	if (buffs.size() < nBuff) {
		Buff elem;
		elem.isAlive = true;
		elem.dx = 0;
		elem.x = resolution.x / 3 - 2 * baloonRadiusX + (rand() % (2 * resolution.x / 3));
		elem.y = -(rand() % (2 * resolution.y));
		elem.type = rand() % 3;
		elem.scale = baloonScale;
		buffs.push_back(elem);
	}
}

void Tema1::Update(float deltaTimeSeconds)
{
	if (alive) {

		if (arrowBuff > 0) {
			delayConst = 0.75f;

			arrowBuff -= deltaTimeSeconds;

			if (arrowBuff < 0) {
				delayConst = 1.5f;
			}
		}
		if (moveBuff > 0) {
			playerStep = 150;

			moveBuff -= deltaTimeSeconds;

			if (moveBuff < 0) {
				playerStep = 100;
			}
		}


		time += deltaTimeSeconds;

		if (time > 10 && first) {
			first = false;
			cout << score << '\n';
		}
		else if (time > 20 && second) {
			second = false;
			cout << score << '\n';
		}
		else if (time > 30) {
			cout << score << '\n';

			nStar += 2;
			starStep += 20;
			ballonStepUp += 10;
			time = 0;
			first = true;
			second = true;
		}

		nextStar(deltaTimeSeconds);
		nextArrow(deltaTimeSeconds);
		nextBaloon(deltaTimeSeconds);
		nextBuffs(deltaTimeSeconds);

		if (onPress && power < maxPower) {
			power += 100 * deltaTimeSeconds;
		}

		if (delay > 0) {
			delay = max(0.0f, delay - deltaTimeSeconds);
		}

		angularStep += 4 * deltaTimeSeconds;
		starX -= starStep * deltaTimeSeconds;

		for (int i = 0; i < alive; i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Scale(2, 2) * modelMatrix;
			modelMatrix = Transform2D::Translate(50 + 50 * i, 20) * modelMatrix;
			RenderMesh2D(meshes["player1"], shaders["VertexColor"], modelMatrix);
		}

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Translate(-7, -5) * modelMatrix;
		modelMatrix = Transform2D::Rotate(arrowAngle) * modelMatrix;
		modelMatrix = Transform2D::Scale(scalePlayer, scalePlayer) * modelMatrix;
		RenderMesh2D(meshes["bow"], shaders["VertexColor"], modelMatrix);

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Scale(1 + (20 * power) / defaultPower, 1) * modelMatrix;
		modelMatrix = Transform2D::Translate(250, 25) * modelMatrix;
		RenderMesh2D(meshes["sqaure1"], shaders["VertexColor"], modelMatrix);

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Scale(1 + (20 * maxPower) / defaultPower, 1) * modelMatrix;
		modelMatrix = Transform2D::Translate(250, 25) * modelMatrix;
		RenderMesh2D(meshes["sqaure3"], shaders["VertexColor"], modelMatrix);

		modelMatrix = glm::mat3(1);
		RenderMesh2D(meshes["sqaure2"], shaders["VertexColor"], modelMatrix);

		for (int i = 0; i < buffs.size(); i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Scale(buffs[i].scale, buffs[i].scale) * modelMatrix;
			modelMatrix = Transform2D::Translate(buffs[i].x, buffs[i].y) * modelMatrix;

			if (buffs[i].type == 0) {
				RenderMesh2D(meshes["buff1"], shaders["VertexColor"], modelMatrix);
			}
			else if (buffs[i].type == 1) {
				RenderMesh2D(meshes["buff2"], shaders["VertexColor"], modelMatrix);
			}
			else {
				RenderMesh2D(meshes["buff3"], shaders["VertexColor"], modelMatrix);
			}
		}

		for (int i = 0; i < baloon.size(); i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Scale(baloonScale * baloon[i].scale, baloonScale) * modelMatrix;
			modelMatrix = Transform2D::Translate(baloon[i].x, baloon[i].y) * modelMatrix;

			if (baloon[i].isRed) {
				RenderMesh2D(meshes["baloon1"], shaders["VertexColor"], modelMatrix);
			}
			else {
				RenderMesh2D(meshes["baloon2"], shaders["VertexColor"], modelMatrix);
			}
		}


		for (int i = 0; i < star.size(); i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Translate(-starWidth / 2, -starWidth / 2) * modelMatrix;
			modelMatrix = Transform2D::Rotate(star[i].orientation * angularStep) * modelMatrix;
			modelMatrix = Transform2D::Scale(scaleStar, scaleStar) * modelMatrix;
			modelMatrix = Transform2D::Translate(star[i].x, star[i].y) * modelMatrix;
			RenderMesh2D(meshes["star1"], shaders["VertexColor"], modelMatrix);
		}

		if (!delay) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Scale(1.5, 1.5) * modelMatrix;
			modelMatrix = Transform2D::Rotate(arrowAngle) * modelMatrix;
			modelMatrix = Transform2D::Translate(playerX + arrowX, playerY + arrowY) * modelMatrix;
			RenderMesh2D(meshes["arrow1"], shaders["VertexColor"], modelMatrix);
		}

		for (int i = 0; i < arrows.size(); i++) {
			modelMatrix = glm::mat3(1);
			modelMatrix = Transform2D::Scale(1.5, 1.5) * modelMatrix;
			modelMatrix = Transform2D::Rotate(arrows[i].angle) * modelMatrix;
			modelMatrix = Transform2D::Translate(arrows[i].x, arrows[i].y) * modelMatrix;
			RenderMesh2D(meshes["arrow1"], shaders["VertexColor"], modelMatrix);
		}

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Rotate(arrowAngle) * modelMatrix;
		modelMatrix = Transform2D::Scale(scalePlayer, scalePlayer) * modelMatrix;
		modelMatrix = Transform2D::Translate(playerX + arrowX, playerY + arrowY) * modelMatrix;
		RenderMesh2D(meshes["bow"], shaders["VertexColor"], modelMatrix);

		modelMatrix = glm::mat3(1);
		modelMatrix = Transform2D::Scale(scalePlayer, scalePlayer) * modelMatrix;
		modelMatrix = Transform2D::Translate(playerX, playerY) * modelMatrix;
		RenderMesh2D(meshes["player1"], shaders["VertexColor"], modelMatrix);
	}
}

void Tema1::FrameEnd()
{

}

void Tema1::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_W) && alive) {
		if (playerY + playerStep * deltaTime + playerHeight > resolution.y - upperBound) {
			playerY = resolution.y - playerHeight - upperBound;
		}
		else {
			playerY += playerStep * deltaTime;
		}
	}
	if (window->KeyHold(GLFW_KEY_S) && alive) {
		if (playerY - playerStep * deltaTime < lowerBound) {
			playerY = lowerBound;
		}
		else {
			playerY -= playerStep * deltaTime;
		}
	}
}

void Tema1::OnKeyPress(int key, int mods)
{
	if (key == GLFW_KEY_R && !alive) {
		alive = 3;
		baloon = {};
		star = {};
		arrows = {};
		playerY = (float)resolution.y / 2;
		playerX = 15;
	}
}

void Tema1::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Tema1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	float a, b;
	if (alive) {
		a = mouseX - playerX - arrowX;
		b = resolution.y - mouseY - playerY - arrowY;
		arrowAngle = atan(b / a);
	}
}

void Tema1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_2 && !delay && alive) {
		onPress = true;
	}
}

void Tema1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_2 && alive) {
		onPress = false;

		if (!delay) {
			delay = delayConst;

			Arrow elem;
			elem.x = playerX + arrowX;
			elem.y = playerY + arrowY;
			elem.angle = arrowAngle;
			elem.step = defaultPower + power;

			arrows.push_back(elem);
		}
		power = 0;
	}
}

void Tema1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Tema1::OnWindowResize(int width, int height)
{
}

