#include "Laborator1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>

using namespace std;

// Order of function calling can be seen in "Source/Core/World.cpp::LoopUpdate()"
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/World.cpp

Laborator1::Laborator1()
{
}

Laborator1::~Laborator1()
{
}

void Laborator1::Init()
{
	// Load a mesh from file into GPU memory
	{
		this->isBlack = true;
		this->isActive = "box";
		this->x = 0.0f;
		this->y = 0.0f;
		this->z = 0.0f;
		this->v = 0.0f;
		this->g = 0.5f;

		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;

		Mesh* sphereMesh = new Mesh("sphere");
		sphereMesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[sphereMesh->GetMeshID()] = sphereMesh;

		Mesh* teapotMesh = new Mesh("teapot");
		teapotMesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "teapot.obj");
		meshes[teapotMesh->GetMeshID()] = teapotMesh;

		
	}
}

void Laborator1::FrameStart()
{

}

void Laborator1::Update(float deltaTimeSeconds)
{
	glm::ivec2 resolution = window->props.resolution;

	// sets the clear color for the color buffer
	
	if (this->isBlack) {
		glClearColor(0, 0, 0, 1);
	}
	else {
		glClearColor(0, 1, 0, 0);
	}

	// clears the color buffer (using the previously set color) and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);

	// render the object
	// RenderMesh(meshes["box"], glm::vec3(1, 0.5f, 0), glm::vec3(0.5f));

	// render the object again but with different properties
	// RenderMesh(meshes["teapot"], glm::vec3(-1, 0.5f, 0));

	// RenderMesh(meshes["sphere"], glm::vec3(0.15f, 0.5f, 0));

	this->v += -this->g * deltaTimeSeconds;

	if (this->y + this->v * deltaTimeSeconds > 0) {
		this->y += this->v * deltaTimeSeconds;
	}
	else {
		this->y = 0;
	}

	RenderMesh(meshes[this->isActive], glm::vec3(this->z, this->y, this->x), glm::vec3(0.5f));


}

void Laborator1::FrameEnd()
{
	DrawCoordinatSystem();
}

// Read the documentation of the following functions in: "Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator1::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_I)) {
		this->x -= 0.6f * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_K)) {
		this->x += 0.6f * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_L)) {
		this->z += 0.6f * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_J)) {
		this->z -= 0.6f * deltaTime;
	}
};

void Laborator1::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_F) {
		// do something
		this->isBlack = !this->isBlack;
	}
	if (key == GLFW_KEY_T) {
		// do something
		this->v = 1.5f;
	}
	if (key == GLFW_KEY_R) {
		// do something
		if (this->isActive == "box") {
			this->isActive = "sphere";
		}
		else if (this->isActive == "sphere") {
			this->isActive = "teapot";
		}
		else {
			this->isActive = "box";
		}
	}
};


void Laborator1::OnKeyRelease(int key, int mods)
{
	// add key release event
};

void Laborator1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
};

void Laborator1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
};

void Laborator1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
	// treat mouse scroll event
}

void Laborator1::OnWindowResize(int width, int height)
{
	// treat window resize event
}
