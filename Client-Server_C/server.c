#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>

#define BUFLEN 2000

// structura mesaj ce e trimis la subscriber
typedef struct message {
	int len;
	char *payload;
} message;

// structura mesaj primit de la udp
typedef struct message_str {
	char topic[50];
	unsigned char type;
	char payload[1500];
} message_str;

// structura topic pentru subscriber
typedef struct topic {
	char name[51];
	int sf;
} topic;

// structura user
typedef struct user_str {
	int fd;
	char *id;
	int n;
	int size;
	topic *topics;
	int active;
} user_str;

// lista de useri online
typedef struct online_user {
	int sf;
	user_str *user; 
	struct online_user *next;
} online_user;

// lista de useri offline (ajung doar cei cu sf 1)
typedef struct offline_user {
	int sf;
	user_str *user;
	int size;
	int n;
	int *len;
	message *mess; 
	struct offline_user *next;
} offline_user;

// hasmap de topicuri si cu useri abonati
typedef struct hashmap {
	char name[51];
	online_user *on_users;
	offline_user *off_users;
	struct hashmap *next;
} hashmap;

user_str **registered;
int reg_len = 0;
hashmap *app = NULL;

void printUsers() {
	int i;
	printf("\nsize: %d\n", reg_len);
	printf("users: ");
	for (i = 0; i < reg_len; i++) {
		printf("(%s, %d) ", registered[i]->id, registered[i]->active);
	}
	printf("\n\n");
}

void printMap() {
	hashmap *aux = app;

	printf("Hashmap:\n");
	while (aux) {

		printf("topic: %s\n", aux->name);
		
		printf("online users: ");
		online_user *online = aux->on_users;
		while (online) {
			printf("%s ", online->user->id);
			online = online->next;
		}
		printf("\n");

		printf("offline users: ");
		offline_user *offline = aux->off_users;
		while (offline) {
			printf("%s ", offline->user->id);
			offline = offline->next;
		}
		printf("\n");
		printf("\n");

		aux = aux->next;
	}
	printf("\n");
}

// functie ce trimite un mesaj unui user
void sent_to_user(user_str *user, message mess) {
	char *aux = malloc(sizeof(int) + mess.len + 1);
	memcpy(aux , &mess, sizeof(int));
	memcpy(aux + sizeof(int), mess.payload, mess.len + 1);
	send(user->fd, aux, sizeof(int) + mess.len + 1 , 0);
	free(aux);
}

// adaugarea unui nou topic in hashmap
int add_topic(char *name) {
	hashmap *node = malloc(sizeof(hashmap));

	if (node == NULL) {
		return -1;
	}

	strcpy(node->name, name);
	node->on_users = NULL;
	node->off_users = NULL;

	node->next = app;
	app = node;
	return 1;
}

// gasirea unui nod de topic in hashmap
hashmap *find_topic(char *name) {
	hashmap *aux = app;

	while (aux) {

		if (strcmp(name, aux->name) == 0) {
			return aux;
		}

		aux = aux->next;
	}

	return NULL;
}

int send_mess(message mess, char *name) {
	hashmap *node = find_topic(name);

	if (!node) {
		return -1;
	}

	offline_user *offline = node->off_users;
	online_user  *online = node->on_users;

	while (online) {
		sent_to_user(online->user, mess);
		online = online->next;
	}

	while (offline) {
		offline->n++;

		if (offline->n > offline->size) {
			offline->size *= 2;
			offline->mess = realloc(offline->mess, offline->size * sizeof(message));
		}

		offline->mess[offline->n - 1] = mess;

		offline = offline->next;
	}

	return 1;
}

// verifica daca un utilizator exista si starea lui
int check_exist(char *id) {
	int i;

	for (i = 0; i < reg_len; i++) {
		if (strcmp(registered[i]->id, id) == 0) {
			if (registered[i]->active == 0) {
				return 1;
			}
			else {
				return -1;
			}
		}
	}
	return 0;
}

// gaseste un user dupa id
user_str *get_userId(char *id) {
	int i;

	for (i = 0; i < reg_len; i++) {
		if (strcmp(registered[i]->id, id) == 0) {
			return registered[i];
		}
	}
	return NULL;
}

// gaseste un user dupa file descriptor
user_str *get_userFd(int fd) {
	int i;

	for (i = 0; i < reg_len; i++) {
		if (registered[i]->fd == fd) {
			return registered[i];
		}
	}
	return NULL;
}

// inregistreaza un user
int reg_user(char *id, int fd) {
	user_str *user = malloc(sizeof(user_str));

	if (!user) {
		return -1;
	}
	
	user->fd = fd;
	user->id = malloc(strlen(id) + 1);
	memcpy(user->id, id, strlen(id) + 1);
	user->n = 0;
	user->size = 10;
	user->topics = malloc(user->size * sizeof(topic));
	user->active = 1;

	reg_len++;
	registered = realloc(registered, reg_len * sizeof(user_str));
	registered[reg_len - 1] = user;

	return 1;
}

// schimba starea din inactiv in activ
// il adauga in lista de useri activi pentru topicuri
void rec_user(char *id, int fd) {
	int i, j;
	hashmap *aux = NULL;
	user_str *user = get_userId(id);

	user->active = 1;
	user->fd = fd;

	for (i = 0; i < user->n; i++) {
		// il intorc in lista de useri activi aux->online
		aux = find_topic(user->topics[i].name);
		online_user *online = malloc(sizeof(online_user));
		online->user = user;
		online->next = aux->on_users;
		aux->on_users = online;

		if (user->topics[i].sf) {
			// caz cand trebui transmise mesajele

			offline_user *parent = NULL;
			offline_user *offline = aux->off_users;

			while (offline && user != offline->user) {
				parent = offline;
				offline = offline->next;
			} 

			for (j = 0; j < offline->n; j++) {
				sent_to_user(user, offline->mess[j]);
			}

			// il sterg din aux->offline
			if (parent) {
				parent->next = offline->next;
			}
			else {
				aux->off_users = offline->next;
			}

			free(offline);
		}
	}
}

// schimba starea din activ in inactiv
// il adauga in lista de useri offline pentru topicuri cu sf
void desc_user(char *id) {
	int i, j;
	user_str *user = get_userId(id);
	hashmap *aux = NULL;

	user->active = 0;

	for (i = 0; i < user->n; i++) {
		aux = find_topic(user->topics[i].name);
		
		// adaug in aux->offline
		if (user->topics[i].sf) {
			offline_user *offline = malloc(sizeof(offline_user));
			offline->user = user;
			offline->size = 10;
			offline->n = 0;
			offline->len = NULL;
			offline->mess = malloc(offline->size * sizeof(message));
			offline->next = aux->off_users;
			aux->off_users = offline;
		}
		
		// sterg din useri activi aux->online
		online_user *parent = NULL;
		online_user *online = aux->on_users;

		while (online && online->user != user) {
			parent = online;
			online = online->next;
		}

		if (parent) {
			parent->next = online->next;
		}
		else {
			aux->on_users = online->next;
		}	

		free(online);
	}
}

// abonarea unui user la topic
int subscribe(user_str *user, char *name, int sf) {
	int i;

	for (i = 0; i < user->n; i++) {
		if (strcmp(name, user->topics[i].name) == 0) {
			return -1;
		}
	}
	
	user->n++;

	if (user->n > user->size) {
		user->size *= 2;
		user->topics = realloc(user->topics, user->size * sizeof(topic));
	
		if (user->topics == NULL) {
			return -1;
		}

	}

	strcpy(user->topics[user->n - 1].name, name);
	user->topics[user->n - 1].sf = sf;

	hashmap *node = find_topic(name);
	online_user *online = malloc(sizeof(online_user));
	online->user = user; 
	online->sf = sf;
	online->next = node->on_users;
	node->on_users = online;

	return 1;
}

// dezabonarea unui user la topic
int unsubscribe(user_str *user, char *name) {
	int i = 0, j;

	while (i != user->n && strcmp(name, user->topics[i].name) != 0) {
		i++;
	}

	if (i == user->n) {
		return -1;
	}
	else {
		for (j = i + 1; j < user->n; j++) {
			user->topics[j - 1] = user->topics[j]; 
		}

		user->n--;
	}

	hashmap *node = find_topic(name);
	online_user *online = node->on_users;
	online_user *parent = NULL;

	while (online && online->user != user) {
		parent = online;
		online = online->next;
	}

	if (parent) {
		parent->next = online->next;
	}
	else {
		node->on_users = online->next;
	}	

	return 1;
}

// se schimba mesajul de la client udp in forma pentru cel tcp
message get_message(message_str *rmes, struct sockaddr_in sender) {
	char name[51];

	if (rmes->topic[49] != 0) {
		strncpy(name, rmes->topic, 50);
		name[50] = '\0';
	}
	else {
		strcpy(name, rmes->topic);
	}

	message mess;
	mess.payload = calloc(2000, sizeof(char));
	char out[1000];

	if (rmes->type == 0) {
		uint32_t n;
        memcpy(&n, rmes->payload + 1, 4);
        n = ntohl(n);

        if (rmes->payload[0] == 1) {
        	n *= -1;
        }
		
		sprintf(mess.payload, "%s:%d - %s - INT - %d\n",
			inet_ntoa(sender.sin_addr), ntohs(sender.sin_port), name, n);

		mess.len = strlen(mess.payload);
	}
	else if (rmes->type == 1) {
		uint16_t n;
        float aux;
        memcpy(&n, rmes->payload, 2);
        n = ntohs(n);
        aux = 100;

		sprintf(mess.payload, "%s:%d - %s - SHORT_REAL - %0.2f\n",
			inet_ntoa(sender.sin_addr), ntohs(sender.sin_port), name, n / aux);
		mess.len = strlen(mess.payload);
	}
	else if (rmes->type == 2) {
		uint32_t n;
        uint8_t tmp;
        memcpy(&n, rmes->payload + 1, 4);
        memcpy(&tmp, rmes->payload + 5, 1);
        n = ntohl(n);
        float x = pow(10, tmp);
    	int aux = 1;
        if (rmes->payload[0] == 1) {
        	sprintf(mess.payload, "%s:%d - %s - FLOAT - -%0.4f\n",
				inet_ntoa(sender.sin_addr), ntohs(sender.sin_port), name, n / x);
        }
        else {
			sprintf(mess.payload, "%s:%d - %s - FLOAT - %0.4f\n",
				inet_ntoa(sender.sin_addr), ntohs(sender.sin_port), name, n / x);
        }

		mess.len = strlen(mess.payload);
	}
	else if (rmes->type == 3) {
		char aux[1501];
		if (rmes->payload[1499] != '\0') {
			strncpy(aux, rmes->payload, 1500);
			aux[1500] = '\0';
		}
		else {
			strcpy(aux, rmes->payload);
		}

		sprintf(mess.payload, "%s:%d - %s - STRING - %s\n",
			inet_ntoa(sender.sin_addr), ntohs(sender.sin_port), name, aux);
		mess.len = strlen(mess.payload);
	}

	return mess;
} 

int main(int argc, char *argv[]) {
	int tcp_fd, udp_fd, newsockfd, portno;
	int stat;
	char buffer[BUFLEN];
	char feedback[BUFLEN];
	struct sockaddr_in serv_addr, cli_addr;
	int n, i, ret;
	socklen_t clilen;

	fd_set read_fds;
	fd_set tmp_fd;
	int fdmax;

	if (argc < 2) {
		return -1;
	}

	// se goleste multimea de descriptori de citire (read_fds) si multimea temporara (tmp_fd)
	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fd);

	// initializare sucketi udp si tcp
	tcp_fd = socket(AF_INET, SOCK_STREAM, 0);
	udp_fd = socket(AF_INET, SOCK_DGRAM, 0);

	portno = atoi(argv[1]);	

	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(portno);
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	// bind fd
	ret = bind(tcp_fd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr));
	ret = bind(udp_fd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr));

	ret = listen(tcp_fd, 20);

	if (tcp_fd > udp_fd) {
		fdmax = tcp_fd;
	}
	else {
		fdmax = udp_fd;
	}

	// se adauga noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
	FD_SET(tcp_fd, &read_fds);
	FD_SET(udp_fd, &read_fds);
	FD_SET(0, &read_fds);
	int loop = 1;
	while (loop) {
		tmp_fd = read_fds; 
		
		ret = select(fdmax + 1, &tmp_fd, NULL, NULL, NULL);

		for (i = 0; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fd)) {
				// caz conectare tcp
				if (i == tcp_fd) {
					// se accepta conexiune de pe socketul de listen
					clilen = sizeof(cli_addr);
					newsockfd = accept(tcp_fd, (struct sockaddr *) &cli_addr, &clilen);

					// se adauga noul socket la multimea descriptorilor de citire
					FD_SET(newsockfd, &read_fds);
					if (newsockfd > fdmax) { 
						fdmax = newsockfd;
					}

					// se primeste mesajul cu id-ul asociat filedescriptorului
					memset(buffer, 0, BUFLEN);
					recv(newsockfd, buffer, sizeof(buffer), 0);

					stat = check_exist(buffer);

					if (stat == 0) {
						printf ("New client %s connected from %s:%d.\n", 
							buffer, inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
						reg_user(buffer, newsockfd);
					}
					else if (stat == 1) {
						printf ("New client %s connected from %s:%d.\n", 
							buffer, inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
						rec_user(buffer, newsockfd);
					}
					else {
						send(newsockfd , "exit", 5, 0);
						close(newsockfd);
						FD_CLR(newsockfd, &read_fds);
					}

				} 
				// caz conectare udp
				else if (i == udp_fd) {
					struct sockaddr_in sender;
					socklen_t sock_size = sizeof(struct sockaddr);

					memset(buffer, 0, BUFLEN);
					recvfrom(i, buffer, BUFLEN, 0, (struct sockaddr *)(&sender), &sock_size);

					message_str *rmes = (message_str *)buffer;
					message mess = get_message(rmes, sender);
					send_mess(mess, rmes->topic);
				}
				// caz input de la tastatura
				else if (i == 0) {
			  		// se citeste de la tastatura
					memset(buffer, 0, BUFLEN);
					fgets(buffer, BUFLEN - 1, stdin);

					int j;


					if (strcmp(buffer, "exit\n") == 0) {
						for (j = 0; j < reg_len; j++) {
							send(registered[j]->fd , buffer, strlen(buffer), 0);
							close(registered[j]->fd);
							FD_CLR(registered[j]->fd, &read_fds);
						}

						loop = 0;
						break;
					}
				}
				// caz subscribe/unsubscribe/inchidere client
				else {
					// s-au primit date pe unul din socketii de client,
					// asa ca serverul trebuie sa le receptioneze
					memset(buffer, 0, BUFLEN);
					n = recv(i, buffer, sizeof(buffer), 0);

					if (n == 0) {
						// conexiunea s-a inchis
						printf ("Client %s disconnected.\n", get_userFd(i)->id);
						desc_user(get_userFd(i)->id);
						close(i);
						FD_CLR(i, &read_fds);
						
						// se scoate din multimea de citire socketul inchis 
					} else {
						char *command, *topic_name, *sf_val;
						int sf_aux;

						if (strstr(buffer, "subscribe") == buffer) {
							command = strtok(buffer, " \n");
							topic_name = strtok(NULL, " \n");
							sf_val = strtok(NULL, " \n");
							
							if (!topic_name || !sf_val || strlen(sf_val) != 1 
								|| !(sf_val[0] - '0' == 0 || sf_val[0] - '0' == 1)) {
								continue;
							}
							
							sf_aux = atoi(sf_val);

							if (!find_topic(topic_name)) {
								add_topic(topic_name);
							}

							subscribe(get_userFd(i), topic_name, sf_aux);
							sprintf(feedback, "subscribed %s.\n", topic_name);
							send(i, feedback, strlen(feedback) + 1, 0);
						}
						else if (strstr(buffer, "unsubscribe") == buffer) {
							command = strtok(buffer, " \n");
							topic_name = strtok(NULL, " \n");


							if (!topic_name || !find_topic(topic_name)) {
								continue;
							}
							unsubscribe(get_userFd(i), topic_name);
							sprintf(feedback, "unsubscribed %s.\n", topic_name);
							send(i, feedback, strlen(feedback) + 1, 0);

						}
						else {
							continue;
						}
					}
				}
			}
		}
	}

	close(tcp_fd);
	close(udp_fd);

	return 0;
}