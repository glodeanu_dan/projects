#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFLEN 2000

typedef struct message {
	int len;
	char *payload;
} message;

int main(int argc, char *argv[]) {
	int sockfd, n, ret;
	struct sockaddr_in serv_addr;
	char buffer[BUFLEN];

	if (argc < 4) {
		return -1;
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[3]));
	ret = inet_aton(argv[2], &serv_addr.sin_addr);

	// ma conectez la server
	ret = connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));
	// trimit primul mesaj cu id-ul meu
	send(sockfd, argv[1], strlen(argv[1]) + 1, 0);

	fd_set read_fds;
    fd_set tmp_fds;
    FD_SET(sockfd, &read_fds);

    // 0 pentru ca citim de la tastatura
    FD_SET(0, &read_fds);

    int fdmax = sockfd;
    int loop = 1;

	while (loop) {

		tmp_fds = read_fds;
    	select(fdmax + 1, &tmp_fds, NULL, NULL, NULL);

    	int i = 0;

    	for(i = 0; i <= fdmax; i++) {
    		if (FD_ISSET(i, &tmp_fds)) {
				if (i == 0) {

			  		// se citeste de la tastatura
					memset(buffer, 0, BUFLEN);
					fgets(buffer, BUFLEN - 1, stdin);

					if (strncmp(buffer, "exit", 4) == 0) {
						loop = 0;
						break;
					}

					// se trimite mesaj la server
					n = send(sockfd, buffer, strlen(buffer), 0);
					memset(buffer, 0, BUFLEN);
    				n = recv(sockfd, buffer, sizeof(buffer), 0);

    				if (n > 0) {
    					printf("%s", buffer);
    				}

				}
				else {
    				memset(buffer, 0, BUFLEN);
    				int aux = recv(i, buffer, sizeof(buffer), 0);

    				// daca e exit inchid programul
					if (strncmp(buffer, "exit", 4) == 0) {
						loop = 0;
						break;
					}

					char *mess = buffer;
					int *len;
					int aux_len = 0;

					// afisez mesajul
					while (aux_len < aux) {

						aux_len += 4;
						len = (int *)mess;

						if (aux == 0) {
							break;
						}

						printf("%s", mess + 4);
						aux_len += strlen(mess + 4) + 1;
						mess = buffer + aux_len;
					}
    			}
    		}
    	}

	}

	close(sockfd);

	return 0;
}
