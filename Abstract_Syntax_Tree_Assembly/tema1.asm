%include "includes/io.inc"

extern getAST
extern freeAST
extern free
extern calloc
extern realloc

section .data
    difference db "-", 0
    addition db "+", 0
    multiplication db "*", 0
    division db "/", 0
    isok db "this node is ok", 0
    check db "check", 0
    true db 0
    is_negative dd 0
    length dd 0
    adress dd 0
    num1 dd 0
    num2 dd 0
    address dd 0
    the_string db "                                           ", 0

section .bss
    ; La aceasta adresa, scheletul stocheaza radacina arborelui
    root: resd 1

section .text
global main
main:
    mov ebp, esp; for correct debugging
    ; NU MODIFICATI
    push ebp
    mov ebp, esp
    
    ; Se citeste arborele si se scrie la adresa indicata mai sus
    call getAST
    mov [root], eax
    
    ; Implementati rezolvarea aici:
    xor ecx, ecx
    push DWORD [root]    
    
    ; parcurg arborele in preordine
    push_nodes:
        ; stochez in ecx un nod
        pop ecx
        mov edi, [ecx]
        
        ; daca in nod este numar doar adaug copii
        cmp BYTE [edi + 1], 0 
        jne skip
        cmp BYTE [edi], 47
        jg skip
        
        ; se verifica daca copii sunt numere
        mov BYTE [true], 1
    
        ; copilul din stanga cazurile numarului pozitiv si negativ fiind tratate aparte
        cmp BYTE [ecx + 4], 0
        je null_child
        mov edi, [ecx + 4]
        mov edi, [edi]
        cmp BYTE [edi], 48
        jge first_ok
            cmp BYTE [edi + 1], 48
            jge first_ok
            mov BYTE [true], 0
        first_ok:
        
        ; copilul din dreapta cazurile numarului pozitiv si negativ fiind tratate aparte
        mov edi, [ecx + 8]
        mov edi, [edi]
        cmp BYTE [edi], 48
        jge second_ok
            cmp BYTE [edi + 1], 48
            jge second_ok
            mov BYTE [true], 0
        second_ok:
           
        jmp not_null_child
        null_child:
            mov BYTE [true], 0
        not_null_child:
        ; se termina de verificat daca copii sunt numere
        
        ; daca nodul are doi copii numere
        cmp BYTE [true], 0
        je next

            
            ; daca nodul are doi copii cu stringuri ce reprezinta numere, calculez acele numere

           
            ; ma asigur ca variabilele sunt contin valorile dorite
            ; pentru a fi mai intuitiv am bagat in esi numarul ce reprezinta copilul, 4 pentru stanga
            mov DWORD [num1], 0
            mov DWORD [is_negative], 0
            mov esi, 4
            
            ; calculez cat de lung e sirul din nod
            mov al, 0      
            mov ebx, [ecx + esi]
            mov edi, [ebx]
            
            ;se verifica daca numarul are semn
            cmp BYTE [edi], 46
            jge over_first
                mov DWORD [is_negative], 1
            over_first:
            
            ; termin de calculat lungime si o stochez in length
            push DWORD ecx
            repne scasb  
            pop ecx
            sub edi, [ebx] 
            dec edi
            sub edi, [is_negative]
            mov [length], edi
            
            dec edi
            xor ebx, ebx
            compute_first_num:
                ; stochez in al o cifra din numar luand in considerare si faptul ca numarul poate fi negativ
                xor eax, eax
                mov edx, [length]
                dec edx
                mov ebx, [ecx + esi]
                mov ebx, [ebx]
                add ebx, [is_negative]
                add ebx, edi
                sub ebx, edx
                mov al, BYTE [ebx]
                sub eax, 48
                    
                    ; inmultesc numarul cu 10 in conformitate cu pozitia din string
                    mov edx, [length]
                    dec edx
                    jmp check1
                    multiply_by_ten_first:
                        mov ebx, 10
                        push edx
                        mul ebx
                        pop edx
                        dec edx
                        check1:
                        cmp edx, 0
                    jnz multiply_by_ten_first
                                    
                ; dupa loop-ul precedent obtin cifra inmultita cu o putere a lui 10, o adaug la num1
                add DWORD [num1], eax
                dec DWORD [length]
                cmp DWORD [length], 0
            jnz compute_first_num
            
            ; adaug semnul in cazul in care numarul e negativ
            cmp DWORD [is_negative], 0
            je positive_first
                mov eax, [num1]
                mov ebx, -1
                mul ebx
                mov [num1], eax
            positive_first:

            ; ma asigur ca variabilele sunt contin valorile dorite
            ; pentru a fi mai intuitiv am bagat in esi numarul ce reprezinta copilul, 8 pentru stanga
            mov DWORD [num2], 0
            mov DWORD [is_negative], 0
            mov esi, 8
            
            ; calculez cat de lung e sirul din nod
            mov al, 0      
            mov ebx, [ecx + esi]
            mov edi, [ebx]
            
            ;se verifica daca numarul are semn
            cmp BYTE [edi], 46
            jge over_second
                mov DWORD [is_negative], 1
            over_second:
            
            ; termin de calculat lungime si o stochez in length
            push DWORD ecx
            repne scasb  
            pop ecx
            sub edi, [ebx] 
            dec edi
            sub edi, [is_negative]
            mov [length], edi
            
            dec edi
            xor ebx, ebx
            compute_second_num:
                ; stochez in al o cifra din numar luand in considerare si faptul ca numarul poate fi negativ
                xor eax, eax
                mov edx, [length]
                dec edx
                mov ebx, [ecx + esi]
                mov ebx, [ebx]
                add ebx, edi
                sub ebx, edx
                add ebx, [is_negative]
                mov al, BYTE [ebx]
                sub eax, 48
                    
                    ; inmultesc numarul cu 10 in conformitate cu pozitia din string
                    mov edx, [length]
                    dec edx
                    jmp check2
                    multiply_by_ten_second:
                        mov ebx, 10
                        push edx
                        mul ebx
                        pop edx
                        dec edx
                        check2:
                        cmp edx, 0
                    jnz multiply_by_ten_second
                                    
                ; dupa loop-ul precedent obtin cifra inmultita cu o putere a lui 10, o adaug la num1
                add DWORD [num2], eax
                dec DWORD [length]
                cmp DWORD [length], 0
            jnz compute_second_num
            
            ; adaug semnul in cazul in care numarul e negativ
            cmp DWORD [is_negative], 0
            je positive_second
                mov eax, [num2]
                mov ebx, -1
                mul ebx
                mov [num2], eax
            positive_second:

            ; mut in bl semnul
            xor eax, eax
            mov edi, [ecx]
            mov bl, BYTE [edi]
            
            ; caz scadere
            cmp BYTE [difference], bl
            jnz not_difference
                mov eax, [num1]
                sub eax, [num2]
            not_difference:
            
            ; caz adunar
            cmp BYTE [addition], bl
            jnz not_addition
                mov eax, [num1]
                add eax, [num2]
            not_addition:
            
            ; caz inmultire
            cmp BYTE [multiplication], bl
            jnz not_multiplication
                xor edx, edx
                mov eax, [num1]
                mov edi, [num2]
                imul edi
                xor edx, edx
            not_multiplication:
            
            ; caz impartire
            cmp BYTE [division], bl
            jnz not_division
                xor edx, edx
                mov eax, [num1]
                cmp eax, 0
                jge edx_not_set
                    mov edx, -1
                edx_not_set:
                mov edi, [num2]
                idiv edi
                xor edx, edx
            not_division:
            
            mov [address], ecx
            mov edi, [ecx]
            
            ; salvez valorile
            push ecx
            push eax
            mov bl, [difference]
            
            ; verific daca am rezultat negativ pentru a pune semnul in string
            xor edi, edi
            cmp eax, 0
            jge positive_number
                mov [the_string], bl
                mov ebx, -1
                mul ebx
                mov edi, 1
            positive_number:
            
            push eax
            xor ecx, ecx
            mov ebx, 10
            
            ; calculez lungimea rezultatului
            compute_length:
                xor edx, edx
                inc ecx
                div ebx
                cmp eax, 0
            jne compute_length

            pop eax  
			; salvez lungimea la adresa length    
            mov DWORD [length], ecx
            add DWORD [length], edi
            
            ; creez un string ce are cifrele dintr-un numar
            int_to_char:
                xor edx, edx
                div ebx
                add dl, 48
                mov [the_string + ecx + edi - 1], dl
            loop int_to_char
            
            ; scot valorile salvate
            pop eax
            pop ecx
            mov edi, [ecx]
            
            ; realoc stringul din nod conform cu noua lungime
            push DWORD [length]
            push DWORD [ecx]
            call realloc
            add esp, 8
            
            ; transfer stringul rezultatului la adresa realocata
            mov ecx, DWORD [length]
            mov DWORD [eax + ecx], 0
            copy:
                mov bl, [the_string + ecx - 1]
                mov [eax + ecx - 1], bl
            loop copy
               
            ; inlocuiesc stringul cu rezultat si cel al operatiei
            mov ebx, [address] 
            mov [ebx], eax
            
            ; adaug din nou radacina si golesc stiva
            mov ebp, esp
            push DWORD [root]   
            jmp skip        
            
        next:
        
        ; se parcurge arborele RSD
        ; adaug nodul drept
        cmp DWORD [ecx + 8], 0
        je empty_right
            push DWORD [ecx + 8]
        empty_right:
        
        ; adaug nodul stang
        cmp DWORD [ecx + 4], 0
        je empty_left
            push DWORD [ecx + 4]
        empty_left:
        
        skip:
        cmp esp, ebp
    jne push_nodes
    
    ; mut in eax adresa stringului din root
    mov eax, [root]
    mov eax, [eax]
    
    ; afisez rezultatul din nodul radacina
    PRINT_STRING [eax]
        
    ; NU MODIFICATI
    ; Se elibereaza memoria alocata pentru arbore
    push dword [root]
    call freeAST
    
    xor eax, eax
    leave
    ret