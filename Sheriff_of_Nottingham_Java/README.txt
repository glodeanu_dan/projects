TEMA 1 POO --SHERIFF OF NOTTINGHAM--
NUME: GLODEANU DAN
GRUPA: 321CD

CLASE:

Bag: este un obiect detinut de fiecare jucator, este folosit atunci cand 
	 jucatorul se comporta ca un comerciant.

	Campuri:
		LinkedList<Goods> cards: cartile din sac.
		int bribe: mita pusain sac.
		int id: intreg ceindeica id-ul bunurilor din sac.

	Metode:

		Bag(List<Goods> cards, int bribe, int id): constructor ce seteaza
			camputile obiectului.

		boolean checkBag(): verifica daca un sac contine doar bunuri legale si
			declarate.

		int getPenalty(): returneaza amenda pentru toate incalcarile din sac.

		void removeIllegals(LinkedList<Goods> inputCards): sterge bunurile 
			ilegale si nedeclarate din sac.

		int getProfit(): returneaza profitul pentru toate cartile din sac.

		int payPenalty(): returneaza amenda pentru serif atunci cand a 
			verificat uncomerciant sincer.

		getters: int getBribe(), LinkedList<Goods> getCards().




BasicPlayer: este un tip de baza pentru jucatori astfel el este si parinte
			 si pentru ceilalti doi jucatori (Bribrd si Greedy), astfel el
			 toate campurile si majoritatea metodelor care sunt comune pentru 
			 toti jucatorii.

	Clase:
		GoodsWithKey: reprezinta un nod ce contine un bun si o cheie, care e   		
			folosita pentru a sorta elementele.

		GoodsComparator: implementeaza interfata de comparare a 
			Comparator<GoodsWithKey>.


	Campuri:
		int index: indica al catalea a fost jucatorul la inceput.
	    int iter: indica numarul rundei curente.
	    int coins: pastreaza numarul de monede.
	    boolean isLast: indica da jucatorul este ultimul din lista de input.
	    boolean canCheck: indeica capacitatea jucatorului de a vreifica sacii 
	    	cu bunuri a celorlalti jucatori atunci cand este sherif.
	    String type: pastreaza tipul jucatorului.
	    Bag bag: sacul cu bunuri.
	    LinkedList<Goods> cards: o lista cu 10 carti ce le are jucatorul in
	     	mana.
	    GoodsFactory factory: factory de bunuri, necesar mai multor metode
	    	din clasa.
	    HashMap<Integer, Integer> totalGoods: stocheaza toate bunurile legale
	    	aduse la taraba.
	    HashMap<Integer, Integer> totalIllegalGoods: stocheaza toate bunurile
	     	ilegale aduse la taraba.
	    Constants constants = new Constants();

	Metode:

		BasicPlayer(int index): constructor ce instantiaza campurile.

		void getCards(LinkedList<Goods> inputCards): adauga 10 carti din input
			in campul cards al jucatorului.

		void payCoins(int coins): adauga monede jucatorului.

		void takeCoins(int coins): ii ia monede unui jucator.

		ArrayList<Goods> selectCards(): selecteaza cartile potrivite din campul 
			cards  pentru al jucatorului si returneaza aceasta lista.

		int putBribe(): returneaza mita care e pusa de acest tip de jucator.

		void makeBag(): creeaza sacul cu bunuri din lista de carti generata de
			ArrayList<Goods> selectCards().

		void checkBag(BasicPlayer player, LinkedList<Goods> inputCards):
		 	atunci cand este sherif verifica un alt jucator.

		void putOnStall(): pune cartile care au trecut verificarea serifului
		 	pe taraba, tinand cont ca o carte ilegala adauga si carti legale.
		 	acestea sunt adaugate in totalGoods, respectiv totalIllegalGoods.

		void sellItems(): vinde bunurile acumulate pe parcursul jocului.

		kingBonus(int id): ii adauga jucatorului bonusul de rege pentru un 
			anumit tip de carte.

		queenBonus(int id): ii adauga jucatorului bonusul de regina pentru un 
			anumit tip de carte.

		void setCanCheck(): verifica daca nu cumva pe parcursul turului de serif 
			jucatorul nu ramane cu mai putin de 16 monede si nu mai poate
			executa verificarea celorlalti jucatori.

		void resetCanCheck(): resteteaza valoarea campului canCheck la finalul
			turului de serif.

		setters: void setIter(int iter), void setIsLast(boolean isLast).

		getters: int getIndex(), Bag getBag(), int getCoins().

		String toString()

GreedyPlayer: acesta extinde clasa BasicPlayer, modificand comportamentul 
			  acestuia. In particular se suprascriu metodele checkBag si 
			  makeBag, restul functionalitatilor se pastreaza. Consructorul
			  apeleaza constructorul pentru BasicPlayer dupa care campul 
			  type e suprascris.

BribedPlyer: acest jucator, de asemenea, il extinde pe BasicPlayer. Suprascrie
			 aceleasi metode ca si GreedyPlayer, insa ma are o metoda in plus
			 int putBribe(int num), care pune mita in dependenta de numarul de
			 carti ilegale.

KingQueenFinder: este un obiect ce este folosit pentru a gasi cine este King si
				 Queen pentru fiecare tip de carte.

	Clase: 
		Node: contine jucatorul king si queen pentru o carte, si de asemenea 
		numarul de carti pentru acei jucatori.

	Campuri: 
		HashMap<Integer, Node> allGoods: cheia este id-ul bunului legal, iar
			valoarea este un nod pentru acea carte.

	Metode:

		void setNode(BasicPlayer player): parcurge lista de jucatori si gaseste
			jucatorii care trebuie sa primeasca Queen si King bonus.

		void giveBonuses(): daca un jucator a fost selectat acestuia i se 
			adauga bonusul.

GameRunner: este clasa in care are loc efectiv jocul, aceasta preia din main 
			datele de input si pe baza acestora creeaza jucatorii si cartile
			din joc.

	Campuri:
	ArrayList<BasicPlayer> players: o lista cu jucatori,
    LinkedList<Goods> inputCards: lista cu cardurile din joc.
    GoodsFactory factory: un factory ce il folosesc in mai multe metode.
    int rounds: numarul de runde
 
 	Metode:

 		GameRunner(GameInput gameInput): constructor ce primeste ca parametru
 			o variabila de tip GameInput si cu ajutorul acesteia creeaza 
 			jucatorii si cardurile.

 		void runGame(): ruleaza jocul, astfel in fiecare runda fiecare jucator 
 			este pe rand sherif, pe cand restul sunt comercianti. 
 			Etapele unei subrunde sunt:
 				* sheriful este verificat daca va putea sa verifice jucatori 
 				  in continuare.
 				* comerciantul isi ia 10 carti.
 				* comerciantul isi creeaza sacul.
 				* sheriful verifica sacul comerciantului.
 				* comerciantul isi pune bunurile pe taraba.
 			La finalul unei subrunde capacitate sherifului de a verifica alti 
 			jucatori e resetata. Dupa ce s-au jucat toate rundele fiecare 
 			jucator isi vinde bunurile si se gasesc jucatorii care vor lua
 			King si Queen bonus.

 		void sortPlayers(): sorteaza jucatorii in functie de monede, daca sunt
 			jucatori cu acelas scor se sorteaza si in functie de pozitia
 			initiala.

 		void printResults(): printeaza jucatorii in ordinea scorului lor.