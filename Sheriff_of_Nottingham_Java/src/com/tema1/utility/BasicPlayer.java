package com.tema1.utility;

import com.tema1.common.Constants;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;
import com.tema1.goods.GoodsType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class BasicPlayer {
    protected int index;
    protected int iter;
    protected int coins;
    protected boolean isLast;
    protected boolean canCheck;
    protected String type;
    protected Bag bag;
    protected Constants constants = new Constants();
    protected LinkedList<Goods> cards;
    protected GoodsFactory factory = GoodsFactory.getInstance();
    protected HashMap<Integer, Integer> totalGoods;
    protected HashMap<Integer, Integer> totalIllegalGoods;

    // constructor pentru un obiectde tip BasicPlayer
    public BasicPlayer(int index) {
        canCheck = true;
        coins = Constants.START_COINS;
        this.isLast = false;
        cards = new LinkedList<Goods>();
        type = "BASIC";
        iter = 0;
        this.index = index;
        totalGoods = new HashMap<Integer, Integer>();
        totalIllegalGoods = new HashMap<Integer, Integer>();
    }

    // metoda ce adauga 10 carti din cartile de input in cartile din mana jucatorului
    final void getCards(LinkedList<Goods> inputCards) {
        for (int i = 0; i < Constants.CARDS_HAND_MAX; i++) {
            this.cards.add(inputCards.poll());
        }
    }

    // clasa interna cu interfata de comparator folosita pentru a sorta vectori de carti
    private class GoodsComparator implements Comparator<GoodsWithKey> {

        public int compare(GoodsWithKey first, GoodsWithKey second) {
            return second.key.compareTo(first.key);
        }
    }

    // metoda ce ii aduaga monede unui jucator
    final void payCoins(int penalty) {
        this.coins += penalty;
    }

    // metoda ce ii ia monede unui jucator
    final void takeCoins(int penalty) {
        this.coins -= penalty;
    }

    // clasa interna ce reprezinta un nod format dintr-o cheie si un bun
    private class GoodsWithKey implements Comparable<GoodsWithKey> {
        private Goods good;
        private Integer key;

        // constructor clasa, initial cheia fiind frecventa bunului
        GoodsWithKey(Goods good, int keyFreq) {
            this.good = good;
            this.key = keyFreq;
        }

        // seteaza cheia ca profitul cartii
        private void setKeyValue() {
            this.key = this.good.getProfit();
        }

        // seteaza cheia ca indexul bunului
        private void setKeyIndex() {
            this.key = this.good.getId();
        }

        // metoda pentru a compara doua astfel de obiecte
        @Override
        public int compareTo(GoodsWithKey other) {
            return this.key - other.key;
        }
    }

    // metoda ce selecteaza cartile pentru sacul acestui tip de jucator
    final ArrayList<Goods> selectCards() {
        ArrayList<Goods> goods = new ArrayList<Goods>();
        LinkedList<Goods> legal = new LinkedList<Goods>();

        // selecteaza doar cartile legale
        for (Goods i : this.cards) {
            if (i.getType().equals(GoodsType.Legal)) {
                legal.add(i);
            }
        }
        // cazul cand nu exista carti legale
        if (legal.size() == 0) {
            // cazul cand nu exista carti deloc
            if (cards.size() == 0) {
                return goods;
            }

            goods.add(this.cards.get(0));
            // se selecteaza bunul ilegal cu cel mai mare profit
            int maxIndex = 0;
            for (int i = 0; i < this.cards.size(); i++) {
                if (this.cards.get(i).getProfit() > goods.get(0).getProfit()) {
                    goods.remove(0);
                    goods.add(this.cards.get(i));
                    maxIndex = i;
                }
            }
            this.cards.remove(maxIndex);
            // returneaza o lista doar cu un bun ilegal
            return goods;
        }
        // cazul cand exista carti legale
        HashMap<Integer, Integer> freq = new HashMap<Integer, Integer>();
        // folosesc un HashMap ce are drept cheie indexul bunului si ca valoare frecventa sa
        for (int i = 0; i < legal.size(); i++) {
            if (freq.containsKey(legal.get(i).getId())) {
                freq.replace(legal.get(i).getId(), freq.get(legal.get(i).getId()) + 1);
            } else {
                freq.put(legal.get(i).getId(), 1);
            }
        }
        ArrayList<GoodsWithKey> selectList = new ArrayList<GoodsWithKey>();

        // adaug intr-o lista ce contine elemente de tip GoodsWithKey cartile din HashMap
        for (Map.Entry<Integer, Integer> aux : freq.entrySet()) {
            selectList.add(new GoodsWithKey(factory.getGoodsById(aux.getKey()), aux.getValue()));
        }
        // sortez lista in functie de frecventa
        GoodsComparator compare = new GoodsComparator();
        selectList.sort(compare);
        int max = selectList.get(0).key;
        // elimin elementele ce nu au frecventa maxima
        for (int i = 0; i < selectList.size(); i++) {
            if (selectList.get(i).key < max) {
                selectList.remove(selectList.get(i));
                i--;
            } else {
                selectList.get(i).setKeyValue();
            }
        }
        // sortez lista in functie de profit
        selectList.sort(compare);
        max = selectList.get(0).key;
        // elimin elementele ce nu au profit maxim
        for (int i = 0; i < selectList.size(); i++) {
            if (selectList.get(i).key < max) {
                selectList.remove(selectList.get(i));
                i--;
            } else {
                selectList.get(i).setKeyIndex();
            }
        }
        // sortez lista in functie de index
        selectList.sort(compare);
        max = selectList.get(0).key;
        freq.clear();
        selectList.clear();
        // adaug in lista ce urmeaza sa fie returnata toate bunurile cu indexul ales
        for (int i = 0; i < this.cards.size(); i++) {
            if (this.cards.get(i).getId() == max) {
                goods.add(this.cards.get(i));
                this.cards.remove(i);
                i--;
            }
        }

        return goods;
    }

    /* Aceasta metoda este individuala pentru fiecare jucator, din aceasta cauza
    * in fiecare subaclasa ea va fi suprascris*/
    public void makeBag() {
        ArrayList<Goods> goods = selectCards();

        if (goods.get(0).getType().equals(GoodsType.Illegal)) {
            bag = new Bag(goods, Constants.BASIC_PLAYER_BRIBE, Constants.APPLE_ID);
        } else {
            bag = new Bag(goods, Constants.BASIC_PLAYER_BRIBE, goods.get(0).getId());
        }
    }

    /* Aceasta metoda este individuala pentru fiecare jucator, din aceasta cauza
     * in fiecare subaclasa ea va fi suprascris*/
    // comportament in calitate de sherif
    public void checkBag(BasicPlayer player, LinkedList<Goods> inputCards) {
        int penalty;
        player.payCoins(player.getBag().getBribe());
        // se verifica daca jucatorul poate sa execute verificarea
        if (!canCheck) {
            return;
        }
        // caz comerciant cinstit, in else comerciant ce a mintit
        if (player.getBag().checkBag()) {
            penalty = player.getBag().payPenalty();
            player.payCoins(penalty);
            this.coins -= penalty;
        } else {
            penalty = player.getBag().getPenalty();
            player.takeCoins(penalty);
            player.getBag().removeIllegals(inputCards);
            this.coins += penalty;
        }
    }

    // pune bunurile ce au trecut verificarea pe taraba
    final void putOnStall() {
        LinkedList<Integer> id = new LinkedList<>();
        LinkedList<Integer> num = new LinkedList<>();
        // se itereaza prin carti
        for (int i = 0; i < bag.getCards().size(); i++) {
            // daca bunul este ilegal acesta se adauga in HasMap-ul respectiv
            if (bag.getCards().get(i).getType().equals(GoodsType.Illegal)) {
                if (totalIllegalGoods.containsKey(bag.getCards().get(i).getId())) {
                    totalIllegalGoods.replace(
                            bag.getCards().get(i).getId(),
                            totalIllegalGoods.get(bag.getCards().get(i).getId()) + 1);
                } else {
                    totalIllegalGoods.put(bag.getCards().get(i).getId(), 1);
                }
                // se adauga si bonusurile bunului ilegal
                switch (bag.getCards().get(i).getId()) {
                    case Constants.SILK:
                        id.add(Constants.CHEESE);
                        num.add(Constants.SILK_QUANTITY);
                        break;
                    case Constants.PEPPER:
                        id.add(Constants.CHICKEN);
                        num.add(Constants.PEPPER_QUANTITY);
                        break;
                    case Constants.BARREL:
                        id.add(Constants.BREAD);
                        num.add(Constants.BARREL_QUANTITY);
                        break;
                    case Constants.BEER:
                        id.add(Constants.WINE);
                        num.add(Constants.BEER_QUANTITY);
                        break;
                    case Constants.SEAFOOD:
                        id.add(Constants.TOMATO);
                        id.add(Constants.POTATO);
                        id.add(Constants.CHICKEN);
                        num.add(Constants.SEAFOOD_TOMATO_QUANTITY);
                        num.add(Constants.SEAFOOD_POTATO_QUANTITY);
                        num.add(Constants.SEAFOOD_CHICKEN_QUANTITY);
                        break;
                    default:
                }

                for (int k = 0; k < num.size(); k++) {
                    for (int j = 0; j < num.get(k); j++) {
                        bag.getCards().add(factory.getGoodsById(id.get(k)));
                    }
                }
                id.clear();
                num.clear();

                continue;
            }
            // daca bunul este legal se adauga in HashMap-ul de bunuri legale
            if (totalGoods.containsKey(bag.getCards().get(i).getId())) {
                totalGoods.replace(bag.getCards().get(i).getId(),
                        totalGoods.get(bag.getCards().get(i).getId()) + 1);
            } else {
                totalGoods.put(bag.getCards().get(i).getId(), 1);
            }
        }

        bag.getCards().clear();
        this.cards.clear();
    }

    // vinde bunurile acumuate pe parcursul jocului
    final void sellItems() {
        for (Map.Entry<Integer, Integer> entry : totalGoods.entrySet()) {
            coins += entry.getValue() * factory.getGoodsById(entry.getKey()).getProfit();
        }
        for (Map.Entry<Integer, Integer> entry : totalIllegalGoods.entrySet()) {
            coins += entry.getValue() * factory.getGoodsById(entry.getKey()).getProfit();
        }
    }

    // se adauga bonusul de king pentru o anumita carte
    final void kingBonus(int id) {
        this.coins += factory.getGoodsById(id).getKingBonus();
    }

    // se adauga bonusul de queen pentru o anumita carte
    final void queenBonus(int id) {
        this.coins += factory.getGoodsById(id).getQueenBonus();
    }

    // seteaza campul canCheck la inceput de subrunda
    final void setCanCheck() {
        if (coins < Constants.MIN_COINS) {
            this.canCheck = false;
        }
    }

    // reseteaza campul canCheck la final de subrunda
    final void resetCanCheck() {
        if (coins >= Constants.MIN_COINS) {
            this.canCheck = true;
        }
    }

    final void setIter(int iter) {
        this.iter = iter;
    }

    final void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    final int getIndex() {
        return index;
    }

    final Bag getBag() {
        return bag;
    }

    final int getCoins() {
        return coins;
    }

    /* Suprascri metoda toString pentru a satisface formatului de output din cerinta */
    @Override
    public String toString() {
        return (this.index + " " + this.type + " " + this.coins);
    }
}
