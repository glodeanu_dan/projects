package com.tema1.utility;

import com.tema1.common.Constants;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import java.util.ArrayList;
import java.util.LinkedList;

public class GreedyPlayer extends BasicPlayer {

    public GreedyPlayer(int index) {
        super(index);
        this.type = "GREEDY";
    }

    /* Suprascrie metoda checkBag pentru a satisface stilului de joc al lui Greedy */
    @Override
    public void checkBag(BasicPlayer player, LinkedList<Goods> inputCards) {
        int coins;
        // dac exista mita, aceasta este luata si jucatorul nu e verificat
        if (player.getBag().getBribe() > 0) {
            this.coins += player.getBag().getBribe();
            return;
        }
        // jucatorul e verificat
        super.checkBag(player, inputCards);
    }

    /* Suprascrie metoda makeBag pentru a satisface stilului de joc al lui Greedy */
    @Override
    public void makeBag() {
        ArrayList<Goods> goods = selectCards();
        // se creeaza sacul
        if (goods.get(0).getType().equals(GoodsType.Illegal)) {
            bag = new Bag(goods, Constants.BASIC_PLAYER_BRIBE, Constants.APPLE_ID);
        } else {
            bag = new Bag(goods, Constants.BASIC_PLAYER_BRIBE, goods.get(0).getId());
        }
        // daca runda este para jucatorul mai adauga o carte ilegala cu profit maxim
        if (iter % 2 == 0 && bag.getCards().size() < Constants.CARDS_BAG_MAX) {

            int index = 0, aux = 0;
            for (Goods i : this.cards) {
                if (i.getType().equals(GoodsType.Illegal) && i.getProfit() > aux) {
                    index = cards.indexOf(i);
                    aux = i.getProfit();
                }
            }

            if (aux == 0) {
                return;
            }

            bag.getCards().add(cards.get(index));
            cards.remove(index);
        }
    }
}
