package com.tema1.utility;

import java.util.HashMap;
import java.util.Map;

public class KingQueenFinder {
    private HashMap<Integer, Node> allGoods = new HashMap<Integer, Node>();
    // clasa interna ce reprezinta valorea unui nod din HashMap
    private class Node {
        private int kingFreq = 0;
        private int queenFreq = 0;
        private BasicPlayer king = null;
        private BasicPlayer queen = null;
    }
    // se gasesc playerii king si queen pentru fiecare carte din joc
    final void setNode(BasicPlayer player) {
        // se iteteaza prin lista de bunuri a jucatorului
        for (Map.Entry<Integer, Integer> entry : player.totalGoods.entrySet()) {
            // cazul cand bunul deja se contine in HashMap-ul ubiectului
            if (allGoods.containsKey(entry.getKey())) {
                // jucatorul curent este setat king iar fostul king devine queen
                if (entry.getValue() > allGoods.get(entry.getKey()).kingFreq
                        || (entry.getValue() == allGoods.get(entry.getKey()).kingFreq
                        && allGoods.get(entry.getKey()).king.getIndex() > player.getIndex())) {
                    allGoods.get(entry.getKey()).queen = allGoods.get(entry.getKey()).king;
                    allGoods.get(entry.getKey()).queenFreq = allGoods.get(entry.getKey()).kingFreq;
                    allGoods.get(entry.getKey()).king = player;
                    allGoods.get(entry.getKey()).kingFreq = entry.getValue();
                    // jucatorul curent este facut queen
                } else if (entry.getValue() > allGoods.get(entry.getKey()).queenFreq
                        || (entry.getValue() == allGoods.get(entry.getKey()).queenFreq
                        && allGoods.get(entry.getKey()).queen.getIndex() > player.getIndex())) {
                    allGoods.get(entry.getKey()).queen = player;
                    allGoods.get(entry.getKey()).queenFreq = entry.getValue();
                }
                // nu exista nici un alt jucator cu acest bun, deci jucatorul curent e king
            } else {
                allGoods.put(entry.getKey(), new Node());
                allGoods.get(entry.getKey()).king = player;
                allGoods.get(entry.getKey()).kingFreq = entry.getValue();
            }
        }
    }

    final void giveBonuses() {
        // se itereaza prin HashMap si fiecare jucator isi primeste bonusul
        for (Map.Entry<Integer, Node> entry : allGoods.entrySet()) {
            if (entry.getValue().king != null) {
                entry.getValue().king.kingBonus(entry.getKey());
            }
            if (entry.getValue().queen != null) {
                entry.getValue().queen.queenBonus(entry.getKey());
            }
        }
    }
}
