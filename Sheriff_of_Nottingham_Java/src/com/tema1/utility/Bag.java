package com.tema1.utility;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import java.util.LinkedList;
import java.util.List;

public class Bag {
    protected LinkedList<Goods> cards = new LinkedList<Goods>();
    private int bribe;
    private int id;

    // constructor pentru un obiect de tip Bag
    public Bag(List<Goods> cards, int bribe, int id) {
        this.cards.addAll(cards);
        this.bribe = bribe;
        this.id = id;
    }

    // metoda ce itereaza prin lista de carti si verifica daca jucatorul a incalcat regulile
    final boolean checkBag() {
        for (Goods i : this.cards) {
            if (i.getType().equals(GoodsType.Illegal)) {
                return false;
            }
            if (i.getId() != id) {
                return false;
            }
        }
        return true;
    }

    // metoda ce returneaza penalty pentru toate cartile ce nu corespund tipului declarat
    final int getPenalty() {
        int penalty = 0;

        for (Goods i : cards) {
            if (i.getId() != id || i.getType().equals(GoodsType.Illegal)) {
                penalty += i.getPenalty();
            }
        }

        return penalty;
    }

    // metoda ce sterge bunurile nedeclarate
    final void removeIllegals(LinkedList<Goods> inputCards) {

        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getId() != id || cards.get(i).getType().equals(GoodsType.Illegal)) {
                inputCards.addLast(cards.get(i));
                this.cards.remove(i);
                i--;
            }
        }
    }

    // metoda ce calculeaza cat ii datoreaza sheiful unui comerciant cinstit
    final int payPenalty() {
        if (cards.isEmpty()) {
            return 0;
        }
        return (this.cards.size() * this.cards.get(0).getPenalty());
    }

    // getter pentru bribe
    final int getBribe() {
        return bribe;
    }

    // getter pentru bunuri
    final LinkedList<Goods> getCards() {
        return this.cards;
    }
}
