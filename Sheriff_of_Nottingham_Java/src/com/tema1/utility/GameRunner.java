package com.tema1.utility;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;
import com.tema1.main.GameInput;

import java.util.ArrayList;
import java.util.LinkedList;

public class GameRunner {
    private ArrayList<BasicPlayer> players;
    private LinkedList<Goods> inputCards;
    private GoodsFactory factory;
    private int rounds;

    // constructor pentru un obiect GameRunner
    public GameRunner(GameInput gameInput) {
        rounds = gameInput.getRounds();
        players = new ArrayList<BasicPlayer>();
        inputCards = new LinkedList<Goods>();
        factory = GoodsFactory.getInstance();
        // se itereaza prin numele jucatorilor primiti la input si se creeaza obiecte de tip jucator
        for (int i = 0; i < gameInput.getPlayerNames().size(); i++) {
            if (gameInput.getPlayerNames().get(i).equals("greedy")) {
                players.add(new GreedyPlayer(i));
            } else if (gameInput.getPlayerNames().get(i).equals("basic")) {
                players.add(new BasicPlayer(i));
            } else {
                players.add(new BribedPlayer(i));
            }
            // daca jucatorul este ultimul el este marcat
            if (i == gameInput.getPlayerNames().size() - 1) {
                players.get(i).setIsLast(true);
            }
        }
        // se creaaza si obiectele de tip bunuri, id acestora fiind primit ca input
        for (int i = 0; i < gameInput.getAssetIds().size(); i++) {
            inputCards.add(factory.getGoodsById(gameInput.getAssetIds().get(i)));
        }
    }

    /* Metoda care ruleaza toate rundele jocului */
    public void runGame() {
        // for a carui iteratie este o runda
        for (int i = 0; i < rounds; i++) {
            // se seteaza runda pentru jucatori
            for (BasicPlayer p: players) {
                p.setIter(i + 1);
            }
            // se ruleaza o subrunda
            for (BasicPlayer sherif: players) {
                for (BasicPlayer commerciant: players) {
                    if (sherif != commerciant) {
                        sherif.setCanCheck();
                        commerciant.getCards(inputCards);
                        commerciant.makeBag();
                        sherif.checkBag(commerciant, inputCards);
                        commerciant.putOnStall();
                    }
                }
                sherif.resetCanCheck();
            }
        }
        // dupa ce s-au terminat rundele se vand bunurile
        for (BasicPlayer player: players) {
            player.sellItems();
        }
        // se aplica bonusurile
        KingQueenFinder finder = new KingQueenFinder();
        for (BasicPlayer player: players) {
            finder.setNode(player);
        }
        finder.giveBonuses();
    }

    /* Metoda care sorteaza jucatorii in functie de scorul acumulat */
    public void sortPlayers() {
        // surteaza jucatorii in functie de bani
        for (int i = 0; i < players.size() - 1; i++) {
            for (int j = i + 1; j < players.size(); j++) {
                if (players.get(i).getCoins() < players.get(j).getCoins()) {
                    BasicPlayer aux = players.get(i);
                    players.set(i, players.get(j));
                    players.set(j, aux);
                }
            }
        }
        // sorteaza jucatorii in functie de id
        for (int i = 0; i < players.size() - 1; i++) {
            for (int j = i + 1; j < players.size(); j++) {
                if (players.get(i).getCoins() == players.get(j).getCoins()
                        && players.get(i).getIndex() > players.get(j).getIndex()) {
                    BasicPlayer aux = players.get(i);
                    players.set(i, players.get(j));
                    players.set(j, aux);
                }
            }
        }
    }
    /* Metoda ce afiseaza rezultatele finale */
    public void printResults() {
        for (BasicPlayer player: players) {
            System.out.println(player);
        }
    }
}
