package com.tema1.utility;

import com.tema1.common.Constants;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import java.util.LinkedList;

public class BribedPlayer extends BasicPlayer {

    // constructor pentru BribedPlayer
    public BribedPlayer(int index) {
        super(index);
        this.type = "BRIBED";
    }

    // returneaza mita pe care o pune BribedPlayer
    private int putBribe(int num) {
        if (num <= 2) {
            coins -= Constants.MIN_BRIBE;
            return Constants.MIN_BRIBE;
        } else {
            coins -= Constants.MAX_BRIBE;
            return Constants.MAX_BRIBE;
        }
    }

    /* Suprascrie metoda makeBag pentru a satisface stilului de joc al lui Bribed */
    @Override
    public void makeBag() {
        LinkedList<Goods> legalGoods = new LinkedList<Goods>();
        LinkedList<Goods> illegalGoods = new LinkedList<Goods>();

        // se itereaza prin cartile din mana si se separa cartile legale si cele ilegale
        for (Goods aux : cards) {
            if (aux.getType().equals(GoodsType.Illegal)) {
                illegalGoods.add(aux);
            } else {
                legalGoods.add(aux);
            }
        }

        // se sorteaza cartile ilegale in functie de profit
        for (int i = 0; i < illegalGoods.size() - 1; i++) {
            for (int j = i + 1; j < illegalGoods.size(); j++) {
                if (illegalGoods.get(i).getProfit() < illegalGoods.get(j).getProfit()) {
                    Goods aux = illegalGoods.get(i);
                    illegalGoods.set(i, illegalGoods.get(j));
                    illegalGoods.set(j, aux);
                }
            }
        }
        // se sorteaza cartile legale in functie de profit
        for (int i = 0; i < legalGoods.size() - 1; i++) {
            for (int j = i + 1; j < legalGoods.size(); j++) {
                if (legalGoods.get(i).getProfit() < legalGoods.get(j).getProfit()) {
                    Goods aux = legalGoods.get(i);
                    legalGoods.set(i, legalGoods.get(j));
                    legalGoods.set(j, aux);
                }
            }
        }

        // se sorteaza cartile legale in unctie de id
        for (int i = 0; i < legalGoods.size() - 1; i++) {
            for (int j = i + 1; j < legalGoods.size(); j++) {
                if (legalGoods.get(i).getProfit() == legalGoods.get(j).getProfit()
                        && legalGoods.get(i).getId() < legalGoods.get(j).getId()) {
                    Goods aux = legalGoods.get(i);
                    legalGoods.set(i, legalGoods.get(j));
                    legalGoods.set(j, aux);
                }
            }
        }
        LinkedList<Goods> goods = new LinkedList<Goods>();
        // cazul cand Bribed e nevoit sa joace cinstit
        if (illegalGoods.size() == 0 || coins <= Constants.MIN_BRIBE) {
            super.makeBag();
            return;
        }

        int illegalNum = 0;
        int potentialPenalty = 0;

        // se adauga cat mai multe carti ilegale cu profit maxim
        while (!illegalGoods.isEmpty() && goods.size() < Constants.CARDS_BAG_MAX) {

            if (coins - potentialPenalty - illegalGoods.peekFirst().getPenalty() > 0) {
                potentialPenalty += illegalGoods.peekFirst().getPenalty();
                cards.remove(illegalGoods.peek());
                goods.add(illegalGoods.poll());
                illegalNum++;
            } else {
                illegalGoods.removeFirst();
            }
        }

        // se completeaza cu carti legale cu profit maxim
        while (!legalGoods.isEmpty() && goods.size() < Constants.CARDS_BAG_MAX) {
            if (coins - potentialPenalty - legalGoods.peekFirst().getPenalty() > 0) {
                potentialPenalty += legalGoods.peekFirst().getPenalty();
                cards.remove(legalGoods.peek());
                goods.add(legalGoods.poll());
            } else {
                legalGoods.removeFirst();
            }
        }
        // se creeaza sacul
        bag = new Bag(goods, putBribe(illegalNum), Constants.APPLE_ID);
    }

    /* Suprascrie metoda checkBag pentru a satisface stilului de joc al lui Bribed */
    @Override
    public void checkBag(BasicPlayer player, LinkedList<Goods> inputCards) {

        // caz in care jucatorul verifica comerciantul
        if ((player.getIndex() == index + 1)
                || (player.getIndex() == index - 1)
                || (this.isLast && player.getIndex() == 0)
                || (this.index == 0 && player.isLast)) {
            super.checkBag(player, inputCards);
        } else {
            // doar se ia mita, daca exista
            if (player.getBag().getBribe() > 0) {
                this.coins += player.getBag().getBribe();
            }
        }
    }
}
