package com.tema1.common;

public final class Constants {
    public static final int START_COINS = 80;
    public static final int BASIC_PLAYER_BRIBE = 0;
    public static final int CARDS_HAND_MAX = 10;
    public static final int CARDS_BAG_MAX = 8;
    public static final int MIN_COINS = 16;
    public static final int APPLE_ID = 0;
    public static final int MIN_BRIBE = 5;
    public static final int MAX_BRIBE = 10;
    public static final int SILK = 20;
    public static final int PEPPER = 21;
    public static final int BARREL = 22;
    public static final int BEER = 23;
    public static final int SEAFOOD = 24;
    public static final int SILK_QUANTITY = 3;
    public static final int PEPPER_QUANTITY = 2;
    public static final int BARREL_QUANTITY = 2;
    public static final int BEER_QUANTITY = 4;
    public static final int SEAFOOD_TOMATO_QUANTITY = 2;
    public static final int SEAFOOD_POTATO_QUANTITY = 3;
    public static final int SEAFOOD_CHICKEN_QUANTITY = 1;
    public static final int CHEESE = 1;
    public static final int BREAD = 2;
    public static final int CHICKEN = 3;
    public static final int TOMATO = 4;
    public static final int POTATO = 6;
    public static final int WINE = 7;
}
