package com.tema1.goods;

public abstract class Goods {
    private final int id;
    private final GoodsType type;
    private final int profit;
    private final int penalty;

    public Goods(final int id, final GoodsType type, final int profit, final int penalty) {
        this.id = id;
        this.type = type;
        this.profit = profit;
        this.penalty = penalty;
    }
    /* Este scris pentru a putea folosi getKingBonus din LegalGoods */
    public int getKingBonus() {
        return 0;
    }
    /* Este scris pentru a putea folosi getQueenBonus din LegalGoods */
    public int getQueenBonus() {
        return 0;
    }

    public final int getId() {
        return id;
    }

    public final GoodsType getType() {
        return type;
    }

    public final int getProfit() {
        return profit;
    }

    public final int getPenalty() {
        return penalty;
    }
}
