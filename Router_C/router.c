#include "skel.h"
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <sys/ioctl.h>
#include <bits/ioctls.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "queue.h"
#include "list.h"

#define BUFLEN 1000

typedef struct ether_arp {
	unsigned short int ar_hrd;		
    unsigned short int ar_pro;
    unsigned char ar_hln;
    unsigned char ar_pln;
    unsigned short int ar_op;
	uint8_t arp_sha[ETH_ALEN];
	uint8_t arp_spa[4];	
	uint8_t arp_tha[ETH_ALEN];	
	uint8_t arp_tpa[4];	
} ether_arp;

typedef struct rcell {
	uint32_t prefix;
	uint32_t next;
	uint32_t mask;
	int interface;
} rcell;

typedef struct arp_entry {
	uint32_t ip;
	uint8_t mac[6];
} arp_entry;

uint16_t ip_checksum(void* vdata,size_t length) {
	// Cast the data pointer to one that can be indexed.
	char* data=(char*)vdata;

	// Initialise the accumulator.
	uint64_t acc=0xffff;

	// Handle any partial block at the start of the data.
	unsigned int offset=((uintptr_t)data)&3;
	if (offset) {
		size_t count=4-offset;
		if (count>length) count=length;
		uint32_t word=0;
		memcpy(offset+(char*)&word,data,count);
		acc+=ntohl(word);
		data+=count;
		length-=count;
	}

	// Handle any complete 32-bit blocks.
	char* data_end=data+(length&~3);
	while (data!=data_end) {
		uint32_t word;
		memcpy(&word,data,4);
		acc+=ntohl(word);
		data+=4;
	}
	length&=3;

	// Handle any partial block at the end of the data.
	if (length) {
		uint32_t word=0;
		memcpy(&word,data,length);
		acc+=ntohl(word);
	}

	// Handle deferred carries.
	acc=(acc&0xffffffff)+(acc>>32);
	while (acc>>16) {
		acc=(acc&0xffff)+(acc>>16);
	}

	// If the data began at an odd byte address
	// then reverse the byte order to compensate.
	if (offset&1) {
		acc=((acc&0xff00)>>8)|((acc&0x00ff)<<8);
	}

	// Return the checksum in network byte order.
	return htons(~acc);
}

// functie de partitie pentru quickSort
int partition(rcell *rtable, int begin, int end) {
	rcell pivot = rtable[end];
	rcell aux;
	int i = begin - 1;
	int j;
	for (j = begin; j < end; j++) {
		if ((rtable[j].prefix < pivot.prefix) || (rtable[j].prefix == pivot.prefix
											&& rtable[j].mask > pivot.mask )) {
			i++;
			aux = rtable[i];
			rtable[i] = rtable[j];
			rtable[j] = aux;
		}
	}

	aux = rtable[i + 1];
	rtable[i + 1] = rtable[end];
	rtable[end] = aux;


	return (i + 1);
}

// quick sort pentru rtable
void quickSort(rcell *rtable, int begin, int end) {
	if (begin < end) {
		int pindex = partition(rtable, begin, end);

		quickSort(rtable, begin, pindex - 1);
		quickSort(rtable, pindex + 1, end);
	}
}

// functie de parsare a tabelei rtable
rcell* parseTable(int *size) {
	FILE *fp;
    char *line = NULL;
    size_t len = 0;
    int v_len = 10;
    rcell *v = malloc(v_len * sizeof(rcell));
    int n = 0;
	char *aux;  

	// deschid fisierul din care citesc
    fp = fopen("rtable.txt", "r");
    if (fp == NULL)
        return NULL;

    // citesc linie cu linie fisierul
    while (getline(&line, &len, fp)!= -1) {
		n++;

		// daca este cazul maresc lungimea vectorului si il realoc
    	if (n > v_len) {
    		v_len *= 2;
    		v = realloc(v, v_len * sizeof(rcell));
    	}

    	// adaug campurile in structura mea rcell
		aux = strtok(line, " \n");
		v[n - 1].prefix = inet_addr(aux);
		aux = strtok(NULL, " \n");
		v[n - 1].next = inet_addr(aux);
		aux = strtok(NULL, " \n");
		v[n - 1].mask = inet_addr(aux);
		aux = strtok(NULL, " \n");
		v[n - 1].interface = atoi(aux);

    }

    fclose(fp);
    if (line)
        free(line);

    *size = n;
    return v;
}

// gasesc adresa mac ce se potriveste cu ip-ul primit ca parametru
struct arp_entry *get_arp_entry(uint32_t ip, struct arp_entry *arp_table, int n) {
    struct arp_entry *entry = NULL;

    int i;

    for (i = 0; i < n; i++) {
    	if (arp_table[i].ip == ip) {
            entry = &arp_table[i];
            break;
        }
    }
    printf("%p\n", entry);
    return entry;
}

// verific checksum pentru un header ip
int isValid(struct iphdr *ip_hdr) {
	uint32_t oldCheck = ip_hdr->check;
    ip_hdr->check = 0;
    ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));
    
    if (oldCheck != ip_hdr->check) {
        return 0;
    }
    return 1;
}


// binary search pentru a gasi cea mai buna intrare din rtable
rcell get_entry(rcell *rtable, int left, int right, uint32_t ip) {
	rcell bad;
	bad.mask = 0;

	// cazul in care nu e gasita o masca potrivita
	if(right < left) {
		return bad;
	}

	int mid = left + (right - left) / 2;
	// caz match 
	if((ip & rtable[mid].mask) == rtable[mid].prefix) {
		
		// iau cea mai mare masca
		while(rtable[mid - 1].prefix == rtable[mid].prefix) {
			mid--;
		}

		return rtable[mid];
	}

	if((ip & rtable[mid].mask) < rtable[mid].prefix)
		return get_entry(rtable, left, mid - 1, ip);
	else
		return get_entry(rtable, mid + 1, right, ip);
}

int forward(packet *m, rcell *v, int n, struct arp_entry *arp_table, int narp, 
	struct ether_header *eth_hdr, struct iphdr *ip_hdr) {

	// caut intrarea cea mai specifica din rtable, daca nu exista trimit un icmp
	rcell next = get_entry(v, 0, n - 1, ip_hdr->daddr);

	if (next.mask == 0) {
		return 0;
	}

	// cau mac-ul pentru noua destinatie
	struct arp_entry *mac_entry = get_arp_entry(next.next, arp_table, narp);

	if (mac_entry == NULL) {
		return -1;
	}

	// modific adresele source si destination MAC
    memcpy(eth_hdr->ether_dhost, mac_entry->mac, 6);
	get_interface_mac(next.interface, eth_hdr->ether_shost);

	// decrementez ttl si updatez checksum
	ip_hdr->ttl--;
	ip_hdr->check = 0;
	ip_hdr->check = ip_checksum(ip_hdr, sizeof(struct iphdr));

	// trimit pachetul
	send_packet(next.interface, m);

	return 1;
}

int main(int argc, char *argv[])
{
	setvbuf(stdout, NULL, _IONBF, 0);
	packet m;
	int rc;
	init();
	queue q = queue_create();

	//  initializez rtable
	struct arp_entry *arptable = NULL;
	rcell *rtable;
	int nr;
	int na = 0;

	rtable = parseTable(&nr);
	quickSort(rtable, 0, nr - 1);

	while (1) {
		rc = get_packet(&m);
		DIE(rc < 0, "get_message");

		// extrag headerul de ethernet
		struct ether_header *eth_hdr = (struct ether_header *)m.payload;

		// cazul cand in care protocolul ether contine un ip
		if (ntohs(eth_hdr->ether_type) == ETHERTYPE_IP) {
			struct iphdr *ip_hdr = (struct iphdr *)(m.payload + sizeof(struct ether_header));

			// verific daca ttl-ul e suficient de mare
			if (ip_hdr->ttl <= 1) {

				// trimit un icmp
				packet err;
				err.interface = m.interface;
				int ip_offset = sizeof(struct ether_header);
				int icmp_offset = sizeof(struct iphdr) + ip_offset;
				err.len = icmp_offset + sizeof(struct icmphdr);

				struct ether_header *err_eth = (struct ether_header *)err.payload;
				struct iphdr *err_ip = (struct iphdr *)(err.payload + ip_offset);
				struct icmphdr *err_icmp = (struct icmphdr *)(err.payload + icmp_offset);

				// completez headerul ip
				memcpy(err_ip, ip_hdr, ip_hdr->tot_len);
				uint16_t aux = err_ip->daddr;
				err_ip->daddr = ip_hdr->saddr;
				err_ip->saddr = aux;
				err_ip->id = 0;
				err_ip->ttl = 64;
				err_ip->version = 4;
				err_ip->protocol = 1;
				err_ip->tot_len = htons(sizeof(struct icmphdr) + sizeof(struct iphdr));
				err_ip->check = 0;
				err_ip->check = ip_checksum(err_ip, sizeof(struct iphdr));
				
				// completez headerul icmp
				err_icmp->type = ICMP_TIME_EXCEEDED;
				err_icmp->code = ICMP_EXC_TTL;
				err_icmp->un.echo.sequence = 0;
				err_icmp->un.echo.id = 0;
				err_icmp->checksum = 0;
				err_icmp->checksum = ip_checksum(err_icmp, sizeof(struct icmphdr));

				// gasesc adresa destinatie si completez headerul ethernet
				rcell next = get_entry(rtable, 0, nr, ip_hdr->saddr);
				err_eth->ether_type = htons(ETHERTYPE_IP);
				memcpy(err_eth->ether_shost, eth_hdr->ether_dhost, 6);
				memcpy(err_eth->ether_dhost, get_arp_entry(next.next, arptable, na)->mac, 6);

				// trimit pachetul
				send_packet(err.interface, &err);
				continue;
			}

			// verific checksum-ul
			if (!isValid(ip_hdr)) {
				continue;
			}

			// caz cand protocolul ip contine un icmp echo request pentru router
			if (ip_hdr->protocol == 1 && inet_addr(get_interface_ip(m.interface)) == ip_hdr->daddr) {

				// extrag headerul icmp
				struct icmphdr *icmp_hdr = (struct icmphdr *)(m.payload + sizeof(struct ether_header) + 
					sizeof(struct iphdr));

				// cazul cand routerul primeste echo request trimit un echo reply ca mai sus
				if (icmp_hdr->type == ICMP_ECHO) {

					//completarea campurilor pentru echo reply
					packet err;
					err.interface = m.interface;
					int ip_offset = sizeof(struct ether_header);
					int icmp_offset = sizeof(struct iphdr) + ip_offset;
					err.len = icmp_offset + sizeof(struct icmphdr);

					struct ether_header *err_eth = (struct ether_header *)err.payload;
					struct iphdr *err_ip = (struct iphdr *)(err.payload + ip_offset);
					struct icmphdr *err_icmp = (struct icmphdr *)(err.payload + icmp_offset);

					//setarea headerului ip
					memcpy(err_ip, ip_hdr, ip_hdr->tot_len);
					uint16_t aux = err_ip->daddr;
					err_ip->daddr = ip_hdr->saddr;
					err_ip->saddr = aux;
					err_ip->id = 0;
					err_ip->ttl = 64;
					err_ip->version = 4;
					err_ip->protocol = 1;
					err_ip->tot_len = htons(sizeof(struct icmphdr) + sizeof(struct iphdr));
					err_ip->check = 0;
					err_ip->check = ip_checksum(err_ip, sizeof(struct iphdr));
					
					//setarea headerului icmp
					err_icmp->type = ICMP_ECHOREPLY;
					err_icmp->code = 0;
					err_icmp->un.echo.sequence = 0;
					err_icmp->un.echo.id = 0;
					err_icmp->checksum = 0;
					err_icmp->checksum = ip_checksum(err_icmp, sizeof(struct icmphdr));

					rcell next = get_entry(rtable, 0, nr, ip_hdr->saddr);

					//setarea tipului de protocol
					err_eth->ether_type = htons(ETHERTYPE_IP);

					memcpy(err_eth->ether_shost, eth_hdr->ether_dhost, 6);
					memcpy(err_eth->ether_dhost, get_arp_entry(next.next, arptable, na)->mac, 6);

					send_packet(err.interface, &err);
				}
			}
			// cazul in care este altfel de pachet si trebuie forwardat
			else {

				// forwardez pachetul
				int out = forward(&m, rtable, nr, arptable, na, eth_hdr, ip_hdr);

				//  caz destination unreachable
				if (out == 0) {

					//trimiterea mesajul prin completarea headerilor
					packet err;
					err.interface = m.interface;
					int ip_offset = sizeof(struct ether_header);
					int icmp_offset = sizeof(struct iphdr) + ip_offset;
					err.len = icmp_offset + sizeof(struct icmphdr);

					struct ether_header *err_eth = (struct ether_header *)err.payload;
					struct iphdr *err_ip = (struct iphdr *)(err.payload + ip_offset);
					struct icmphdr *err_icmp = (struct icmphdr *)(err.payload + icmp_offset);


					memcpy(err_ip, ip_hdr, ip_hdr->tot_len);
					uint16_t aux = err_ip->daddr;
					err_ip->daddr = ip_hdr->saddr;
					err_ip->saddr = aux;
					err_ip->id = 0;
					err_ip->ttl = 64;
					err_ip->version = 4;
					err_ip->protocol = 1;
					err_ip->tot_len = htons(sizeof(struct icmphdr) + sizeof(struct iphdr));
					err_ip->check = 0;
					err_ip->check = ip_checksum(err_ip, sizeof(struct iphdr));
					
					//setarea mesajului de unreachable
					err_icmp->type = ICMP_DEST_UNREACH;
					err_icmp->code = ICMP_NET_UNREACH;
					err_icmp->un.echo.sequence = 0;
					err_icmp->un.echo.id = 0;
					err_icmp->checksum = 0;
					err_icmp->checksum = ip_checksum(err_icmp, sizeof(struct icmphdr));

					rcell next = get_entry(rtable, 0, nr, ip_hdr->saddr);

					err_eth->ether_type = htons(ETHERTYPE_IP);

					memcpy(err_eth->ether_shost, eth_hdr->ether_dhost, 6);
					memcpy(err_eth->ether_dhost, get_arp_entry(next.next, arptable, na)->mac, 6);

					send_packet(err.interface, &err);
				}

				// caz cand nu s-a gasit intrarea in tabela
				else if (out == -1) {

					packet *copy = malloc(sizeof(packet));
					memcpy(copy, &m, sizeof(packet));

					queue_enq(q, copy);

					char *broad = "ff:ff:ff:ff:ff:ff";
					char *tha = "00:00:00:00:00:00";

					// adaug pachetul in coada
					rcell next = get_entry(rtable, 0, nr, ip_hdr->daddr);

					packet err;
					err.interface = next.interface;
					err.len = sizeof(struct ether_header) + sizeof(struct ether_arp);

					struct ether_header *err_eth = (struct ether_header *)err.payload;
					struct ether_arp *err_arp = (struct ether_arp *)(err.payload + sizeof(struct ether_header));

					// completez arp-ul
					err_arp->ar_hrd = htons(ARPHRD_ETHER);
					err_arp->ar_pro = htons(2048);
					err_arp->ar_hln = 6;
					err_arp->ar_pln = 4;
					err_arp->ar_op = htons(ARPOP_REQUEST);

					get_interface_mac(m.interface, err_arp->arp_sha);
					uint32_t aux1 = inet_addr(get_interface_ip(m.interface));
					memcpy(err_arp->arp_spa, &aux1, 4);
					hwaddr_aton(tha, err_arp->arp_tha);

					uint32_t aux2 = inet_addr(get_interface_ip(next.interface));
					aux2 = ip_hdr->daddr;
					memcpy(err_arp->arp_tpa, &aux2, 4);

					err_eth->ether_type = htons(ETHERTYPE_ARP);
					get_interface_mac(m.interface, err_eth->ether_shost);
					
					// adresa broadcast
					hwaddr_aton(broad, err_eth->ether_dhost);
					send_packet(err.interface, &err);
				}
				continue;
			}
		}
		// cazul in care protocolul ether contine un arp
		else if (ntohs(eth_hdr->ether_type) == ETHERTYPE_ARP) {
        	struct ether_arp *arp_hdr = (struct ether_arp *)(m.payload + sizeof(struct ether_header));

    		if (ntohs(arp_hdr->ar_op) == ARPOP_REQUEST) {

				//raspunde cu arp reply pentru adresa de mac potrivita.
				memcpy(arp_hdr->arp_spa, arp_hdr->arp_tpa, sizeof( arp_hdr->arp_tpa));
				memcpy(arp_hdr->arp_tha, arp_hdr->arp_sha, sizeof( arp_hdr->arp_sha));
				get_interface_mac(m.interface, arp_hdr->arp_sha);
				
				arp_hdr->ar_op = ntohs(ARPOP_REPLY);
			
				// memcpy(aux, eth_hdr->ether_dhost, sizeof(eth_hdr->ether_dhost));
				memcpy(eth_hdr->ether_dhost, eth_hdr->ether_shost, sizeof(eth_hdr->ether_shost));
				memcpy(eth_hdr->ether_shost, arp_hdr->arp_sha, sizeof(arp_hdr->arp_sha));
				send_packet(m.interface, &m);

        	}

        	// cazul cand primesc arp reply
        	else if (ntohs(arp_hdr->ar_op) == ARPOP_REPLY) {
				struct ether_arp *arp_hdr = (struct ether_arp *)(m.payload + sizeof(struct ether_header));
				queue tmp_q = queue_create();


    			packet *pkg;
				struct iphdr *ip_aux;
				struct ether_header *eth_aux;

				//cautarea pachetului in coada de asteptare
    			while (!queue_empty(q)) {
    				pkg = (packet *)queue_deq(q);
    				eth_aux = (struct ether_header *)pkg->payload;
    				ip_aux = (struct iphdr *)(pkg->payload + sizeof(struct ether_header));

    				//gasirea pachetului cu adresa potrivita
    				if (get_entry(rtable, 0, nr, ip_aux->daddr).next == *((uint32_t *)arp_hdr->arp_spa)) {
    					
    					//realocarea tabelei arp
    					arptable = realloc(arptable, sizeof(arp_entry) * (na + 1));
    					arptable[na].ip = *((uint32_t *)arp_hdr->arp_spa);
    					memcpy(arptable[na].mac, arp_hdr->arp_sha, 6);
    					na++;

    					forward(pkg, rtable, nr, arptable, na, eth_aux, ip_aux);
    				}
					else {
						queue_enq(tmp_q, pkg);
					}
    				
    			}

    			q = tmp_q;
        	}
		}
	}
}
