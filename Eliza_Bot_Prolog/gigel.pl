:- ensure_loaded('chat.pl').

% Returneaza true dacă regula dată ca argument se potriveste cu
% replica data de utilizator. Replica utilizatorului este
% reprezentata ca o lista de tokens. Are nevoie de
% memoria replicilor utilizatorului pentru a deduce emoția/tag-ul
% conversației.
% match_rule/3
% match_rule(_Tokens, _UserMemory, rule(_, _, _, _, _)) :- fail.
match_rule(Tokens, _UserMemory, rule(T, _, A, _, _)) :- rules(Tokens, L), member(rule(T, _, A, _, _), L).

% Primeste replica utilizatorului (ca lista de tokens) si o lista de
% reguli, iar folosind match_rule le filtrează doar pe cele care se
% potrivesc cu replica dată de utilizator.
% find_matching_rules/4
% find_matching_rules(+Tokens, +Rules, +UserMemory, -MatschingRules)
find_matching_rules(Tokens, [R], UserMemory, [R]) :- !.

find_matching_rules(Tokens, Rules, _UserMemory, MatchingRules) :-
	findall(R, (member(R, Rules), match_rule(Tokens, UserMemory, R), R \= rule(_, [[nu, inteleg]], _, _, _)), Y),
	setof(X, member(X, Y), MatchingRules).

% Intoarce in Answer replica lui Gigel. Selecteaza un set de reguli
% (folosind predicatul rules) pentru care cuvintele cheie se afla in
% replica utilizatorului, in ordine; pe setul de reguli foloseste
% find_matching_rules pentru a obtine un set de raspunsuri posibile.
% Dintre acestea selecteaza pe cea mai putin folosita in conversatie.
%
% Replica utilizatorului este primita in Tokens ca lista de tokens.
% Replica lui Gigel va fi intoarsa tot ca lista de tokens.
%
% UserMemory este memoria cu replicile utilizatorului, folosita pentru
% detectarea emotiei / tag-ului.
% BotMemory este memoria cu replicile lui Gigel și va si folosită pentru
% numararea numarului de utilizari ale unei replici.
%
% In Actions se vor intoarce actiunile de realizat de catre Gigel in
% urma replicii (e.g. exit).
%
% Hint: min_score, ord_subset, find_matching_rules

% select_answer/5
% select_answer(+Tokens, +UserMemory, +BotMemory, -Answer, -Actions)
emotion(UserMemory, []) :- get_emotion(UserMemory, neutru), !.
emotion(UserMemory, [X]) :- get_emotion(UserMemory, X).

user_tag(UserMemory, []) :- get_tag(UserMemory, none), !.
user_tag(UserMemory, [X]) :- get_tag(UserMemory, X).

select_answer(Tokens, UserMemory, BotMemory, Answer, Actions) :-
	findall(Aux2, (rules(T, Aux1), ord_subset(T, Tokens), member(Aux2, Aux1)), L), 
	find_matching_rules(Tokens, L, UserMemory, MatchingRules), 
	user_tag(UserMemory, T),
	findall((Key, Val), (member(rule(Tokens, A, _,  _, T), MatchingRules), member(B, A), unwords(B, Key), get_value(BotMemory, Key, Val)), Bag1),
	((Bag1 == [], emotion(UserMemory, E),
			findall((Key, Val), (member(rule(Tokens, A, _, E , _), MatchingRules), member(B, A), unwords(B, Key), get_value(BotMemory, Key, Val)), Bag));
	(member(rule(Tokens, A, _, E , _), Bag1), emotion(UserMemory, E),
			findall((Key, Val), (member(rule(Tokens, A, _, E , _), Bag1), member(B, A), unwords(B, Key), get_value(BotMemory, Key, Val)), Bag));
	( \+ member(rule(Tokens, A, _, E , _), Bag1), Bag = Bag1)), 
	min_element(Bag, A), words(A, Answer), add_answer(Answer, UserMemory, Mem),
	rules(_, Y), member(rule(_, X, Actions, _, _), Y), member(Answer, X).

% Esuează doar daca valoarea exit se afla in lista Actions.
% Altfel, returnează true.
% handle_actions/1
% handle_actions(+Actions)
handle_actions(Actions) :- \+ member(exit, Actions).


% Caută frecvența (numărul de apariți) al fiecarui cuvânt din fiecare
% cheie a memoriei.
% e.g
% ?- find_occurrences(memory{'joc tenis': 3, 'ma uit la box': 2, 'ma uit la un film': 4}, Result).
% Result = count{box:2, film:4, joc:3, la:6, ma:6, tenis:3, uit:6, un:4}.
% Observați ca de exemplu cuvântul tenis are 3 apariți deoarce replica
% din care face parte a fost spusă de 3 ori (are valoarea 3 în memorie).
% Recomandăm pentru usurința să folosiți înca un dicționar în care să tineți
% frecvențele cuvintelor, dar puteți modifica oricum structura, această funcție
% nu este testată direct.

% find_occurrences/2
% find_occurrences(+UserMemory, -Result)
add_dict(In, In, Key, 0) :- !.
add_dict(In, Out, Key, Val) :- 
	Val1 is Val - 1, add_dict(In, Out1, Key, Val1),
	add_answer(Key, Out1, Out).

insert_list(In, Out, [(Key, Val)]) :- words(Key, Word), add_dict(In, Out, Word, Val), !.
insert_list(In, Out, [(Key, Val)|Res]) :- insert_list(In, Out1, Res), words(Key, Word), add_dict(Out1, Out, Word, Val).

find_occurrences(UserMemory, Result) :- 
	dict_keys(UserMemory, Keys),
	findall((W, V), (member(K, Keys), get_value(UserMemory, K, V), words(K, Aux), member(W, Aux)), Bag),
	insert_list(memory{}, Result, Bag).

% Atribuie un scor pentru fericire (de cate ori au fost folosit cuvinte din predicatul happy(X))
% cu cât scorul e mai mare cu atât e mai probabil ca utilizatorul să fie fericit.
% get_happy_score/2
% get_happy_score(+UserMemory, -Score)
get_happy_score(UserMemory, Score) :-
	find_occurrences(UserMemory, Words),
	dict_keys(Words, Keys),
	findall((W, V), (member(K, Keys), get_value(Words, K, V), words(K, Aux), member(W, Aux), happy(W)), Vec),
	max_element(Vec, Key), member((Key, Score), Vec).

% Atribuie un scor pentru tristețe (de cate ori au fost folosit cuvinte din predicatul sad(X))
% cu cât scorul e mai mare cu atât e mai probabil ca utilizatorul să fie trist.
% get_sad_score/2
% get_sad_score(+UserMemory, -Score)
get_sad_score(UserMemory, Score) :- 
	find_occurrences(UserMemory, Words),
	dict_keys(Words, Keys),
	findall((W, V), (member(K, Keys), get_value(Words, K, V), words(K, Aux), member(W, Aux), sad(W)), Vec),
	max_element(Vec, Key), member((Key, Score), Vec).

% Pe baza celor doua scoruri alege emoția utilizatorul: `fericit`/`trist`,
% sau `neutru` daca scorurile sunt egale.
% e.g:
% ?- get_emotion(memory{'sunt trist': 1}, Emotion).
% Emotion = trist.
% get_emotion/2
% get_emotion(+UserMemory, -Emotion)
get_emotion(memory{}, neutru).
get_emotion(UserMemory, neutru) :- get_sad_score(UserMemory, S1), get_happy_score(UserMemory, S2), S1 =:= S2.
get_emotion(UserMemory, trist) :- get_sad_score(UserMemory, S1), get_happy_score(UserMemory, S2), S1 > S2.
get_emotion(UserMemory, fericit) :- get_sad_score(UserMemory, S1), get_happy_score(UserMemory, S2), S1 < S2.

% Atribuie un scor pentru un Tag (de cate ori au fost folosit cuvinte din lista tag(Tag, Lista))
% cu cât scorul e mai mare cu atât e mai probabil ca utilizatorul să vorbească despre acel subiect.
% get_tag_score/3
% get_tag_score(+Tag, +UserMemory, -Score)
comp_score([], 0).
comp_score([(K, V)|Res], Score) :- comp_score(Res, S1), Score is S1 + V.


get_tag_score(Tag, UserMemory, Score) :- 
	find_occurrences(UserMemory, Words),
	dict_keys(Words, Keys),
	findall((W, V), (member(K, Keys), get_value(Words, K, V), words(K, Aux), member(W, Aux), tag(Tag, L), member(W, L)), Vec),
	comp_score(Vec, Score).

% Pentru fiecare tag calculeaza scorul și îl alege pe cel cu scorul maxim.
% Dacă toate scorurile sunt 0 tag-ul va fi none.
% e.g:
% ?- get_tag(memory{'joc fotbal': 2, 'joc box': 3}, Tag).
% Tag = sport.
% get_tag/2
% get_tag(+UserMemory, -Tag)
get_tag(memory{}, none).
get_tag(UserMemory, Tag) :-
	findall((T, V), (tag(T, _), get_tag_score(T, UserMemory, V)), Vec),
	max_element(Vec, Tag).	
