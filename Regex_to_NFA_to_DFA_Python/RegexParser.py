# Generated from Regex.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b")
        buf.write("\60\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3")
        buf.write("\2\3\2\3\2\3\2\3\2\5\2\24\n\2\3\3\3\3\3\3\3\3\5\3\32\n")
        buf.write("\3\3\4\3\4\3\4\3\4\3\4\7\4!\n\4\f\4\16\4$\13\4\3\5\3\5")
        buf.write("\5\5(\n\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\2\3\6\b\2\4\6\b")
        buf.write("\n\f\2\2\2-\2\23\3\2\2\2\4\31\3\2\2\2\6\33\3\2\2\2\b\'")
        buf.write("\3\2\2\2\n)\3\2\2\2\f+\3\2\2\2\16\24\5\4\3\2\17\20\5\4")
        buf.write("\3\2\20\21\7\4\2\2\21\22\5\2\2\2\22\24\3\2\2\2\23\16\3")
        buf.write("\2\2\2\23\17\3\2\2\2\24\3\3\2\2\2\25\32\5\6\4\2\26\27")
        buf.write("\5\6\4\2\27\30\5\4\3\2\30\32\3\2\2\2\31\25\3\2\2\2\31")
        buf.write("\26\3\2\2\2\32\5\3\2\2\2\33\34\b\4\1\2\34\35\5\b\5\2\35")
        buf.write("\"\3\2\2\2\36\37\f\3\2\2\37!\7\3\2\2 \36\3\2\2\2!$\3\2")
        buf.write("\2\2\" \3\2\2\2\"#\3\2\2\2#\7\3\2\2\2$\"\3\2\2\2%(\5\f")
        buf.write("\7\2&(\5\n\6\2\'%\3\2\2\2\'&\3\2\2\2(\t\3\2\2\2)*\7\7")
        buf.write("\2\2*\13\3\2\2\2+,\7\5\2\2,-\5\2\2\2-.\7\6\2\2.\r\3\2")
        buf.write("\2\2\6\23\31\"\'")
        return buf.getvalue()


class RegexParser ( Parser ):

    grammarFileName = "Regex.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'*'", "'|'", "'('", "')'", "<INVALID>", 
                     "' '" ]

    symbolicNames = [ "<INVALID>", "KLEENE", "UNION", "OPEN", "CLOSED", 
                      "CHARACTER", "WS" ]

    RULE_expr = 0
    RULE_concat_expr = 1
    RULE_kleene_expr = 2
    RULE_atom = 3
    RULE_char = 4
    RULE_inner_expr = 5

    ruleNames =  [ "expr", "concat_expr", "kleene_expr", "atom", "char", 
                   "inner_expr" ]

    EOF = Token.EOF
    KLEENE=1
    UNION=2
    OPEN=3
    CLOSED=4
    CHARACTER=5
    WS=6

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def concat_expr(self):
            return self.getTypedRuleContext(RegexParser.Concat_exprContext,0)


        def UNION(self):
            return self.getToken(RegexParser.UNION, 0)

        def expr(self):
            return self.getTypedRuleContext(RegexParser.ExprContext,0)


        def getRuleIndex(self):
            return RegexParser.RULE_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = RegexParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expr)
        try:
            self.state = 17
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 12
                self.concat_expr()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 13
                self.concat_expr()
                self.state = 14
                self.match(RegexParser.UNION)
                self.state = 15
                self.expr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Concat_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def kleene_expr(self):
            return self.getTypedRuleContext(RegexParser.Kleene_exprContext,0)


        def concat_expr(self):
            return self.getTypedRuleContext(RegexParser.Concat_exprContext,0)


        def getRuleIndex(self):
            return RegexParser.RULE_concat_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConcat_expr" ):
                return visitor.visitConcat_expr(self)
            else:
                return visitor.visitChildren(self)




    def concat_expr(self):

        localctx = RegexParser.Concat_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_concat_expr)
        try:
            self.state = 23
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 19
                self.kleene_expr(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 20
                self.kleene_expr(0)
                self.state = 21
                self.concat_expr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Kleene_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(RegexParser.AtomContext,0)


        def kleene_expr(self):
            return self.getTypedRuleContext(RegexParser.Kleene_exprContext,0)


        def KLEENE(self):
            return self.getToken(RegexParser.KLEENE, 0)

        def getRuleIndex(self):
            return RegexParser.RULE_kleene_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitKleene_expr" ):
                return visitor.visitKleene_expr(self)
            else:
                return visitor.visitChildren(self)



    def kleene_expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = RegexParser.Kleene_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 4
        self.enterRecursionRule(localctx, 4, self.RULE_kleene_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self.atom()
            self._ctx.stop = self._input.LT(-1)
            self.state = 32
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = RegexParser.Kleene_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_kleene_expr)
                    self.state = 28
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 29
                    self.match(RegexParser.KLEENE) 
                self.state = 34
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def inner_expr(self):
            return self.getTypedRuleContext(RegexParser.Inner_exprContext,0)


        def char(self):
            return self.getTypedRuleContext(RegexParser.CharContext,0)


        def getRuleIndex(self):
            return RegexParser.RULE_atom

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom" ):
                return visitor.visitAtom(self)
            else:
                return visitor.visitChildren(self)




    def atom(self):

        localctx = RegexParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_atom)
        try:
            self.state = 37
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegexParser.OPEN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 35
                self.inner_expr()
                pass
            elif token in [RegexParser.CHARACTER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 36
                self.char()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CharContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CHARACTER(self):
            return self.getToken(RegexParser.CHARACTER, 0)

        def getRuleIndex(self):
            return RegexParser.RULE_char

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitChar" ):
                return visitor.visitChar(self)
            else:
                return visitor.visitChildren(self)




    def char(self):

        localctx = RegexParser.CharContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_char)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 39
            self.match(RegexParser.CHARACTER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Inner_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OPEN(self):
            return self.getToken(RegexParser.OPEN, 0)

        def expr(self):
            return self.getTypedRuleContext(RegexParser.ExprContext,0)


        def CLOSED(self):
            return self.getToken(RegexParser.CLOSED, 0)

        def getRuleIndex(self):
            return RegexParser.RULE_inner_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInner_expr" ):
                return visitor.visitInner_expr(self)
            else:
                return visitor.visitChildren(self)




    def inner_expr(self):

        localctx = RegexParser.Inner_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_inner_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self.match(RegexParser.OPEN)
            self.state = 42
            self.expr()
            self.state = 43
            self.match(RegexParser.CLOSED)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[2] = self.kleene_expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def kleene_expr_sempred(self, localctx:Kleene_exprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 1)
         




