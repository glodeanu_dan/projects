import sys
from typing import List, Tuple, Set, Dict
from antlr4 import *
from RegexVisitor import RegexVisitor
from RegexParser import RegexParser
from RegexLexer import RegexLexer

EPSILON = "eps"
alias = {}
current_state = 1
fin_state = 0

class RegexShowVisitor(RegexVisitor):
    state = 0
    alph = set()
    tran = []

    # Visit a parse tree produced by RegexParser#expr.
    def visitExpr(self, ctx:RegexParser.ExprContext):
        if ctx.UNION():
            start = self.state
            self.tran.append((self.state, EPSILON, self.state + 1))
            self.state += 1
            self.visit(ctx.concat_expr())
            state_concat = self.state
            self.tran.append((start, EPSILON, self.state + 1))
            self.state += 1
            self.visit(ctx.expr())
            state_expr = self.state
            self.tran.append((state_concat, EPSILON, self.state + 1))
            self.tran.append((state_expr, EPSILON, self.state + 1))
            self.state += 1

            return
        else:
            return self.visitChildren(ctx)


    # Visit a parse tree produced by RegexParser#concat_expr.
    def visitConcat_expr(self, ctx:RegexParser.Concat_exprContext):
        if ctx.concat_expr():
            self.visit(ctx.kleene_expr())
            self.tran.append((self.state, EPSILON, self.state + 1))
            self.state += 1
            self.visit(ctx.concat_expr())
            return
        else:
            return self.visitChildren(ctx)


    # Visit a parse tree produced by RegexParser#kleene_expr.
    def visitKleene_expr(self, ctx:RegexParser.Kleene_exprContext):
        if ctx.KLEENE():
            start = self.state
            self.tran.append((self.state, EPSILON, self.state + 1))
            self.state += 1
            self.visit(ctx.kleene_expr())
            self.state += 1
            self.tran.append((self.state - 1, EPSILON, self.state))
            self.tran.append((start , EPSILON, self.state))
            self.tran.append((self.state - 1, EPSILON, start + 1))
            return

        else:
            return self.visitChildren(ctx)


    # Visit a parse tree produced by RegexParser#atom.
    def visitAtom(self, ctx:RegexParser.AtomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegexParser#char.
    def visitChar(self, ctx:RegexParser.CharContext):
        self.tran.append((self.state, str(ctx.CHARACTER()), self.state + 1))
        self.state += 1
        self.alph.add(str(ctx.CHARACTER()))
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegexParser#inner_expr.
    def visitInner_expr(self, ctx:RegexParser.Inner_exprContext):
        return self.visitChildren(ctx)

class FA:
    def __init__(self, nr: int, alphabet: Set[chr], finalStates: Set[int],
                 delta: Dict[Tuple[int, chr], List[int]]):
        self.nr = nr
        self.states = set(range(self.nr))
        self.alphabet = alphabet
        self.initialState = 0
        self.finalStates = finalStates
        self.delta = delta

def read_nfa(file):
	nr = int(fIn.readline()[:-1])
	fin = set()
	alph = set()
	delta = {}

	for i in fIn.readline()[:-1].split():
		fin.add(int(i))

	line = fIn.readline().rstrip('\n')

	while line:
		aux = line.split()

		if aux[1] != EPSILON:
			alph.add(aux[1])

		my_key = (int(aux[0]), aux[1])
		aux = aux[2:]

		delta[my_key] = [int(a) for a in aux]

		line = fIn.readline().rstrip('\n')

	fa = FA(nr, alph, fin, delta)
	return fa

def epsilonClosure(state, delta):
	mySet = set()
	mySet.add(state)

	n = 0

	while len(mySet) != n:
		n = len(mySet)
		aux = [delta[(s,c)] for (s,c) in delta.keys() if (c == EPSILON) and (s in mySet)]
		for i in aux:
			mySet.update([a for a in i])

	return list(mySet)

def step(states, tran, delta):
        aux = [d for (s,c), d in delta.items() if s in states and c == tran]
        dest = set()

        for i in aux:
            dest.update(i)

        mySet = [s for s in dest]

        for s in mySet:
        	dest.update(epsilonClosure(s, delta))

        return list(dest)

def get_key(states):
	states.sort()
	return tuple(states)

def convert(nfa):
	nr = nfa.nr
	fin = set()
	alph = set()
	delta = {}
	finalStates = set()

	init = epsilonClosure(nfa.initialState, nfa.delta)
	alias[get_key(init)] = 0
	current_state = 1
	visited = set()
	visited.add(get_key(init))

	for i in nfa.finalStates:
		if i in init:
			finalStates.add(alias[get_key(init)])

	q = [init]
	print(len(init))

	while len(q) != 0:
		state = q[0]
		q.pop(0)

		for c in nfa.alphabet:
			next_state = step(state, c, nfa.delta)

			if next_state and not (get_key(next_state) in visited):
				visited.add(get_key(next_state))
				q.append(next_state)
				alias[get_key(next_state)] = current_state
				current_state += 1
				
				for i in nfa.finalStates:
					if i in next_state:
						finalStates.add(alias[get_key(next_state)])
			
			if next_state:
				delta[(alias[get_key(state)], c)] = alias[get_key(next_state)]

	aux = 1
	for c in nfa.alphabet:
		for s in alias.keys():
			if not (alias[s],c) in delta.keys():
				if aux:
					aux = 0
					
					for i in nfa.alphabet:
						delta[(current_state, i)] = current_state
					current_state += 1

				delta[(alias[s],c)] = current_state - 1


	dfa = FA(current_state, nfa.alphabet, finalStates, delta)
	return dfa

def write_dfa(dfa, f):
	f.write(str(dfa.nr) + '\n')

	for i in dfa.finalStates:
		f.write(str(i))
		f.write(' ')

	f.write('\n')

	for (s, c) in dfa.delta.keys():
		f.write(str(s) + ' ' + c + ' ' + str(dfa.delta[(s, c)]) + '\n')

def write_nfa(nfa, f):
	f.write(str(nfa.nr) + '\n')

	for i in nfa.finalStates:
		f.write(str(i))
		f.write(' ')

	f.write('\n')

	for (s, c) in nfa.delta.keys():
		states = ''

		for state in nfa.delta[(s, c)]:
			states += str(state) + ' '

		f.write(str(s) + ' ' + c + ' ' + states[:-1] + '\n')

def nfa_to_dfa(fIN):
	input = FileStream(fIN)
	lexer = RegexLexer(input)
	stream = CommonTokenStream(lexer)
	parser = RegexParser(stream)
	tree = parser.expr()
	delta = {}

	showVisitor = RegexShowVisitor()
	showVisitor.visit(tree)

	for (s1, c, s2) in showVisitor.tran:
		print((s1, c, s2))
		print((s1, c) in delta.keys())
		if (s1, c) in delta.keys():
			delta[(s1, c)].append(s2)
		else:
			delta[(s1, c)] = [s2]

	print(delta)
	print(showVisitor.alph)
	print(showVisitor.state)

	fa = FA(showVisitor.state + 1, showVisitor.alph, 
		[showVisitor.state], delta)
	return fa

if __name__ == '__main__':
	# fIn = open(sys.argv[1], 'r')
	fOut1 = open(sys.argv[2], 'w')
	fOut2 = open(sys.argv[3], 'w')
	# nfa = read_nfa(fIn)
	nfa = nfa_to_dfa(sys.argv[1])
	write_nfa(nfa, fOut1)
	dfa = convert(nfa)
	write_dfa(dfa, fOut2)
	fOut1.close();
	fOut2.close();
