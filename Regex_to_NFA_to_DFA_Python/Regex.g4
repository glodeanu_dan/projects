grammar Regex;

KLEENE : '*' ;
UNION : '|' ;
OPEN : '(' ;
CLOSED : ')' ;
CHARACTER : [a-z] ;
WS : ' ' -> skip ;

expr : concat_expr | concat_expr UNION expr ;
concat_expr : kleene_expr | kleene_expr concat_expr ;
kleene_expr : atom | kleene_expr KLEENE ;
atom: inner_expr | char ;
char : CHARACTER ;
inner_expr : OPEN expr CLOSED ;
