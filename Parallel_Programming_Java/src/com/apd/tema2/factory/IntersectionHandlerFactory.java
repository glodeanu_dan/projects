package com.apd.tema2.factory;

import com.apd.tema2.Main;
import com.apd.tema2.entities.*;
import com.apd.tema2.intersections.*;
import com.apd.tema2.utils.Constants;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

/**
 * Clasa Factory ce returneaza implementari ale InterfaceHandler sub forma unor
 * clase anonime.
 */
public class IntersectionHandlerFactory {

    public static IntersectionHandler getHandler(String handlerType) {
        // simple semaphore intersection
        // max random N cars roundabout (s time to exit each of them)
        // roundabout with exactly one car from each lane simultaneously
        // roundabout with exactly X cars from each lane simultaneously
        // roundabout with at most X cars from each lane simultaneously
        // entering a road without any priority
        // crosswalk activated on at least a number of people (s time to finish all of
        // them)
        // road in maintenance - 2 ways 1 lane each, X cars at a time
        // road in maintenance - 1 way, M out of N lanes are blocked, X cars at a time
        // railroad blockage for s seconds for all the cars
        // unmarked intersection
        // cars racing
        return switch (handlerType) {
            case "simple_semaphore" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    System.out.println("Car " + car.getId() + " has reached the semaphore, now waiting...");

                    try {
                        sleep(car.getWaitingTime());
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Car " + car.getId() + " has waited enough, now driving...");
                }
            };
            case "simple_n_roundabout" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Simple_n_roundabout intersection = (Simple_n_roundabout) Main.intersection;
                    System.out.println("Car " + car.getId() + " has reached the roundabout, now waiting...");
                    try {
                        intersection.cars.acquire();
                    }
                    catch (InterruptedException e) {

                    }
                    System.out.println("Car " + car.getId() + " has entered the roundabout");

                    try {
                        sleep(intersection.time);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Car " + car.getId() + " has exited the roundabout after " +
                            (intersection.time / 1000) + " seconds");

                    try {
                        intersection.cars.release();
                    }
                    catch (Exception e) {

                    }
                }
            };
            case "simple_strict_1_car_roundabout" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Simple_strict_1_car_roundabout intersection = (Simple_strict_1_car_roundabout) Main.intersection;

                    System.out.println("Car " + car.getId() + " has reached the roundabout");

//                  masina se sincronizeaza pe obiectul corespunzator cu directia sa
                    synchronized (intersection.directions.get(car.getStartDirection())) {
                        System.out.println("Car " + car.getId() + " has entered the roundabout from lane "
                                + car.getStartDirection());

                        try {
                            sleep(intersection.time);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        System.out.println("Car " + car.getId() + " has exited the roundabout after " +
                                (intersection.time / 1000) + " seconds");
                    }
                }
            };
            case "simple_strict_x_car_roundabout" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Simple_strict_x_car_roundabout intersection = (Simple_strict_x_car_roundabout) Main.intersection;

                    System.out.println("Car " + car.getId() + " has reached the roundabout, now waiting...");

//                  masina se sincronizeaza pe semaforul corespunzator cu directia sa
                    try {
                        intersection.directions.get(car.getStartDirection()).acquire();
                        System.out.println("Car " + car.getId() + " was selected to enter the roundabout from lane "
                                + car.getStartDirection());
                    }
                    catch (InterruptedException e) {

                    }

                    try {
                        intersection.barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Car " + car.getId() + " has entered the roundabout from lane "
                        + car.getStartDirection());

                    try {
                        sleep(intersection.time);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Car " + car.getId() + " has exited the roundabout after " +
                            (intersection.time / 1000) + " seconds");

                    try {
                        try {
                            intersection.barrier.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (BrokenBarrierException e) {
                            e.printStackTrace();
                        }intersection.directions.get(car.getStartDirection()).release();
                    }
                    catch (Exception e) {

                    }
                }
            };
            case "simple_max_x_car_roundabout" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    // Get your Intersection instance

                    try {
                        sleep(car.getWaitingTime());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } // NU MODIFICATI

                    // Continuati de aici

                    Simple_max_x_car_roundabout intersection = (Simple_max_x_car_roundabout) Main.intersection;

                    System.out.println("Car " + car.getId() + " has reached the roundabout from lane "
                            + car.getStartDirection());

//                  masina se sincronizeaza pe obiectul corespunzator cu directia sa
                    try {
                        intersection.directions.get(car.getStartDirection()).acquire();
                    }
                    catch (InterruptedException e) {

                    }
                    System.out.println("Car " + car.getId() + " has entered the roundabout from lane "
                            + car.getStartDirection());

                    try {
                        sleep(intersection.time);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Car " + car.getId() + " has exited the roundabout after " +
                            (intersection.time / 1000) + " seconds");

                    try {
                        intersection.directions.get(car.getStartDirection()).release();
                    }
                    catch (Exception e) {

                    }
                }
            };
            case "priority_intersection" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    // Get your Intersection instance

                    try {
                        sleep(car.getWaitingTime());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } // NU MODIFICATI

                    // Continuati de aici

                    Priority_intersection intersection = (Priority_intersection) Main.intersection;

//                  separ masinile cu prioritate si fara
                    if (car.getPriority() != 1) {
//                      adaug masina in coada pentru masini cu prioritate

                        synchronized (intersection.nr) {
                            intersection.nr++;

                            if (intersection.nr == 1) {
                                try {
                                    intersection.free.acquire();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        synchronized (this) {
                            System.out.println("Car " + car.getId() + " with high priority has entered the intersection");

                            try {
                                sleep(intersection.time);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            System.out.println("Car " + car.getId() + " with high priority has exited the intersection");
                        }

                        synchronized (intersection.nr) {
                            intersection.nr--;

                            if (intersection.nr == 0) {
                                intersection.free.release();
                            }
                        }


                    }
                    else {
//                      adaug masina in coada masinilor fara prioritate
                        System.out.println("Car " + car.getId() + " with low priority is trying to enter the intersection...");

                        synchronized (intersection.nr) {
                            if (intersection.nr == 0) {
                                System.out.println("Car " + car.getId() + " with low priority has entered the intersection");
                                return;
                            }
                        }

                        try {
                            intersection.free.acquire();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        System.out.println("Car " + car.getId() + " with low priority has entered the intersection");

                        intersection.free.release();
                    }
                }
            };
            case "crosswalk" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Crosswalk intersection = (Crosswalk) Main.intersection;

                    try {
                        intersection.q.put(car);
                    }
                    catch (Exception e) {
                    }

                    System.out.println("Car " + car.getId() + " has now green light");

                    try {
                        intersection.barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                    while (!intersection.q.isEmpty()) {
                        if (intersection.q.peek() == car) {
//                          este verde si masinile se invart in cerc
                            if (!Main.pedestrians.isFinished() && !Main.pedestrians.isPass()) {
                                try {
                                    intersection.q.put(car);
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    intersection.q.take();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
//                          s-au strans suficienti pietoni si au inceput sa traverseze masinile afiseaza ca este rosu
                            else if (!Main.pedestrians.isFinished() && Main.pedestrians.isPass() && intersection.isGreen[car.getId()]) {
                                System.out.println("Car " + car.getId() + " has now red light");
                                intersection.isGreen[car.getId()] = false;

                                try {
                                    intersection.q.put(car);
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    intersection.q.take();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
//                          pietonii au trecut, masinile trec pentru ultima data
                            else if (Main.pedestrians.isFinished() && !intersection.isGreen[car.getId()]) {
                                System.out.println("Car " + car.getId() + " has now green light");

                                try {
                                    intersection.q.take();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
//                          nu au fost pietnoi
                            else if (Main.pedestrians.isFinished()) {
                                try {
                                    intersection.q.take();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            };
            case "simple_maintenance" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Simple_maintenance intersection = (Simple_maintenance) Main.intersection;

                    System.out.println("Car " + car.getId() + " from side number " + car.getStartDirection() +
                            " has reached the bottleneck");

                    while (true) {
                        synchronized (intersection) {
                            if (car.getStartDirection() == intersection.dir) {
                                intersection.nr--;
                                System.out.println("Car " + car.getId() + " from side number " + car.getStartDirection() +
                                        " has passed the bottleneck");
//                              daca au trecut suficiente masini resetez contorul si schimb directia
                                if (intersection.nr == 0) {
                                    if (car.getStartDirection() == 0) {
                                        intersection.dir = 1;
                                    }
                                    else {
                                        intersection.dir = 0;
                                    }
                                    intersection.nr = intersection.capacity;
                                }

                                break;
                            }
                        }
                    }
                }
            };
            case "complex_maintenance" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Complex_maintenance intersection = (Complex_maintenance) Main.intersection;
                    boolean done = false;
                    int way = intersection.getWay(car.getStartDirection());

                    System.out.println("Car " + car.getId() + " has come from the lane number "
                            + car.getStartDirection());

                    synchronized (this) {
                        intersection.waiting[car.getStartDirection()]++;
                        try {
                            intersection.q.get(car.getStartDirection()).put(car);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    try {
                        intersection.barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                    while (true) {
//                      semafor pentru fiecare banda de redirectare
                        try {
                            intersection.roads[way].acquire();
                        }
                        catch (InterruptedException e) {

                        }

//                      daca pe acest segment directia coincide cu directia masina incearca sa teraca
                        if (car.getStartDirection() == intersection.dir[way] && car == intersection.q.get(car.getStartDirection()).peek()) {
                            synchronized (intersection.obj[way]) {
                                intersection.nr[way]--;
                                intersection.waiting[car.getStartDirection()]--;
                            }


                            System.out.println("Car " + car.getId() + " from the lane " + car.getStartDirection() +
                                    " has entered lane number " + way);

                            try {
                                intersection.q.get(car.getStartDirection()).take();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }


//                          daca au trecut tate masinile pe acest sens sau nu mai exista masini pe sens care sa treaca
//                          se trece la sensul urmator si se reseteaza capacitatea
                            if (intersection.nr[way] == 0 || intersection.waiting[car.getStartDirection()] == 0) {
//                              cazuri cand repun in coada sau scot
                                if (intersection.waiting[car.getStartDirection()] == 0) {
                                    System.out.println("The initial lane " + car.getStartDirection() +
                                            " has been emptied and removed from the new lane queue");
                                }
                                else {
                                    System.out.println("The initial lane " + car.getStartDirection() +
                                            " has no permits and is moved to the back of the new lane queue");
                                }

                                intersection.nr[way] = intersection.capacity;

                                intersection.dir[way] = intersection.getNextWay(intersection.dir[way], way);
                                while (intersection.waiting[intersection.dir[way]] == 0 && intersection.notZeroOnly(way)) {
                                    intersection.dir[way] = intersection.getNextWay(intersection.dir[way], way);
                                }

                            }

                            done = true;
                        }

                        try {
                            intersection.roads[way].release();
                        }
                        catch (Exception e) {

                        }

                        if (done) {
                            break;
                        }
                    }
                }
            };
            case "railroad" -> new IntersectionHandler() {
                @Override
                public void handle(Car car) {
                    Railroad intersection = (Railroad) Main.intersection;

//                  adaug masinile in cozi
                    if (car.getStartDirection() == 0) {
                        synchronized (intersection.first) {
                            try {
                                intersection.first.put(car);
                                System.out.println("Car " + car.getId() + " from side number 0 has stopped by the railroad");
                            }
                            catch (Exception e) {
                            }
                        }
                    }
                    else {
                        synchronized (intersection.second) {
                            try {
                                intersection.second.put(car);
                                System.out.println("Car " + car.getId() + " from side number 1 has stopped by the railroad");
                            }
                            catch (Exception e) {
                            }
                        }
                    }


                    try {
                        intersection.barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                    if (car.getId() == 0) {
                        System.out.println("The train has passed, cars can now proceed");
                    }

                    ArrayBlockingQueue<Car> q;
                    if (car.getStartDirection() == 0) {
                        q = intersection.first;
                    }
                    else {
                        q = intersection.second;
                    }

//                  scot cate o masina din coada si trece peste calea ferata
                    while (!q.isEmpty()) {
                        if (car == q.peek()) {
                            try {
                                intersection.train.acquire();
                            }
                            catch (InterruptedException e) {

                            }

                            System.out.println("Car " + car.getId() + " from side number " + car.getStartDirection() +
                                    " has started driving");

                            try {
                                q.take();
                            }
                            catch (Exception e) {
                            }

                            try {

                                intersection.train.release();
                            }
                            catch (Exception e) {

                            }
                        }
                    }

                }
            };
            default -> null;
        };
    }
}
