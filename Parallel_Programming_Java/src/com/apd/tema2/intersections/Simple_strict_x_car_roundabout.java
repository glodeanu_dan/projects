package com.apd.tema2.intersections;

import com.apd.tema2.entities.Intersection;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Simple_strict_x_car_roundabout implements Intersection {
    public int time;
    public int nr;
    public int max;
    public ArrayList<Semaphore> directions = new ArrayList<Semaphore>();
    public CyclicBarrier barrier;

    public void set(String[] line) {
        nr = Integer.parseInt(line[0]);
        time = Integer.parseInt(line[1]);
        max = Integer.parseInt(line[2]);

        barrier = new CyclicBarrier(nr * max);

        for (int i = 0; i < nr; i++) {
            directions.add(new Semaphore(max));
        }
    }

}
