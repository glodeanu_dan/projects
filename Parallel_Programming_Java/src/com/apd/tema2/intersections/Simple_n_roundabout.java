package com.apd.tema2.intersections;

import com.apd.tema2.entities.Intersection;

import java.util.concurrent.Semaphore;

public class Simple_n_roundabout implements Intersection {
    public int time;
    public int nr;
    public Semaphore cars;

    public void set(String[] line) {
        nr = Integer.parseInt(line[0]);
        time = Integer.parseInt(line[1]);
        cars = new Semaphore(nr);
    }

}
