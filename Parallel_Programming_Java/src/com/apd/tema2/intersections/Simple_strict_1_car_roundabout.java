package com.apd.tema2.intersections;

import com.apd.tema2.entities.Intersection;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Simple_strict_1_car_roundabout implements Intersection {
    public int time;
    public int nr;
    public ArrayList<Object> directions = new ArrayList<Object>();

    public void set(String[] line) {
        nr = Integer.parseInt(line[0]);
        time = Integer.parseInt(line[1]);

        for (int i = 0; i < nr; i++) {
            directions.add(new Object());
        }
    }

}
