package com.apd.tema2.intersections;

import com.apd.tema2.entities.Car;
import com.apd.tema2.entities.Intersection;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Complex_maintenance implements Intersection {
    public int[] dir;
    public int[] nr;
    public ArrayList<ArrayBlockingQueue<Car>> q = new ArrayList<>();
    public int capacity;
    public int N;
    public int M;
    public int[] waiting;
    public Semaphore[] roads;
    public CyclicBarrier barrier;
    public Object[] obj;

    public void set(String[] line) {
        M = Integer.parseInt(line[0]);
        N = Integer.parseInt(line[1]);
        capacity = Integer.parseInt(line[2]);
        dir = new int[M];
        nr = new int[M];
        waiting = new int[N];
        roads = new Semaphore[M];
        obj = new Object[M];

        for (int i = 0; i < M; i++) {
            obj[i] = new Object();
            nr[i] = capacity;
            dir[i] = i * N / M;;
            roads[i] = new Semaphore(1);
        }

        for (int i = 0; i < N; i++) {
            waiting[i] = 0;
        }

    }

    public void initBarrier(int n) {
        barrier = new CyclicBarrier(n);

        for (int i = 0; i < N; i++) {
            q.add(new ArrayBlockingQueue<Car>(n));
        }
    }

    public boolean notZeroOnly(int way) {
        int start = way * N / M;
        int end = Math.min((way + 1) * N / M, N);

        for (int i = start; i < end; i++) {
            if (waiting[i] != 0) {
                return true;
            }
        }

        return false;
    }

    public int getWay(int d) {
        for (int i = 0; i < M; i++) {
            int start = i * N / M;
            int end = Math.min((i + 1) * N / M, N);

            if (d >= start && d < end) {
                return i;
            }
        }

        return 0;
    }

    public int getNextWay(int d, int way) {
        int start = way * N / M;
        int end = Math.min((way + 1) * N / M, N);
        int next = d + 1;

        if (next >= end) {
            next = start;
        }

        return next;
    }
}
