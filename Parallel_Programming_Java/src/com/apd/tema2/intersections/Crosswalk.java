package com.apd.tema2.intersections;

import com.apd.tema2.entities.Car;
import com.apd.tema2.entities.Intersection;
import com.apd.tema2.utils.Constants;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Crosswalk implements Intersection {
    public int time;
    public static ArrayBlockingQueue<Car> q;
    public boolean[] isGreen;
    public CyclicBarrier barrier;

    public void set(String[] line) {

    }

    public void initQ(int n) {
        q = new ArrayBlockingQueue<Car>(n + 1);
        barrier = new CyclicBarrier(n);
        isGreen = new boolean[n];

        for (int i = 0; i < n; i++) {
            isGreen[i] = true;
        }
    }
}
