package com.apd.tema2.intersections;

import com.apd.tema2.entities.Car;
import com.apd.tema2.entities.Intersection;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Priority_intersection implements Intersection {
    public boolean priority;
    public Integer nr;
    public int time;
    public Semaphore free = new Semaphore(1);

    public void set(String[] line) {
        priority = false;
        nr = 0;
        time = 2000;
    }

}
