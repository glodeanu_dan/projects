package com.apd.tema2.intersections;

import com.apd.tema2.entities.Car;
import com.apd.tema2.entities.Intersection;
import com.apd.tema2.utils.Constants;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Railroad implements Intersection {
    public int time;
    public Semaphore train = new Semaphore(1);
    public static ArrayBlockingQueue<Car> first;
    public static ArrayBlockingQueue<Car> second;
    public CyclicBarrier barrier;

    public void set(String[] line) {
        time = Constants.TRAIN_PASSING_TIME;
        first = new ArrayBlockingQueue<>(Integer.parseInt(line[0]));
        second = new ArrayBlockingQueue<>(Integer.parseInt(line[0]));
    }

    public void initBarier(int n) {
        barrier = new CyclicBarrier(n);
        first = new ArrayBlockingQueue<Car>(n);
        second = new ArrayBlockingQueue<Car>(n);
    }

}
