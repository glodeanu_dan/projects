#ifndef TREEMAP_H_
#define TREEMAP_H_

#include <stdlib.h>

#define MAX(a, b) (((a) >= (b))?(a):(b))

//-------------------------------------------------------------------------

typedef struct node{
	void* elem; // Node key
	void* info; // Node satellite information
	struct node *pt; // Parent link
	struct node *lt; // left child link
	struct node *rt; // right child link
	struct node* next; // next link in the list of duplicates
	struct node* prev; // prev link in the list of duplicates
	struct node* end; // pointer to the end of the duplicates list
	long height;
}TreeNode;

typedef struct TTree{
	TreeNode *root; // pointer to the root of the tree
	void* (*createElement)(void*); // create element method
	void (*destroyElement)(void*); // destroy element method
	void* (*createInfo)(void*); // create info method
	void (*destroyInfo)(void*); // destroy info method
	int (*compare)(void*, void*); // comparison method between elements
	long size;
}TTree;

////////////////////////////////////////////

void print2DUtil(TreeNode *root, int space) 
{ 
	int COUNT = 4;
    // Base case 
    if (root == NULL) 
        return; 
  
    // Increase distance between levels 
    space += COUNT; 
  
    // Process right child first 
    print2DUtil(root->rt, space); 
  
    // Print current TreeNode after space 
    printf("\n"); 
    for (int i = COUNT; i < space; i++) 
        printf(" "); 
    // printf("%s\n", (char*)(root->elem)); 
    printf("%d\n", *(int*)(root->elem)); 
  
    // Process left child 
    print2DUtil(root->lt, space); 
} 
  
// Wrapper over print2DUtil() 
void print2D(TreeNode *root) 
{ 
   // Pass initial space count as 0 
   print2DUtil(root, 0); 
} 

///////////////////////////////////////////

void printlist (TreeNode *h) {
	TreeNode *aux = h;

	while (aux != NULL) {
		printf("%d ", *(int*)aux->elem);
		// printf("%s ", (char*)aux->elem);
		aux = aux->next;
	}
	printf("\n");
}

/******************************************/

/////////////////////////////////////////
int depth(TreeNode* node) {  
    if (node == NULL)  
        return 0;  
    else
    {  
        int l = depth(node->lt);  
        int r = depth(node->rt);  
      
        if (l > r)  
            return(l + 1);  
        else return(r + 1);  
    }  
}

void re_height(TreeNode* node) {  
    if (node == NULL)  
        return;  
    
    re_height(node->lt);
    re_height(node->rt);
    node->height = depth(node);
}

TTree* createTree(void* (*createElement)(void*), void (*destroyElement)(void*),
		void* (*createInfo)(void*), void (*destroyInfo)(void*),
		int compare(void*, void*)){

	TTree *tree = malloc(sizeof(TTree));

	if (tree == NULL) {
		return NULL;
	}

	tree->root = NULL;
	tree->createElement = createElement;
	tree->destroyElement = destroyElement;
	tree->createInfo = createInfo;
	tree->destroyInfo = destroyInfo;
	tree->compare = compare;
	tree->size = 0;

	return tree;
}

int isEmpty(TTree* tree){
	if (tree->size == 0) 
		return 1;
	return 0;
}


TreeNode* search(TTree* tree, TreeNode* x, void* elem){
	
	TreeNode *node = tree->root;

	while (node != NULL && tree->compare(node->elem, elem) != 0) {
		if (tree->compare(node->elem, elem) == 1) {
			node = node->lt;
		}
		else {
			node = node->rt;
		}
	}

	return node;
}


TreeNode* minimum(TreeNode* x){
	TreeNode *aux = x;	

	while (aux->lt != NULL) {
		aux = aux->lt;
	}

	return aux;
}

TreeNode* maximum(TreeNode* x){
	TreeNode *aux = x;

	while (aux->rt != NULL) {
		aux = aux->rt;
	}

	return aux;
}

TreeNode* successor(TreeNode* x){
	TreeNode *aux, *test = x;

	while (test->pt != NULL) {
		test = test->pt;
	}

	if (x == maximum(test)) {
		return NULL;
	}

	if (x->rt != NULL) {
		aux = x->rt;
		while (aux->lt != NULL) {
			aux = aux->lt;
		}
	}
	else {
		TreeNode *n1 = x, *n2 = n1->pt;

		while (n2->lt != n1 && n2 != NULL) {
			n1 = n1->pt;
			n2 = n2->pt;
		}
		aux = n2;
	}
	return aux;
}

TreeNode* predecessor(TreeNode* x){
	TreeNode *aux, *test = x;

	while (test->pt != NULL) {
		test = test->pt;
	}
	if (x == minimum(test))
		return NULL;

	if (x->lt != NULL) {
		aux = x->lt;
		while (aux->rt != NULL) {
			aux = aux->rt;
		}
	}
	else {
		TreeNode *n1 = x, *n2 = n1->pt;

		while (n2->rt != n1 && n2 != NULL) {
			n1 = n1->pt;
			n2 = n2->pt;
		}
		aux = n2;
	}

	return aux;
}

void updateHeight(TreeNode* x){
	int leftHeight = 0;
	int rightHeight = 0;
	if(x != NULL){
		if(x->lt != NULL) leftHeight = x->lt->height;
		if(x->rt != NULL) rightHeight = x->rt->height;
		x->height = MAX(leftHeight, rightHeight) + 1;
	}
}

void avlRotateLeft(TTree* tree, TreeNode* node){
	TreeNode *piv = node->rt, *p = node->pt;

	node->rt = piv->lt;
	if (piv->lt != NULL) {
		piv->lt->pt = node;
	}
	piv->lt = node;
	node->pt = piv;

	if (p == NULL) {
		tree->root = piv;
		piv->pt = NULL;
	}
	else {
		piv->pt = p;

		if (tree->compare(p->elem, piv->elem) == 1) {
			p->lt = piv;
		}
		else {
			p->rt = piv;
		}
	}
}

void avlRotateRight(TTree* tree, TreeNode* node){
	TreeNode *piv = node->lt, *p = node->pt;

	node->lt = piv->rt;
	if (piv->rt != NULL) {
		piv->rt->pt = node;
	}
	node->pt = piv;
	piv->rt = node;

	if (p == NULL) {
		tree->root = piv;
		piv->pt = NULL;
	}
	else {
		piv->pt = p;
		if (tree->compare(p->elem, piv->elem) == 1) {
			p->lt = piv;
		}
		else {
			p->rt = piv;
		}
	}
}

/* Get AVL balance factor for node x */
int avlGetBalance(TreeNode *node){
	int l, r;

	if (node->lt == NULL) {
		l = 0;
	}
	else {
		l = node->lt->height;
	}

	if (node->rt == NULL) {
		r = 0;
	}
	else {
		r = node->rt->height;
	}
	return l-r;
}


void avlFixUp(TTree* tree, TreeNode* p){
	while (p != NULL) {
		if (avlGetBalance(p) < -1) {

			if (avlGetBalance(p->rt) <= 0) {
				avlRotateLeft(tree, p);
			}
			else {
				avlRotateRight(tree, p->rt);
				avlRotateLeft(tree, p);
			}
		}
		else if (avlGetBalance(p) > 1) {

			if (avlGetBalance(p->lt) >= 0) {
				avlRotateRight(tree, p);
			}
			else {
				avlRotateLeft(tree, p->lt);
				avlRotateRight(tree, p);
			}
		}
		re_height(tree->root);
		p = p->pt;
	}
}


TreeNode* createTreeNode(TTree *tree, void* value, void* info){
	// Allocate node
	TreeNode* newNode = (TreeNode*) malloc(sizeof(TreeNode));

	// Set its element field
	newNode->elem = tree->createElement(value);

	//Set its info
	newNode->info = tree->createInfo(info);

	// Set its tree links
	newNode->pt = newNode->rt = newNode->lt = NULL;

	// Set its list links
	newNode->next = newNode->prev = newNode->end = NULL;

	/*
	 *  The height of a new node is 1,
	 *  while the height of NULL is 0
	 */
	newNode->height = 1;

	return newNode;
}

void destroyTreeNode(TTree *tree, TreeNode* node){
	// Check arguments
	if(tree == NULL || node == NULL) return;

	// Use object methods to de-alocate node fields
	tree->destroyElement(node->elem);
	tree->destroyInfo(node->info);

	// De-allocate the node
	free(node);
}

void makelist (TTree *tree, TreeNode *node) {
    if (tree == NULL) {
    	return;
    }

    node->end = node;
    if (tree->size == 1) {
    	tree->root->next = NULL;
    	tree->root->prev = NULL;
    	tree->root->end = tree->root;
    	return;
    }
    else if (minimum(tree->root) == node) {
    	node->prev = NULL;
    	node->next = successor(node);
    	successor(node)->prev = node;
    	return;
    }
    else if (maximum(tree->root) == node) {
    	node->next = NULL;
    	node->prev = predecessor(node);
    	predecessor(node)->end->next = node;
    	return;
    }
    else {
    	node->next = successor(node);
    	if (successor(node) != NULL)
    		successor(node)->prev = node;
    	node->prev = predecessor(node);
    	if (predecessor(node) != NULL)
    		predecessor(node)->end->next = node;
    	return;
    }
 
    int i, n = tree->size;
    TreeNode *prev = NULL;
    TreeNode *list = minimum(tree->root);

    if (list != NULL ) {
    	list->prev = NULL;
    }
    // list = list->end;

    while (list != NULL) {
        list->prev = prev;
        
        if (prev != NULL) {
        	prev->next = list;
        }

        list->end = list;
        prev = list;
        list = successor(list);
    }

    if (maximum(tree->root) != NULL) 
    	(maximum(tree->root))->next = NULL;
}

void insert(TTree* tree, void* elem, void* info) {
	TreeNode *node = createTreeNode(tree, elem, info);

	TreeNode *copy = search(tree, tree->root, elem);

	if (copy != NULL) {
		copy = copy->end;

		node->height = copy->height;

		node->next = copy->next;
		if (copy->next != NULL)
			copy->next->prev = node;

		copy->next = node;
		node->prev = copy;

		TreeNode *aux = node;

		while (aux != NULL && tree->compare(aux->elem, node->elem) == 0) {
			aux->end = node;
			aux = aux->prev;
		}

		return;
	}
	if (isEmpty(tree)) {
		tree->root = node;
		tree->size++;
	}
	else if (search(tree, NULL, elem) == NULL) {
		TreeNode *aux = tree->root,
				 *p, *prev;

		tree->size++;
		while (aux != NULL) {
			p = aux;
			if (tree->compare(aux->elem, elem) == 1) {
				aux = aux->lt;
			}
			else {
				aux = aux->rt;
			}
		}

		if (tree->compare(p->elem, elem) == 1) {
			p->lt = node;
			node->pt = p;
			p->height++;
		}
		else {
			p->rt = node;
			node->pt = p;
			p->height++;
		}
		
		avlFixUp(tree, p);
		re_height(tree->root);
	}
	makelist(tree, node);
}

void avlFix(TTree *tree, TreeNode *node) {
	avlFixUp(tree, node->lt);
	avlFixUp(tree, node->rt);
	avlFixUp(tree, node);
}

void delete(TTree* tree, void* elem){
	TreeNode *node = search(tree, tree->root, elem);

	if (node != node->end) {

		TreeNode *aux = node->end;

		aux->prev->next = aux->next;
		if (aux->next != NULL)
			aux->next->prev = aux->prev;

		TreeNode *tmp = aux->prev;

		while (tmp != NULL && tree->compare(tmp->elem, aux->elem) == 0) {
			tmp->end = aux->prev;
			tmp = tmp->prev;
		}

		destroyTreeNode(tree, aux);
		return;
	}
	else if (!(node->prev == NULL && node->next == NULL)) {
		if (node->prev == NULL) {
			if (node->end == node) {
				node->next->prev = NULL;
			}
			else {
				node = node->end;
				node->prev->next = node->next;
				node->next->prev = node->prev;
			}
		}
		else if (node->end->next == NULL) {
			if (node->end == node) {
				node->prev->next = NULL;
			}
			else {
				node = node->end;
				node->prev->next = NULL;
			}
		}
		else {
			node = node->end;
			node->prev->next = node->next;
			node->next->prev = node->prev;
		}
	}

	if (node == NULL || tree->root == NULL) {
		return;
	}


	TreeNode *parent = node->pt;
	TreeNode *piv = node->rt;

	if (node->lt == NULL && node->rt == NULL) {
		if (parent != NULL) {
			if (parent->rt == node) {
				parent->rt = NULL;
			}
			else {
				parent->lt = NULL;
			}
		}
		else {
			tree->root = NULL;
		}
		re_height(tree->root);
		avlFixUp(tree, parent);
		re_height(tree->root);
		tree->size--;
		destroyTreeNode(tree, node);
		return;
	}
	else if (piv->lt == NULL) {
		piv->lt = node->lt;
	}
	else {
		while (piv->lt != NULL) {
			piv = piv->lt;
		}
	}

	TreeNode *piv_pt = piv->pt;
	
	if (piv != NULL) {
		piv->pt = parent;
		
		if (parent == NULL) {
			tree->root = piv;
		}
		else {
			if (tree->compare(parent->elem, elem) == 1) {
				parent->lt = piv;
			}
			else {
				parent->rt = piv;
			}
		}
	}
	if (node->rt == piv) {
		node->rt = NULL;
	}
	if (node->lt == piv) {
		node->lt = NULL;
	}
	if (piv_pt != node) {
		piv_pt->lt = piv->rt;
		if (piv->rt != NULL) 
			piv->rt->pt = piv_pt;
	}
	piv->rt = node->rt;
	piv->lt = node->lt;
	if (node->rt != NULL) {
		node->rt->pt = piv;
	}
	if (node->lt != NULL) {
		node->lt->pt = piv;
	}

	re_height(tree->root);
	avlFix(tree, tree->root);
	re_height(tree->root);
	tree->size--;
	destroyTreeNode(tree, node);
}

void destroyTree(TTree* tree){
	
	TreeNode *aux = minimum(tree->root),
			 *tmp;

	while (aux != NULL) {
		tmp = aux->next;
		destroyTreeNode(tree, aux);
		aux = tmp;
	}

	free(tree);
}


#endif /* TREEMAP_H_ */
