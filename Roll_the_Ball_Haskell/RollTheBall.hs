{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE EmptyDataDecls, MultiParamTypeClasses,
             TypeSynonymInstances, FlexibleInstances,
             InstanceSigs #-}
module RollTheBall where
import Pipes
import ProblemState
import Data.Array as A

{-
    Direcțiile în care se poate mișca o piesa pe tablă
-}

data Directions = North | South | West | East
    deriving (Show, Eq, Ord)

{-
    Sinonim tip de date pentru reprezetarea unei perechi (Int, Int)
    care va reține coordonatele celulelor de pe tabla de joc
-}

type Position = (Int, Int)

{-
    Tip de date pentru reprezentarea celulelor tablei de joc
-}
data Cell = Cell Char
    deriving (Eq, Ord)

instance Show Cell 
    where show (Cell c) = [c]

{-
    Tip de date pentru reprezentarea nivelului curent
-}
data Level = Level (A.Array Position Cell)
    deriving (Eq, Ord)
{-
    *** Optional *** 
  
    Dacă aveți nevoie de o funcționalitate particulară,
    instantiați explicit clasele Eq și Ord pentru Level.
    În cazul acesta, eliminați deriving (Eq, Ord) din Level.
-}

{-
    *** TODO ***

    Instanțiati Level pe Show. 
    Atenție! Fiecare linie este urmată de \n (endl in Pipes).
-}
isLast :: (A.Array (Int, Int) Cell) -> Int -> Int -> Cell
isLast arr i j
    | (snd $ snd $ A.bounds arr) >= j = arr A.! (i, j)  
    | otherwise = Cell endl


addLines :: (A.Array (Int, Int) Cell) -> (A.Array (Int, Int) Cell)
addLines arr = A.array ((0,0), (fst pos, snd pos + 1)) [((x, y), v) | x <- [0..(fst pos)], y <- [0..(snd pos + 1)], v <- [isLast arr x y]]
    where pos = snd $ A.bounds arr

instance Show Level 
    where show (Level arr) = endl : [c | Cell c <- A.elems (addLines arr)]

{-
    *** TODO ***
    Primește coordonatele colțului din dreapta jos a hărții.
    Intoarce un obiect de tip Level în care tabla este populată
    cu EmptySpace. Implicit, colțul din stânga sus este (0,0)
-}

emptyLevel :: Position -> Level
emptyLevel pos = Level (A.array ((0, 0), pos) [((x, y), v) | x <- [0..(fst pos)], y <- [0..(snd pos)], v <- [Cell emptySpace]])

{-
    *** TODO ***

    Adaugă o celulă de tip Pipe în nivelul curent.
    Parametrul char descrie tipul de tile adăugat: 
        verPipe -> pipe vertical
        horPipe -> pipe orizontal
        topLeft, botLeft, topRight, botRight -> pipe de tip colt
        startUp, startDown, startLeft, startRight -> pipe de tip initial
        winUp, winDown, winLeft, winRight -> pipe de tip final
    Parametrul Position reprezintă poziția de pe hartă la care va fi adaugată
    celula, dacă aceasta este liberă (emptySpace).
-}

addCell :: (Char, Position) -> Level -> Level
addCell (cell, (i, j)) (Level arr)
    | i < 0 || i > n || j < 0 || j > m = (Level arr) 
    | otherwise = Level (arr A.// [((i, j), Cell cell)]) 
    where 
        (n, m) = snd $ A.bounds arr

{-
    *** TODO *** 

    Primește coordonatele colțului din dreapta jos al hărții și o listă de 
    perechi de tipul (caracter_celulă, poziția_celulei).
    Întoarce un obiect de tip Level cu toate celeule din listă agăugate pe
    hartă.
    Observatie: Lista primită ca parametru trebuie parcursă de la dreapta 
    la stanga.
-}
 
createLevel :: Position -> [(Char, Position)] -> Level
createLevel pos arr = foldr addCell (emptyLevel pos) arr

{-
    *** TODO ***

    Mișcarea unei celule în una din cele 4 direcții 
    Schimbul se poate face doar dacă celula vecină e goală (emptySpace).
    Celulele de tip start și win sunt imutabile.

    Hint: Dacă nu se poate face mutarea puteți lăsa nivelul neschimbat.
-}
-- North | South | West | East

isEmpty :: Position -> (A.Array (Int, Int) Cell) -> Bool
isEmpty pos arr = (arr A.! pos) == Cell emptySpace

switchCell :: Position -> Position -> (A.Array (Int, Int) Cell) -> (A.Array (Int, Int) Cell)
switchCell pos1 pos2 arr = ((arr A.// [(pos2, (arr A.! pos1))])  A.// [(pos1, (arr A.! pos2))]) 

moveCell :: Position -> Directions -> Level -> Level
moveCell pos d (Level arr)
    | contains (arr A.! pos) win = Level arr
    | contains (arr A.! pos) start = Level arr
    | d == North && fst pos > 0 && isEmpty (fst pos - 1, snd pos) arr = Level (switchCell pos (fst pos - 1, snd pos) arr)
    | d == South && fst pos < fst bord && isEmpty (fst pos + 1, snd pos) arr = Level (switchCell pos (fst pos + 1, snd pos) arr)
    | d == West && snd pos > 0 && isEmpty (fst pos, snd pos - 1) arr = Level (switchCell pos (fst pos, snd pos - 1) arr)
    | d == East && snd pos < snd bord && isEmpty (fst pos, snd pos + 1) arr = Level (switchCell pos (fst pos, snd pos + 1) arr)
    | otherwise = Level arr
    where 
        bord = snd $ A.bounds arr

start :: [Cell]
start = [Cell startUp, Cell startDown, Cell startLeft, Cell startRight]

win :: [Cell]
win = [Cell winUp, Cell winDown, Cell winLeft, Cell winRight]

{-
    *** HELPER ***

    Verifică dacă două celule se pot conecta.
    Atenție: Direcția indică ce vecin este a
    doua celulă pentru prima.

    ex: connection botLeft horPipe East = True (╚═)
        connection horPipe botLeft East = False (═╚)
-}
-- fromWest :: [Cell]
-- fromWest = [Cell horPipe, Cell botLeft, Cell topLeft, Cell startRight, Cell winRight]
-- fromEast :: [Cell]
-- fromEast = [Cell horPipe, Cell botRight, Cell topRight, Cell startLeft, Cell winLeft]
-- fromNorth :: [Cell]
-- fromNorth = [Cell verPipe, Cell topLeft, Cell topRight, Cell startDown, Cell winDown]
-- fromSouth :: [Cell]
-- fromSouth = [Cell verPipe, Cell botLeft, Cell botRight, Cell startUp, Cell startDown]

contains :: Cell -> [Cell] -> Bool
contains e arr = not $ null (filter (== e) arr)

connection :: Cell -> Cell -> Directions -> Bool
connection a b dir
    | dir == North && contains a fromSouth && contains b fromNorth = True
    | dir == South && contains a fromNorth && contains b fromSouth = True
    | dir == West && contains a fromEast && contains b fromWest = True
    | dir == East && contains a fromWest && contains b fromEast = True
    | otherwise = False

fromWest :: [Cell]   
fromWest = [Cell horPipe, Cell botLeft, Cell topLeft, Cell startRight, Cell winRight]

fromEast :: [Cell]
fromEast = [Cell horPipe, Cell botRight, Cell topRight, Cell startLeft, Cell winLeft]

fromNorth :: [Cell]
fromNorth = [Cell verPipe, Cell topLeft, Cell topRight, Cell startDown, Cell winDown]

fromSouth :: [Cell]
fromSouth = [Cell verPipe, Cell botLeft, Cell botRight, Cell startUp, Cell winUp]

    

{-
    *** TODO ***

    Va returna True dacă jocul este câștigat, False dacă nu.
    Va verifica dacă celulele cu Pipe formează o cale continuă de la celula
    de tip inițial la cea de tip final.
    Este folosită în cadrul Interactive.
-}
checkPath :: Cell -> (Int, Int) -> (Int, Int) -> (A.Array (Int, Int) Cell) -> Bool
checkPath cell (i, j) prev arr
    | contains cell win = True
    | j < m && (i, j + 1) /= prev && contains cell fromWest && contains (arr A.! (i, j + 1)) fromEast = checkPath (arr A.! (i, j + 1)) (i, j + 1) (i, j) arr
    | j > 0 && (i, j - 1) /= prev && contains cell fromEast && contains (arr A.! (i, j - 1)) fromWest = checkPath (arr A.! (i, j - 1)) (i, j - 1) (i, j) arr
    | i < n && (i + 1, j) /= prev && contains cell fromNorth && contains (arr A.! (i + 1, j)) fromSouth = checkPath (arr A.! (i + 1, j)) (i + 1, j) (i, j) arr
    | i > 0 && (i - 1, j) /= prev && contains cell fromSouth && contains (arr A.! (i - 1, j)) fromNorth = checkPath (arr A.! (i - 1, j)) (i - 1, j) (i, j) arr
    | otherwise = False
    where 
        (n, m) = snd $ A.bounds arr

getStart :: (A.Array (Int, Int) Cell) -> Cell
getStart arr = arr A.! (head $ filter (\x -> contains (arr A.! x) start) (A.indices arr))

getIdStart :: (A.Array (Int, Int) Cell) -> (Int, Int)
getIdStart arr = head $ filter (\x -> contains (arr A.! x) start) (A.indices arr)

wonLevel :: Level -> Bool
wonLevel (Level arr) = checkPath (getStart arr) (getIdStart arr) (-1, -1) arr

succLvl :: Level -> Directions -> [((Position, Directions), Level)]
succLvl (Level arr) dir = [((pos , dir), lvl) | pos <- (A.indices arr), lvl <- [moveCell pos dir (Level arr)], lvl /= (Level arr)]

instance ProblemState Level (Position, Directions) where
    successors lvl = (succLvl lvl North) ++ (succLvl lvl South) ++ (succLvl lvl West) ++ (succLvl lvl East)

    isGoal lvl = wonLevel lvl
    
    reverseAction (((i, j), dir), lvl)
        | dir == South = (((i + 1, j), North), lvl)
        | dir == North = (((i - 1, j), South), lvl)
        | dir == East = (((i, j + 1), West), lvl)
        | otherwise = (((i, j - 1), East), lvl)

