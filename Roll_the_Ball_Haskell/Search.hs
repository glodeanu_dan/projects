{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Search where
import Data.Char
import ProblemState
-- import Pipes
-- import Data.Array as A
import RollTheBall
{-
    *** TODO ***

    Tipul unei nod utilizat în procesul de căutare. Recomandăm reținerea unor
    informații legate de:

    * stare;
    * acțiunea care a condus la această stare;
    * nodul părinte, prin explorarea căruia a fost obținut nodul curent;
    * adâncime
    * copiii, ce vor desemna stările învecinate
-}

data Node s a = State s (Maybe a) (Maybe (Node s a)) Int [Node s a]
    deriving (Eq)

instance Show (Node s a)
    where show (State _ _ _ d _) = [chr (d + 48)]

{-
    *** TODO ***
    Gettere folosite pentru accesul la câmpurile nodului
-}
nodeState :: Node s a -> s
nodeState (State s _ _ _ _) = s

nodeParent :: Node s a -> Maybe (Node s a)
nodeParent (State _ _ p _ _) = p

nodeDepth :: Node s a -> Int
nodeDepth (State _ _ _ d _) = d

nodeAction :: Node s a -> Maybe a
nodeAction (State _ a _ _ _) = a

nodeChildren :: Node s a -> [Node s a]
nodeChildren (State _ _ _ _ c) = c

{-
    *** TODO ***

    Generarea întregului spațiu al stărilor
    Primește starea inițială și creează nodul corespunzător acestei stări,
    având drept copii nodurile succesorilor stării curente.
-}

putChildren ::(ProblemState s a, Eq s) => s -> a -> Node s a -> Int -> [Node s a]
putChildren s a pp d = [State y (Just x) (Just p) d (putChildren y x p (d + 1)) | (x, y) <- (successors s)]
    where p = State s (Just a) (Just pp) (d - 1) (putChildren s a pp d) 

createStateSpace :: (ProblemState s a, Eq s) => s -> Node s a
createStateSpace s = State s Nothing Nothing 0 [State y (Just x) (Just (createStateSpace s)) 1 (putChildren y x (createStateSpace s) 2) | (x, y) <- (successors s)]
-- createStateSpace s = undefined

{-
    *** TODO ***
   
    Primește un nod inițial și întoarce un flux de perechi formate din:
    * lista nodurilor adăugate în frontieră la pasul curent
    * frontiera

-}

getParents :: Node s a -> [Node s a]
getParents  (State _ _ Nothing _ _) = []
getParents  (State _ _ (Just p) _ _) = [p]


        -- a = filter (\x -> not $ elem x visited) (nodeChildren (head $ snd $ head (auxBfs p)))

auxBfs :: (Ord s, Eq s) => [Node s a] -> [Node s a] -> [s]-> Node s a -> [([Node s a], [Node s a])]
auxBfs _ _ visited (State s a Nothing d c) = (aux, aux) : (foldl (++) [] (map (auxBfs aux aux [s]) c))
    where aux = [(State s a Nothing d c)]
auxBfs first second visited (State _ _ (Just p) _ c)= (a, b ++ a) : (foldl (++) [] (map (auxBfs a (b ++ a) ([x | (State x _ _ _ _) <- c] ++ visited)) c))
    where
        a = filter (\(State x _ _ _ _) -> not $ elem x visited) (nodeChildren (head second))
        b = (tail second)

bfs :: (Eq s, Ord s) => Node s a -> [([Node s a], [Node s a])]
bfs node = auxBfs [] [] [] node 

print1 :: [([Node s a], [Node s a])] -> [([s], [s])]
print1 arr = map (\(x, y) -> (map (\(State a _ _ _ _) -> a) x, map (\(State b _ _ _ _) -> b) y)) arr

{-
    *** TODO ***
  
    Primește starea inițială și finală și întoarce o pereche de noduri, reprezentând
    intersecția dintre cele două frontiere.
-}

-- data Node s a = State s (Maybe a) (Maybe (Node s a)) Int [Node s a]
intersection :: (ProblemState s a, Eq s) => [Node s a] -> [Node s a] -> Bool
intersection arr1 arr2 = or (map (\x -> elem x b) a)
    where
        a = [x | (State x _ _ _ _) <- arr1]
        b = [x | (State x _ _ _ _) <- arr2]

getSol :: (ProblemState s a, Eq s) => ([Node s a], [Node s a]) -> (Node s a, Node s a)
getSol (a, b) = head ([(x, y) | x <- a, y <- b, nodeState x == nodeState y])

bidirBFS :: (ProblemState s a, Eq s, Ord s) => Node s a -> Node s a -> (Node s a, Node s a)
bidirBFS start finish = undefined
-- bidirBFS start finish = head [getSol x | x <- zip a b, intersection (fst x) (snd x)]
--     where
--         a = map fst (bfs start) 
--         b = map fst (bfs finish)


{-
    *** TODO ***

    Pornind de la un nod, reface calea către nodul inițial, urmând legăturile
    către părinți.

    Întoarce o listă de perechi (acțiune, stare), care pornește de la starea inițială
    și se încheie în starea finală.

-}

auxExtract :: Node s a -> [(Maybe a, s)]
auxExtract (State s a Nothing _ _) = [(a, s)]
auxExtract (State s a (Just p) _ _) = (a, s) : extractPath p
extractPath :: Node s a -> [(Maybe a, s)]
extractPath = reverse . auxExtract



{-
    *** TODO ***

    Pornind de la o stare inițială și una finală, se folosește de bidirBFS pentru a găsi
    intersecția dintre cele două frontiere și de extractPath pentru a genera calea.

    Atenție: Pentru calea gasită în a doua parcurgere, trebuie să aveți grijă la a asocia
    corect fiecare stare cu acțiunea care a generat-o.

    Întoarce o listă de perechi (acțiune, stare), care pornește de la starea inițială
    și se încheie în starea finală.
-}

solve :: (ProblemState s a, Ord s)
      => s          -- Starea inițială de la care se pornește
      -> s          -- Starea finală la care se ajunge
      -> [(Maybe a, s)]   -- Lista perechilor
solve f s = (extractPath a) ++ (tail $ reverse $ extractPath b)
    where (a, b) = bidirBFS (createStateSpace f) (createStateSpace s)
