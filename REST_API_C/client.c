#include <stdio.h>      
#include <stdlib.h>      
#include <unistd.h>      
#include <string.h>     
#include <sys/socket.h> 
#include <netinet/in.h>  
#include <netdb.h>       
#include <arpa/inet.h>
#include "helpers.h"
#include "requests.h"
#include "parson.h"

void print_mess(char *mess) {
    int i;
    if (strstr(mess, "\"error")) {
        char *err = strstr(mess, "\"error");
        for (i = 0; i < strlen(err); i++) {
            if (err[i] == '}') {
                printf("\n");
                break;
            }

            printf("%c", err[i]);
        }
    }
    else {
        for (i = 0; i < strlen(mess); i++) {
            printf("%c", mess[i]);
            if (mess[i] == '\n') {
                break;
            }
        }
        if (strstr(mess, "[{")) {
        char *js = strstr(mess, "[{") + 1;
        for (i = 0; i < strlen(js); i++) {
            if (js[i - 1] == ',' && js[i] != '{') {
                printf(" ");
            }

            if (js[i] == ']') {
                printf("\n");
                break;
            }
            else if (js[i] == ',' && js[i + 1] == '{') {
                printf("\n");
            }
            else if (strchr("{\"}", js[i]) == NULL) {
                printf("%c", js[i]);
            }

        }
    }
    }
}

int main(int argc, char *argv[]) {
    char *message;
    char *response;
    char command[20];
    char line[1000];
    char *val, *key, *cookies = NULL, *token, *aux;
    JSON_Value *root_value;
    JSON_Object *root_object;
    char *serialized_string = NULL;
    int sockfd, i, count;
    char **data = malloc (2 * sizeof(char));

    while (1) {
        fgets(command, sizeof(command), stdin);

        if (strcmp(command, "register\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            root_value = json_value_init_object();
            root_object = json_value_get_object(root_value);
            
            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            serialized_string = json_serialize_to_string(root_value);

            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/register", "application/json", serialized_string, NULL, NULL);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);

            print_mess(response);

            json_free_serialized_string(serialized_string);
            json_value_free(root_value);

            root_value = NULL;
            root_object = NULL;
        }
        else if (strcmp(command, "login\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            root_value = json_value_init_object();
            root_object = json_value_get_object(root_value);
            
            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            serialized_string = json_serialize_to_string(root_value);
            // puts(serialized_string);

            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/login", "application/json", serialized_string, NULL, NULL);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);

            json_free_serialized_string(serialized_string);
            json_value_free(root_value);

            cookies = strstr(response, "connect.sid");

            if (cookies != NULL) {
                cookies = strtok(cookies, ";");
            }

            serialized_string = NULL;
            root_value = NULL;
            root_object = NULL;
        }
        else if (strcmp(command, "enter_library\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/access", NULL, cookies, NULL);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);

            response = strstr(response, "token\":\"");

            if (response != NULL) {
                token = response + strlen("token\":\"");
                token = strtok(token, "\"");
            }

        }
        else if (strcmp(command, "get_books\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", NULL, cookies, token);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);   
            print_mess(response);         
        }
        else if (strcmp(command, "get_book\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");

            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", val, cookies, token);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);
        }
        else if (strcmp(command, "add_book\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            root_value = json_value_init_object();
            root_object = json_value_get_object(root_value);
            
            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");
            json_object_set_string(root_object, key, val);

            serialized_string = json_serialize_to_string(root_value);

            count = 0;
            aux = strstr(serialized_string, "page_count\":");
            
            if (aux != NULL) {
                aux += strlen("page_count\":");

                while (count != 2 ) {
                    if (*aux == '\"') {
                        *aux = ' ';
                        count++;
                    }
                    aux++;
                }
            }

            // puts(serialized_string);

            message = compute_post_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", "application/json", serialized_string, cookies, token);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);

            json_free_serialized_string(serialized_string);
            json_value_free(root_value);
            
        }
        else if (strcmp(command, "delete_book\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            fgets(line, sizeof(line), stdin);
            key = strtok(line, "=\n");
            val = strtok(NULL, "=\n");

            message = compute_delete_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/library/books", val, cookies, token);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);
        }
        else if (strcmp(command, "logout\n") == 0) {
            sockfd = open_connection("3.8.116.10", 8080, AF_INET, SOCK_STREAM, 0);

            message = compute_get_request("ec2-3-8-116-10.eu-west-2.compute.amazonaws.com", 
                "/api/v1/tema/auth/logout", NULL, cookies, NULL);
            // puts(message);
            send_to_server(sockfd, message);
            response = receive_from_server(sockfd);
            // puts(response);
            print_mess(response);
            cookies = NULL;
            token = NULL;
        }
        else if (strcmp(command, "exit\n") == 0) {
            close_connection(sockfd);
            break;
        }
        else {
            printf("ERROR: Unknown command\n");
        }
    }

    return 0;
}
