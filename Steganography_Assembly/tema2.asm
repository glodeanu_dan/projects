%include "include/io.inc"

extern atoi
extern printf
extern exit
extern malloc
extern free

; Functions to read/free/print the image.
; The image is passed in argv[1].
extern read_image
extern free_image
; void print_image(int* image, int width, int height);
extern print_image

; Get image's width and height.
; Store them in img_[width, height] variables.
extern get_image_width
extern get_image_height

section .data
    use_str db "Use with ./tema2 <task_num> [opt_arg1] [opt_arg2]", 10, 0
    hint db "revient"
    len dd 7 
    mess db "C'est un proverbe francais.", 0
    mess_len dd 27
    A db ".-", 0
    B db "-...", 0
    C db "-.-.", 0
    D db "-..", 0
    E db ".", 0
    F db "..-.", 0
    G db "--.", 0
    H db "....", 0
    I db "..", 0
    J db ".---", 0
    K db "-.-", 0
    L db ".-..", 0
    M db "--", 0
    N db "-.", 0
    O db "---", 0
    P db ".--.", 0
    Q db "--.-", 0
    R db ".-.", 0
    S db "...", 0
    T db "-", 0
    U db "..-", 0
    V db "...-", 0
    W db ".--", 0
    X db "-..-", 0
    Y db "-.--", 0
    Z db "--..", 0
    ONE db ".----", 0
    TWO db "..---", 0
    THREE db "...--", 0
    FOUR db "....-", 0
    FIVE db ".....", 0
    SIX db "-....", 0
    SEVEN db "--...", 0
    EIGHT db "---..", 0
    NINE db "----.", 0
    ZERO db "-----", 0
    SPACE db "|", 0
    COMMA db "--..--", 0
    
section .bss
    task:       resd 1
    img:        resd 1
    img_width:  resd 1
    img_height: resd 1

section .text
global main

;functie task1
bruteforce_singlebyte_xor:
    push ebp
    mov ebp, esp
    
    ;curat registrii
    xor edx, edx
    xor ecx, ecx
    xor ebx, ebx
    xor edi, edi
    xor esi, esi
    
    ; aflu numarul de elemente
    mov eax, [img_width]
    mul dword [img_height]
    mov ebx, eax
    sub ebx, [len]
    mov esi, dword [ebp + 8]
    
    ; gasesc cheia
    find_key:
        xor ecx, ecx
        test_key:

            xor edi, edi
            
            find_str:
                ; iau un element din matrice
                push edi
                add edi, ecx
                mov eax, [esi + 4 * edi]
                pop edi
                
                ; ii aplic cheia
                xor al, dl
                ; daca elementul difera cu cel din string incerc alta cheie
                cmp al, [hint + edi]
                jne not_mess
            
                ; daca elementul din string si cel din matrice coincid verific urmatorul
                inc edi
                cmp edi, dword [len]
            jl find_str
            
            ; aici se ajunge daca cheia este buna
            ; determin linia pe care e mesajul
            push ecx
            push edx
            xor edx, edx
            mov eax, ecx
            div dword [img_width]
            xor edx, edx
            mul dword [img_width]
            mov ecx, eax
            pop edx
            
            xor edi, edi
            push ecx
            
            ; numar cate caractere are measajul
            count:
                mov eax, [esi + 4 * ecx]
                xor al, dl

                inc ecx
                inc edi
            cmp al, 0
            jne count
            
            ; aloc memorie petru mesaj
            pop ecx
            sub esp, edi
            mov ebx, esp
            
            push edx
            push esi
            push edi
            push ecx
            
            push edi
            call malloc
            add esp, 4
            mov ebx, eax
            
            pop ecx
            pop edi
            pop esi
            pop edx
            
            ; copii mesajul
            copy:
                mov eax, [esi + 4 * ecx]
                xor al, dl
                mov [ebx], al
                
                inc ebx
                inc ecx
            cmp al, 0
            jne copy
            
            ; salvez mesajul
            sub ebx, edi

            ;salvez cheia
            push edx
            xor edx, edx
            ; salvez linia
            mov eax, ecx
            div dword [img_width]    
            pop edx
            add esp, edi
            
            add esp, 4
            ; ma duc la sfarsit
            jmp finish
            not_mess:
         
            ; parcurg matricea
            inc ecx
        cmp ecx, ebx
        jle test_key
        
        ; verific daca mai sunt chei
        inc edx
        cmp edx, 256
    jl find_key
    
    
    finish:
    leave 
    ret
    
; functie task 2
encrypt_xor:
    push ebp
    mov ebp, esp
    
    ; apelez functia de la task 1 pentru a obtine cheia si linia
    mov ecx, dword [ebp + 8]
    push ecx
    call bruteforce_singlebyte_xor
    add esp, 4
    
    ; elibereaz mesajul
    pushad
    push ebx
    call free
    add esp, 4
    popad
    
    mov edi, eax
    
    ; stochez in eax demensiunea matricii
    push edx
    xor edx, edx
    mov eax, [img_width]
    mul dword [img_height]
    dec eax
    pop edx
    inc edi
    mov ecx, dword [ebp + 8]
   
   ; decriptez imagine
    decrypt:
        mov ebx, [ecx + 4 * eax]
        xor bl, dl
        mov [ecx + 4 * eax], ebx
        dec eax
    cmp eax, 0
    jge decrypt
    
    ; ii dau lui eax coordonatele pe care trebuie sa-mi scriu mesajul
    push edx
    xor edx, edx
    mov eax, edi
    mul dword [img_width]
    xor edx, edx
    mov ecx, 4
    mul ecx
    
    add eax, dword [ebp + 8]
    
    mov ecx, [mess_len]
    
    ; imi scriu mesajul pe urmatoarea linie
    write_message:
        xor ebx, ebx
        mov bl, [mess + ecx]
        mov [eax + 4 * ecx], ebx
        dec ecx
    jge write_message
    
    pop edx
    ;determin noua cheie
    mov eax, edx
    xor edx, edx
    mov ebx, 2
    mul ebx
    add eax, 3
    xor edx, edx
    mov ebx, 5
    div ebx
    sub eax, 4
    mov ebx, eax

    ; pun in eax dimensiunea matricii/ vectorului
    mov eax, [img_width]
    mul dword [img_height]
    dec eax
    mov ecx, dword [ebp + 8]
    xor edi, edi
        
    ; aplic cheia pe imagine
    encrypt:
        mov edx, [ecx + 4 * eax]
        xor dl, bl        
        mov [ecx + 4 * eax], edx
        dec eax
    cmp eax, 0
    jge encrypt
    
    
    leave 
    ret
    
; functie task 3
morse_encrypt:
    push ebp
    mov ebp, esp
    
    ; calculez in eax adresa la care voi scri
    mov edi, [ebp + 12]    
    xor edx, edx
    mov eax, [ebp + 16]
    mov ecx, 4
    mul ecx
    add eax, [ebp + 8]
    
    ; pastrez adresa stringului de input
    mov edi, [ebp + 12]
    xor ebx, ebx
    xor ecx, ecx

    ; loop care itereaza prin sirul primit la input
    change:
        ; pastrez charul curent in bl
        mov bl, [edi + ecx]
        
        ; switch in functie de caracterul curent
        cmp bl, 'A'
        jne skip_a
            mov esi, A
        skip_a:
        
        cmp bl, 'B'
        jne skip_b
            mov esi, B
        skip_b:
        
        cmp bl, 'C'
        jne skip_c
            mov esi, C
        skip_c:
        
        cmp bl, 'D'
        jne skip_d
            mov esi, D
        skip_d:
        
        cmp bl, 'E'
        jne skip_e
            mov esi, E
        skip_e:
        
        cmp bl, 'F'
        jne skip_f
            mov esi, F
        skip_f:
        
        cmp bl, 'G'
        jne skip_g
            mov esi, G
        skip_g:
        
        cmp bl, 'H'
        jne skip_h
            mov esi, H
        skip_h:
        
        cmp bl, 'I'
        jne skip_i
            mov esi, I
        skip_i:
        
        cmp bl, 'J'
        jne skip_j
            mov esi, J
        skip_j:
        
        cmp bl, 'K'
        jne skip_k
            mov esi, K
        skip_k:
        
        cmp bl, 'L'
        jne skip_l
            mov esi, L
        skip_l:
        
        cmp bl, 'M'
        jne skip_m
            mov esi, M
        skip_m:
        
        cmp bl, 'N'
        jne skip_n
            mov esi, N
        skip_n:
        
        cmp bl, 'O'
        jne skip_o
            mov esi, O
        skip_o:
        
        cmp bl, 'P'
        jne skip_p
            mov esi, P
        skip_p:
        
        cmp bl, 'Q'
        jne skip_q
            mov esi, Q
        skip_q:
        
        cmp bl, 'R'
        jne skip_r
            mov esi, R
        skip_r:
       
        cmp bl, 'S'
        jne skip_s
            mov esi, S
        skip_s:
        
        cmp bl, 'T'
        jne skip_t
            mov esi, T
        skip_t:
        
        cmp bl, 'U'
        jne skip_u
            mov esi, U
        skip_u:
        
        cmp bl, 'V'
        jne skip_v
            mov esi, V
        skip_v:
        
        cmp bl, 'W'
        jne skip_w
            mov esi, W
        skip_w:
        
        cmp bl, 'X'
        jne skip_x
            mov esi, X
        skip_x:
        
        cmp bl, 'Y'
        jne skip_y
            mov esi, Y
        skip_y:
        
        cmp bl, 'Z'
        jne skip_z
            mov esi, Z
        skip_z:
        
        cmp bl, '1'
        jne skip_1
            mov esi, ONE
        skip_1:
        
        cmp bl, '2'
        jne skip_2
            mov esi, TWO
        skip_2:
        
        cmp bl, '3'
        jne skip_3
            mov esi, THREE
        skip_3:
        
        cmp bl, '4'
        jne skip_4
            mov esi, FOUR
        skip_4:
        
        cmp bl, '5'
        jne skip_5
            mov esi, FIVE
        skip_5:
        
        cmp bl, '6'
        jne skip_6
            mov esi, SIX
        skip_6:
        
        cmp bl, '7'
        jne skip_7
            mov esi, SEVEN
        skip_7:
        
        cmp bl, '8'
        jne skip_8
            mov esi, EIGHT
        skip_8:
        
        cmp bl, '9'
        jne skip_9
            mov esi, NINE
        skip_9:
        
        cmp bl, '0'
        jne skip_zero
            mov esi, ZERO
        skip_zero:
        
        cmp bl, ' '
        jne skip_space
            mov esi, SPACE
        skip_space:
        
        cmp bl, ','
        jne skip_comma
            mov esi, COMMA
        skip_comma:
              
        ; daca nu e prima scriere adaug intai spatiu intre litere  
        cmp ecx, 0
        je first_iter
            xor edx, edx
            mov dl, ' '
            mov [eax], edx
            add eax, 4
        first_iter:
        
        ; scriu intreg sirul asociat caracterului
        mov bl, [esi]
        write_morse:
            mov [eax], ebx
            add eax, 4
            inc esi
            mov bl, [esi]
        cmp bl, 0
        jne write_morse
                
        inc ecx
    cmp BYTE [edi + ecx], 0
    jne change
    
    mov [eax], dword 0
    
    leave
    ret
    
; functie task 4
lsb_encode:
    push ebp
    mov ebp, esp
    
    ; determin offstul pentru a accesa un elementul primit ca parametru
    mov eax, [ebp + 16]
    dec eax
    xor edx, edx
    mov ebx, 4
    mul ebx
    add eax, [ebp + 8]
    mov edi, eax
    
    mov eax, [ebp + 12]
    
    ; loop ce continua pana ajunge la \0
    encode:
        mov bl, 7
        
        ; se scrie un byte, bit cu bit
        write_bit:
            mov dl, [eax]
            mov cl, 1
            
            push ebx
            jmp verify_iter
            ; se shifteaza bitul din mesaj curent pe prima pozitie
            shift_byte:
            
                shr dl, 1                
                
                dec bl
            
            verify_iter:
            cmp bl, 0
            jg shift_byte
            pop ebx
            
            ; se seteaza bitul din imagine
            and cl, dl
            cmp cl, 0 
            je reset_bit
                or [edi], cl
                jmp seted_bit
            reset_bit:
                mov cl, 254
                and [edi], cl
            seted_bit:

            add edi, 4 
            
            dec bl
        cmp bl, 0
        jge write_bit
        
        inc eax
    cmp dl, 0
    jne encode

    leave 
    ret
    
; functie task 5
lsb_decode:
    push ebp
    mov ebp, esp
    
    ; determin offsetul pentru pozitia elementului
    xor edx, edx
    mov eax, [ebp + 12]
    dec eax
    mov ebx, 4
    mul ebx
    
    add eax, dword [ebp + 8]
    mov edi, eax
    
    ; aloc un string de doi octeti al doile fiind \0
    sub esp, 2
    mov byte [esp + 1], 0
    
    ; continua pana nu am citit \0
    write_mess:
        xor edx, edx
        xor ecx, ecx
        mov dh, 8
        
        ; citesc cel mai putin semnificativ bit din imagine si il pun in dl
        read_bit:
            mov cl, 1
            mov eax, [edi]
            and cl, al
                        
            push edx
            mov dl, 8
            sub dl, dh
            mov dh, dl
            ; shiftez bitul pe pozitia potrivita
            jmp verify
            shift:
            
                shl cl, 1
                inc dh
            
            verify:
            cmp dh, 8
            jl shift
            
            ; primul bit e 2^0
            shr cl, 1
            
            pop edx

            ; adaug bitul in byte
            or dl, cl
            
            add edi, 4
            dec dh
        cmp dh, 0
        jg read_bit
        
        ; printez byte-ul rezultat
        mov [esp], dl
        PRINT_STRING [esp]
        
    cmp dl, 0
    jne write_mess
    
    add esp, 2

    leave 
    ret
    
; functe task 6
blur:
    push ebp
    mov ebp, esp

    ; incep de la a doua linie
    mov esi, 1
        
    ; iterez prin linii pana cand ajung la penultima inclusiv
    next_line:
        ; incep cu al doilea element din linie
        mov edi, 1
        
        ; iterez pana la ultimul inclusiv
        next_colon:
            ; determin pozitia curenta
            xor edx, edx
            mov eax, edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            mov eax, esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            push dword [eax]
            
            ; determin elementul de pe linia de sus
            xor edx, edx
            mov eax, edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            dec esi
            mov eax, esi
            inc esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            push dword [eax]
            
            ; determin elementul de pe linia de jos
            xor edx, edx
            mov eax, edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            inc esi
            mov eax, esi
            dec esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            push dword [eax]
            
            ; determin elementul din stanga
            xor edx, edx
            dec edi
            mov eax, edi
            inc edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            mov eax, esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            push dword [eax]
            
            ; determin elementul din dreapta
            xor edx, edx
            inc edi
            mov eax, edi
            dec edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            mov eax, esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            push dword [eax]
            
            ; aflu media aritmetica
            pop eax
            pop ebx
            add eax, ebx
            pop ebx
            add eax, ebx
            pop ebx
            add eax, ebx
            pop ebx
            add eax, ebx
            
            xor edx, edx
            mov ebx, 5
            div ebx
            ; salvez media elementului curent
            push eax
            
            inc edi
            mov eax, [img_width]
            dec eax
        cmp edi, eax
        jl next_colon
    
        inc esi
        mov eax, [img_height]
        dec eax
    cmp esi, eax
    jl next_line

    ; incep de la coada, deoarece rezultatele sunt pe sitiva #LIFO
    mov esi, [img_height]
    sub esi, 2
    ; iterez pana ajung la a doua linie inclusiv
    write_next_line:
        ; incep de la penultimul element
        mov edi, [img_width]
        sub edi, 2
        
        ; iterez pana la al doiea element inclusiv
        write_next_colon:
            ; determin pozitia curenta
            xor edx, edx
            mov eax, edi
            mov ecx, 4
            mul ecx
            mov ecx, eax
            
            xor edx, edx
            mov eax, esi
            mul dword [img_width]
            xor edx, edx
            mov ebx, 4
            mul ebx
            
            add eax, ecx
            add eax, [ebp + 8]
            
            pop ebx
            ; inlocuiesc elemntul cu media sa
            mov [eax], ebx
            
            dec edi
        cmp edi, 0
        jg write_next_colon
    
        dec esi
    cmp esi, 0
    jg write_next_line
    
    leave 
    ret

main:
    mov ebp, esp; for correct debugging
    ; Prologue
    ; Do not modify!
    push ebp
    mov ebp, esp

    mov eax, [ebp + 8]
    cmp eax, 1
    jne not_zero_param

    push use_str
    call printf
    add esp, 4

    push -1
    call exit

not_zero_param:
    ; We read the image. You can thank us later! :)
    ; You have it stored at img variable's address.
    mov eax, [ebp + 12]
    push DWORD[eax + 4]
    call read_image
    add esp, 4
    mov [img], eax

    ; We saved the image's dimensions in the variables below.
    call get_image_width
    mov [img_width], eax

    call get_image_height
    mov [img_height], eax

    ; Let's get the task number. It will be stored at task variable's address.
    mov eax, [ebp + 12]
    push DWORD[eax + 8]
    call atoi
    add esp, 4
    mov [task], eax


    ; There you go! Have fun! :D
    mov eax, [task]
    cmp eax, 1
    je solve_task1
    cmp eax, 2
    je solve_task2
    cmp eax, 3
    je solve_task3
    cmp eax, 4
    je solve_task4
    cmp eax, 5
    je solve_task5
    cmp eax, 6
    je solve_task6
    jmp done

solve_task1:
    ; apelez functia si printez rezultatul
    push DWORD [img]
    call bruteforce_singlebyte_xor
    add esp, 4
    PRINT_STRING [ebx]
    NEWLINE
    
    ;dezaloc mesajul
    pushad
    push ebx
    call free
    add esp, 4
    popad
    
    PRINT_DEC 4, edx
    NEWLINE
    PRINT_DEC 4, eax
    NEWLINE
    jmp done
solve_task2:
    ; apelez functia si printez imaginea modificata
    push DWORD [img]
    call encrypt_xor
    add esp, 4
    
    push dword [img_height]
    push dword [img_width]
    push dword [img]
    call print_image
    add esp, 12
    
    jmp done
solve_task3:
    ; accesez argv[3] si argv [4]
    mov eax, [ebp + 12]
    push DWORD[eax + 16]
    call atoi
    add esp, 4
    
    mov ebx, [ebp + 12]
    mov ebx, [ebx + 12]

    ; apelez functia
    push eax
    push ebx
    push dword [img]
    call morse_encrypt
    add esp, 4
    
    ; printez imaginea
    push dword [img_height]
    push dword [img_width]
    push dword [img]
    call print_image
    add esp, 12
    
    jmp done
solve_task4:
    ; accesez argv[3] si argv [4]
    mov eax, [ebp + 12]
    push DWORD[eax + 16]
    call atoi
    add esp, 4
    
    mov ebx, [ebp + 12]
    mov ebx, [ebx + 12]

    ; apelez functia
    push eax
    push ebx
    push dword [img]
    call lsb_encode
    add esp, 4
    
    ; printez imaginea
    push dword [img_height]
    push dword [img_width]
    push dword [img]
    call print_image
    add esp, 12
    
    jmp done
solve_task5:
    ; accesez argv[3]
    mov eax, [ebp + 12]
    push dword[eax + 12]
    mov eax, [eax + 12]
    call atoi
    add esp, 4
    
    ; apelez functia
    push eax
    push dword [img]
    call lsb_decode
    add esp, 8
    
    jmp done
solve_task6:
    ; apelez functia
    push dword [img]
    call blur
    add esp, 4
    
    ; printez imaginea modificata
    push dword [img_height]
    push dword [img_width]
    push dword [img]
    call print_image
    add esp, 12
    
    jmp done

    ; Free the memory allocated for the image.
done:
    push DWORD[img]
    call free_image
    add esp, 4

    ; Epilogue
    ; Do not modify!
    xor eax, eax
    leave
    ret
    
