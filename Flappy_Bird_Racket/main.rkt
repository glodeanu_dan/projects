#lang racket/gui
;Ignorați următoarele linii de cod. Conțin import-uri și export-uri necesare checker-ului.

(require 2htdp/image)
(require 2htdp/universe)
(require lang/posn)
(require racket/trace)

(require "abilities.rkt")
(require "constants.rkt")
(require "random.rkt")
(require racket/trace)

;---------------------------------------checker_exports------------------------------------------------
(provide next-state)
(provide next-state-bird)
(provide next-state-bird-onspace)
(provide change)

(provide get-pipes)
(provide get-pipe-x)
(provide next-state-pipes)
(provide add-more-pipes)
(provide clean-pipes)
(provide move-pipes)

(provide invalid-state?)
(provide check-ground-collision)
(provide check-pipe-collisions)

(provide draw-frame)

(provide get-initial-state)
(provide get-bird)
(provide get-bird-y)
(provide get-bird-v-y)

; pipe
(provide get-pipes)
(provide get-pipe-x)

; score25
(provide get-score)

(provide get-abilities)
(provide get-abilities-visible)
(provide get-abilities-active)
; variables
(provide get-variables)
(provide get-variables-gravity)
(provide get-variables-momentum)
(provide get-variables-scroll-speed)


;---------------------------------------checker_exports------------------------------------------------
;Initial state
; Primul pas pe care trebuie sa il facem este sa cream starea initiala a jocului.
; Aceasta va fi salvata in (get-initial-state), si trebuie sa incapsuleze toate informatiile
; necesare jocului, si anume: informatii despre pasare, despre pipes si despre powerups.
; Recomandam ca in pasare, sa retineti, printre altele, informatii despre y-ul curent
; si viteza pe y
; Pe parcursul temei, in state, salvati coordonatele colturilor din stanga sus ale obiectelor.
; Aceasta va face mai usoara atat logica miscarii obiectelor, cat si testarea cerintelor.
; Toate coordonatele oferite in comentarii sau in fisierul constants.rkt, se refera la
; coltul din stanga sus ale obiectelor!
;Inițial state
; Primul pas pe care trebuie să îl facem este să creăm starea inițială a jocului.
; Aceasta va fi salvată în (get-initial-state), și trebuie să incapsuleze toate informațiile
; necesare jocului, și anume: informații despre pasăre, despre pipes și, pentru bonus,
; despre powerups și despre variabilele de mediu.
; Recomandăm ca în pasăre, să rețineți, printre altele, informații despre y-ul curent
; și viteză pe y.
; Pe parcursul temei, în state, salvați coordonatele colțurilor din stânga sus ale obiectelor.
; Aceasta va face mai ușoară atât logică mișcării obiectelor, cât și testarea cerințelor.
; Toate coordonatele oferite în comentarii sau în fișierul variables.rkt se referă la
; colțul din stânga sus ale obiectelor!

;TODO 1
; După ce definiți structurile lui (get-initial-state) și a păsării, introduceți în prima
; pe cea din urmă. Colțul din stânga sus a păsării se va află inițial la:
;    y = bird-inițial-y
;    și x = bird-x.
; (get-initial-state) va fi o funcție care va returna starea inițială a jocului.
(struct pipe-struct (x y-gap) #:transparent)
(struct game (score bird pipes var abilities) #:transparent)
(struct bird-struct (x y v-y) #:transparent)
(struct var (gravity momentum scroll-speed) #:transparent)
(struct abilities-struct (active visible) #:transparent)
 
;TODO 8
; În starea jocului, trebuie să păstrăm informații despre pipes. Pe parcursul jocului,
; pipe-urile se vor schimba, unele vor fi șterse și vor fi adăugate altele.
; După ce definiți structura pentru pipe și pentru mulțimea de pipes din stare,
; adăugați primul pipe în starea jocului. Acesta se va află inițial în afară ecranului.
; Celelalte pipe-uri vor fi adăugate ulterior, poziționându-le după acest prim pipe.
; Atenție! Fiecare pipe este format din 2 părți, cea superioară și cea inferioară,
; acestea fiind despărțite de un gap de înălțime pipe-self-gap.
; Colțul din stânga sus al gap-ului dintre componentele primului pipe se va afla inițial la:
;    y = (+ added-number (random random-threshold)), pentru a da un element de noroc jocului,
; și x = scene-width,
; pentru a-l forța să nu fie inițial pe ecran.
; Atenție! Recomandăm să păstrați în stare colțul din stânga sus al chenarului lipsa
; dintre cele 2 pipe-uri!

;TODO 16
; Vrem o modalitate de a păstra scorul jocului. După ce definiți structura
; acestuia, adăugați scorul inițial, adică 0, în starea inițială a jocului.
; Atenție get-initial-state trebuie sa fie o funcție
; și trebuie apelată în restul codului.
(define (get-initial-state)
  (game 
   0
   (bird-struct bird-x bird-initial-y 0)
   (list (pipe-struct scene-width (+ added-number (random random-threshold))))
   (var initial-gravity initial-momentum initial-scroll-speed)
   (abilities-struct null (next-abilities-visible null initial-scroll-speed (bird-struct bird-x bird-initial-y 0)))))

;TODO 2
; După aceasta, implementați un getter care extrage din structura voastră
; pasărea, și un al doilea getter care extrage din structura pasăre
; y-ul curent pe care se află această.
(define (get-bird state)
  (game-bird state))
(define (get-bird-y bird)
  (bird-struct-y bird))
(define (get-bird-x bird)
  (bird-struct-x bird))
;TODO 3
; Trebuie să implementăm logică gravitației. next-state-bird va primi drept
; parametri o structură de tip pasăre, și gravitația(un număr real). Aceasta va adaugă
; vitezei pe y a păsării, gravitația.
(define (next-state-bird bird gravity)
  (struct-copy bird-struct bird [v-y (+ gravity (bird-struct-v-y bird))]
               [y (+ (bird-struct-y bird) (get-bird-v-y bird))]))
              
;TODO 4
; După aceasta, implementati un getter care extrage din structura voastră
; viteza pe y a păsării.
(define (get-bird-v-y bird)
  (bird-struct-v-y bird))

;TODO 6
; Dorim să existe un mod prin care să imprimăm păsării un impuls.
; Definiți funcția next-state-bird-onspace care va primi drept parametri
; o structură de tip pasăre, momentum(un număr real), și va schimbă viteza
; pe y a păsării cu acea valoare.
(define (next-state-bird-onspace bird momentum)
  (struct-copy bird-struct bird [v-y (- 0 momentum)]))
 
; Change
; Change va fi responsabil de input-ul de la tastatură al jocului.
;TODO 7
; Acesta va primi drept parametri o structură de tip stare, și tasta pe
; care am apăsat-o. Aceasta va imprimă păsării momentum-ul, apelând
; funcția next-state-bird-onspace. Pentru orice altă tasta, starea rămâne aceeași.
(define (change current-state pressed-key)
  (if (key=? pressed-key " ")
      (struct-copy game current-state
                   [bird (next-state-bird-onspace (get-bird current-state) initial-momentum)])
      current-state))

;TODO 9
; După ce ați definit structurile pentru mulțimea de pipes și pentru un singur pipe,
; implementați getterul get-pipes, care va extrage din starea jocului mulțimea de pipes,
; sub formă de lista.
(define (get-pipes state)
  (game-pipes state))

;TODO 10
; Implementați get-pipe-x ce va extrage dintr-o singură structura de tip pipe, x-ul acesteia.
(define (get-pipe-x pipe)
  (pipe-struct-x pipe))

(define (get-pipe-y pipe)
  (pipe-struct-y-gap pipe))

;TODO 11
; Trebuie să implementăm logica prin care se mișcă pipes.
; Funcția move-pipes va primi drept parametri mulțimea pipe-urilor din stare
; și scroll-speed(un număr real). Aceasta va adaugă x-ului fiecărui pipe
; scroll-speed-ul dat.
(define (make-step pipe speed)
  (struct-copy pipe-struct pipe [x (- (get-pipe-x pipe) speed)]))

(define (move-pipes pipes scroll-speed)
  (map (lambda (x) (make-step x scroll-speed)) pipes))

;TODO 12
; Vom implementa logica prin care pipe-urile vor fi șterse din stare. În momentul
; în care colțul din DREAPTA sus al unui pipe nu se mai află pe ecran, acesta trebuie
; șters.
; Funcția va primi drept parametru mulțimea pipe-urilor din stare.
;
; Hint: cunoaștem lățimea unui pipe, pipe-width
(define (clean-pipes pipes)
  (filter (lambda (x)
            (positive? (+ pipe-width (get-pipe-x x))))
          pipes))


;TODO 13
; Vrem să avem un stream continuu de pipe-uri.
; Implementati funcția add-more-pipes, care va primi drept parametru mulțimea pipe-urilor
; din stare și, dacă avem mai puțin de no-pipes pipe-uri, mai adăugăm una la mulțime,
; având x-ul egal cu pipe-width + pipe-gap + x-ul celui mai îndepărtat pipe, în raport
; cu pasărea.
(define (add-one-pipe last)
  (pipe-struct (+ (get-pipe-x last) pipe-width pipe-gap)
               (+ added-number (random random-threshold))))

(define (add-more-pipes pipes)
  (if (< (length pipes) no-pipes)
      (let ((rev-pipes (reverse pipes)))
        (reverse (cons (add-one-pipe (car rev-pipes)) rev-pipes)))
      pipes))

;TODO 14
; Vrem ca toate funcțiile implementate anterior legate de pipes să fie apelate
; de către next-state-pipes.
; Aceasta va primi drept parametri mulțimea pipe-urilor și scroll-speed-ul,
; și va apela cele trei funcții implementate anterior, în această ordine:
; move-pipes, urmat de clean-pipes, urmat de add-more pipes.
(define (next-state-pipes pipes scroll-speed)
  (let ((moved-pipes (move-pipes pipes scroll-speed)))
    (let ((cleaned-pipes (clean-pipes moved-pipes)))
      (add-more-pipes cleaned-pipes))))

;TODO 17
; Creați un getter ce va extrage scorul din starea jocului.
(define (get-score state)
  (game-score state))

;TODO 19
; Vrem să creăm logica coliziunii cu pământul.
; Implementati check-ground-collision, care va primi drept parametru
; o structura de tip pasăre, și returnează true dacă aceasta are coliziune
; cu pământul.
;
; Hint: știm înălțimea păsării, bird-height, și y-ul pământului, ground-y.
; Coliziunea ar presupune ca un colț inferior al păsării să aibă y-ul
; mai mare sau egal cu cel al pământului.
(define (check-ground-collision bird)
  (if (>= (+ (get-bird-y bird) bird-height) ground-y)
      #t
      #f))

; invalid-state?
; invalid-state? îi va spune lui big-bang dacă starea curentă mai este valida,
; sau nu. Aceasta va fi validă atât timp cât nu avem coliziuni cu pământul
; sau cu pipes.
; Aceasta va primi ca parametru starea jocului.

;TODO 20
; Vrem să integrăm verificarea coliziunii cu pământul în invalid-state?.

;TODO 22
; Odată creată logică coliziunilor dintre pasăre și pipes, vrem să integrăm
; funcția nou implementată în invalid-state?.
(define (invalid-state? state)
  (or (check-ground-collision (get-bird state))
      (check-pipe-collisions (get-bird state) (get-pipes state)))
  )
      

;TODO 21
; Odată ce am creat pasărea, pipe-urile, scor-ul și coliziunea cu pământul,
; următorul pas este verificarea coliziunii dintre pasăre și pipes.
; Implementati funcția check-pipe-collisions care va primi drept parametri
; o structură de tip pasăre, mulțimea de pipes din stare, și va returna
; true dacă există coliziuni, și false în caz contrar. Reiterând,
; fiecare pipe este format din 2 părți, cea superioară și cea inferioară,
; acestea fiind despărțite de un gap de înălțime pipe-self-gap. Pot există
; coliziuni doar între pasăre și cele două părți. Dacă pasărea se află în
; chenarul lipsă, nu există coliziune.
;
; Hint: Vă puteți folosi de check-collision-rectangle, care va primi drept parametri
; colțul din stânga sus și cel din dreapta jos ale celor două dreptunghiuri
; pe care vrem să verificăm coliziunea.
(define (check-pipe-collisions bird pipes)
  (or (one-pipe-colision bird (car pipes))
       (if (null? (cdr pipes))
           #f
           (one-pipe-colision bird (cadr pipes)))))

;(struct posn (x y))

(define (one-pipe-colision bird pipe)
  (let* ((bird-x (get-bird-x bird))
         (bird-y (get-bird-y bird))
         (first-x (get-pipe-x pipe))
         (first-y (get-pipe-y pipe))
         (bird-upper (make-posn bird-x bird-y))
         (bird-lower (make-posn (+ bird-x bird-width) (+ bird-y bird-height)))
         (first-upper (make-posn first-x (- first-y pipe-height)))
         (first-lower (make-posn (+ first-x pipe-width) first-y))
         (second-upper (make-posn first-x (+ first-y pipe-self-gap)))
         (second-lower (make-posn (+ first-x pipe-width) (+ first-y pipe-self-gap pipe-height))))
    (or
     (check-collision-rectangles bird-upper bird-lower first-upper first-lower)
     (check-collision-rectangles bird-upper bird-lower second-upper second-lower))))
                 
(define (check-collision-rectangles A1 A2 B1 B2)
  (match-let ([(posn AX1 AY1) A1]
              [(posn AX2 AY2) A2]
              [(posn BX1 BY1) B1]
              [(posn BX2 BY2) B2])
    (and (< AX1 BX2) (> AX2 BX1) (< AY1 BY2) (> AY2 BY1))))

;Next-state
; Next-state va fi apelat de big-bang la fiecare cadru, pentru a crea efectul de
; animație. Acesta va primi ca parametru o structură de tip stare, și va întoarce
; starea corespunzătoare următorului cadru.

;TODO 5
; Trebuie să integrăm funcția implementată anterior, și anume next-state-bird,
; în next-state.

;TODO 15
; Vrem să implementăm logică legată de mișcarea, ștergerea și adăugarea pipe-urilor
; în next-state. Acesta va apela next-state-pipes pe pipe-urile din starea curentă.

;TODO 18
; Vrem ca next-state să incrementeze scorul cu 0.1 la fiecare cadru.
(define (next-state state)
  (match-let ([(game score bird pipes variable abilities) state])
    (struct-copy game state [score (+ 0.1 score)]
                 [bird (next-state-bird bird initial-gravity)]
                 [pipes (next-state-pipes pipes (get-variables-scroll-speed variable))]
                 [var (next-variables (var initial-gravity initial-momentum initial-scroll-speed) abilities)]
                 [abilities (next-abilities abilities bird (get-variables-scroll-speed variable))])))

; draw-frame
; draw-frame va fi apelat de big-bang dupa fiecare apel la next-state, pentru a afisa cadrul curent.
;TODO 23
; Fiecare cadru va fi desenat in urmatorul mod:
; bird peste ground, peste scor, peste pipes, peste empty-scene.
;
; Hint: score-to-image primeste un numar real si intoarce scor-ul sub forma de imagine;
; Scor-ul îl puteți plasa direct la coordonatele date, fără a mai face translatiile menționate mai jos.
; Noi tinem minte coltul din stanga sus al imaginii, insa, la suprapunerea unei imagini A peste o alta imagine,
; coordonatele unde plasam imaginea A reprezinta centrul acesteia. Trebuie facute translatiile de la coltul din stanga
; sus la centrul imaginilor.
; Variabile folosite in aceasta functie:
; bird -> bird-width si bird-height
; ground -> ground-y si ground-height, acesta va acoperi intreaga latime a ecranului
; scor -> text-x si text-y
; pipes -> pipe-width si pipe-height
(define bird (rectangle bird-width bird-height  "solid" "yellow"))
(define ground (rectangle scene-width ground-height "solid" "brown"))
(define initial-scene (rectangle scene-width scene-height "solid" "white"))
(define pipe (rectangle pipe-width pipe-height  "solid" "green"))


(define text-family (list "Gill Sans" 'swiss 'normal 'bold #f))
(define (score-to-image x)
(if SHOW_SCORE
	(apply text/font (~v (round x)) 24 "indigo" text-family)
	empty-image))

(define (trans-x coord dim)
  (+ coord (quotient dim 2)))

(define (trans-y coord dim)
  (+ coord (quotient dim 2)))

(define (draw-frame state)
  (match-let ([(game score bird- pipes _ abilities) state])
    
    
    (let* ((pipe-img-aux (foldl (lambda (x acc) (place-image pipe (trans-x (get-pipe-x x) pipe-width)
                                                             (trans-y (- (get-pipe-y x) pipe-height) pipe-height) acc))
                            initial-scene pipes))
           (pipe-img (foldl (lambda (x acc) (place-image pipe (trans-x (get-pipe-x x) pipe-width)
                                                         (trans-y (+ (get-pipe-y x) pipe-self-gap) pipe-height) acc))
                            pipe-img-aux pipes))
           (score-img (place-image (score-to-image score) (posn-x text-posn) (posn-y text-posn) pipe-img))
           (ground-img (place-image ground (trans-x 0 scene-width) (trans-y ground-y ground-height) score-img))
           (visible-img (place-visible-abilities (get-abilities-visible abilities) ground-img))
           (active-img (place-active-abilities (get-abilities-active abilities) visible-img))
           (bird-img (place-image bird (trans-x (get-bird-x bird-) bird-width) (trans-y (get-bird-y bird-) bird-height) active-img)))
      bird-img)))

; Folosind `place-image/place-images` va poziționa pipe-urile pe scenă.
(define (place-pipes pipes scene)
	'(ceva))


; Bonus
; Completați abilities.txt mai întâi, aceste funcții căt, apoi legați
; această funcționalitate la jocul inițial.
;(struct ability-struct (image time pos next))

; Abilitatea care va accelera timpul va dura 10 de secunde, va avea imaginea (hourglass "tomato")
; va avea inițial poziția null si va modifica scrolls-speed dupa formulă
; scroll-speed = scroll-speed + 1
(define fast-ability (ability-struct (hourglass "mediumseagreen") 10 null
  (lambda (x) (struct-copy var x [scroll-speed (if (< 5 (sub1 (get-variables-scroll-speed x)))
                                                   (sub1 (get-variables-scroll-speed x))
                                                   5)]))))

; Abilitatea care va încetini timpul va dura 30 de secunde, va avea imaginea (hourglass "mediumseagreen")
; va avea inițial poziția null si va modifica scrolls-speed dupa formulă
; scroll-speed = max(5, scroll-speed - 1)
(define slow-ability (ability-struct (hourglass "tomato") 30 null
  (lambda (x) (struct-copy var x [scroll-speed (add1 (get-variables-scroll-speed x))]))))
                                                      

; lista cu toate abilităţile posibile în joc
(define ABILITIES (list slow-ability fast-ability))


(define (get-variables state)
  (game-var state))
(define (get-variables-gravity var)
  (var-gravity var))
(define (get-variables-momentum var)
  (var-momentum var))
(define (get-variables-scroll-speed var)
  (var-scroll-speed var))

; Întoarce abilităţile din stare, cu o reprezentare
; intermediară care trebuie să conțină două liste:
;  - lista abilităţilor vizibile (încarcate în scenă dar nu neaparat vizibile pe ecran).
;  - lista abilităţilor activate (cu care pasărea a avut o coloziune).
(define (get-abilities state)
  (game-abilities state))

; Întoarce abilităţile vizibile din reprezentarea intermediară.
(define (get-abilities-visible abilities)
  (abilities-struct-visible abilities))

; Întoarce abilităţile active din reprezentarea intermediară.
(define (get-abilities-active abilities)
  (abilities-struct-active abilities))

;(trace next-state)

; Șterge din reprezentarea abilităţilor vizibile pe cele care nu mai sunt vizibile.
; echivalent cu clean-pipes.
(define (clean-abilities abilities bird)
	(filter (lambda (x)
            (and (positive? (+ (posn-x (get-ability-pos x)) (image-width (get-ability-image x))))
                 (not (check-ability-collision x bird))))
          abilities))


; Muta abilităţile vizibile spre stanga.
; echivalent cu move-pipes.
(define (make-step-ability ability scroll-speed)
  (struct-copy ability-struct ability
                  [pos (make-posn (- (posn-x (get-ability-pos ability)) scroll-speed) (posn-y (get-ability-pos ability)))]))
                                 

(define (move-abilities abilities scroll-speed)
	(map (lambda (x) (make-step-ability x scroll-speed)) abilities))


; Scurge timpul pentru abilităţile activate și le sterge pe cele care au expirat.
; Puteți să va folosiți de variabila globală fps.

(define (time-counter abilities)
  (if (null? abilities)
      abilities
      (let ((copy (map (lambda (x)
                         (struct-copy ability-struct x [time (- (get-ability-time x) (/ 1. fps))]))
                       abilities)))
        (filter (lambda (x)
                  (positive? (get-ability-time x)))
                copy))))
          

; Generează următoarele abilitați vizibile.
; *Atentie* La orice moment pe scena trebuie să fie exact DISPLAYED_ABILITIES
; abilităţi vizibile
; Folosiți funcția fill-abilities din abilities.rkt cât si cele scrise mai sus:
; move-abilities, clean-abilities, time-counter, etc..
(define (next-abilities-visible visible scroll-speed bird)
	(let*
            ((moved (move-abilities visible scroll-speed))
             (cleaned (clean-abilities moved bird))
             (final (fill-abilities cleaned (- DISPLAYED_ABILITIES (length cleaned)) ABILITIES)))
          final))
             

; Generează structura intermediară cu abilități.
; Observați ca nu există next-abilities-active aceastea sunt acele abilităti
; întoarse next-abilities-visible care au o coliziune cu pasărea.
; Puteti folosi `filer`/`filter-not` ca sa verificați ce abilităti au și abilitați
; nu au coliziuni cu pasărea sau puteti folosi `partition`
(define (check-ability-collision x bird)
  (let ((bird-x (get-bird-x bird)) (bird-y (get-bird-y bird)))
    (check-collision-rectangles
                             (make-posn bird-x bird-y)
                             (make-posn (+ bird-x bird-width)(+ bird-y bird-height))
                             (get-ability-pos x)
                             (make-posn (trans-x (posn-x (get-ability-pos x)) (image-width (get-ability-image x)))
                                   (trans-y (posn-y (get-ability-pos x)) (image-height (get-ability-image x)))))))

(define (add-active-ability visible active bird)
  (let* ((bird-x (get-bird-x bird)) (bird-y (get-bird-y bird))
         (colision (filter (lambda (x)
                            (check-ability-collision x bird))
                           visible)))
    (if (null? colision)
        active
        (reverse (append colision (reverse active))))))

(define (next-abilities-active visible active bird)
  (add-active-ability visible (time-counter active) bird))
  

(define (next-abilities abilities bird scroll-speed)
  (match-let ([(abilities-struct active visible) abilities])
	(struct-copy abilities-struct abilities
                     [active (next-abilities-active visible active bird)] 
                     [visible (next-abilities-visible visible scroll-speed bird)])))
                     

; Dând-use variabilele actuale și abilitațile calculați care vor
; variabile finale folosite în joc
; Folositi compose-abilities
; Atenție când apelați `next-variables` în next-state dați ca paremetru
; initial-variables și nu variabilele aflate deja în stare
; In felul acesta atunci când
(define (next-variables variables abilities)
  (struct-copy var variables [scroll-speed (get-variables-scroll-speed ((compose-abilities (get-abilities-active abilities)) variables))]))


; Folosind `place-image/place-images` va poziționa abilităţile vizibile la ability pos.
(define (place-visible-abilities abilities scene)
	(foldr (lambda (x acc) (place-image
                                (get-ability-image x)
                                (posn-x (get-ability-pos x))
                                (posn-y (get-ability-pos x))
                                acc))
               scene
               abilities))

; Folosind `place-image/place-images` va poziționa abilităţile active
; în partea de sus a ecranului lângă scor.
; Imaginiile vor scalate cu un factor de 0.75 și așezate plecând
; de la ability-posn (constantă globală) cu spații de 50 de px.
; Imaginea cu indexul i va fi așezată la (ability-posn.x - 50*i, ability-posn.y)
(define (place-active-abilities abilities scene)
  (let iter ((i 1) (len (length abilities)) (scene scene) (L abilities))
    (if (<= i len)
        (iter (add1 i)
              len
              (place-image (scale 0.75 (get-ability-image (car L)))
                           (- (posn-x abilities-posn) (* 50 (sub1 i)))
                           (posn-y abilities-posn)
                           
                           scene)
              (cdr L))
        scene)))

(module+ main
	(big-bang (get-initial-state)
	 [on-tick next-state (/ 1.0 fps)]
	 [to-draw draw-frame]
	 [on-key change]
	 [stop-when invalid-state?]
	 [close-on-stop #t]
	 [record? #f]))
