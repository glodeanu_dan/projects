
#ifndef TEAM_EXTRACTOR_H_D
#define TEAM_EXTRACTOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FootballClub.h"

// returneaza un element de tip *Player ce are date identice cu jucatorul init 
// dar are o adresa diferita
Player *transfer_data(Player *init) {
	Player *fin = malloc(sizeof(Player));  

	if (fin == NULL) {
		return NULL;
	}

	// se aloca si se copiaza datele
	fin->name = malloc((strlen(init->name) + 1) * sizeof(char));
	if (fin->name == NULL) {
		return NULL;
	}
	memcpy(fin->name, init->name, (strlen(init->name) + 1));

	fin->position = malloc((strlen(init->position) + 1) * sizeof(char));
	if (fin->position == NULL) {
		return NULL;
	}
	memcpy(fin->position, init->position, (strlen(init->position) + 1));

	fin->score = init->score;
	fin->injured = init->injured;
	fin->prev = NULL;
	fin->next = NULL;
	return fin;
}

// returneaza o lista de jucatori neaccidentati a clubului A si B
Player *union_teams(FootballClub *clubs, char *club_A, char *club_B) {
	//se gasesc cluburile
	Player *a = (find_club(clubs, club_A))->players,
		   *b = (find_club(clubs, club_B))->players,
		   *aux, *tmp = NULL;
	int i = 0;

	// se parcurg pe rand clubul A si B, adauganduse jucatorii intr-o noua lista
	// elementele careia au adrese diferite fata de cei din listele cluburilor
	while (a != NULL) {
		aux = transfer_data(a);
		help_add(&tmp, &aux);
		i++;
		aux = aux->next;
		a = a->next;
	}

	while (b != NULL) {
		aux = transfer_data(b);
		help_add(&tmp, &aux);
		aux = aux->next;
		b = b->next;
	}
	return  tmp;
}

// returneaza jucatorul cu cel mai mare scor de pe pozitia position
Player *get_best_player(FootballClub *clubs, char *position) {
	// se aloca un element de tip *Player cu valoare scorului -101 care va fi 
	// schimbat in cazulin care mai exista jucatori pe pozitia position
	FootballClub *aux = clubs;
	Player *tmp = aux->players;
	Player *max = malloc(sizeof(Player));
	int help = 1;
	
	if (max == NULL) {
		return NULL;
	}

	max->score = -101;

	// se parcurg toate cluburile si toate listele de jucatori, in cautarea
	// jucatorului cu pozitia position si scor maxim 
	while (aux != NULL) {
		tmp = aux->players;
		while (tmp != NULL) {
			if (strcmp(tmp->position, position) == 0) {
				if ((max->score < tmp->score) || (max->score == tmp->score
					&& strcmp(tmp->name, max->name) < 0)) {
					if (help) {
						free(max);
					}
					max = tmp;
					help = 0;
				}
			}
			tmp = tmp->next;
		}
		aux = aux->next;
	}

	if (max->score != -101) {
		return transfer_data(max);
	}
	else {
		free(max);
		return NULL;
	}
}

// se adauga jucatorul player in lsta de jucatori player_list, respectandu-se
// o ordine
void insert_player(Player **players_list, Player **player) {
	// caz 1: lista e goala
	if (len_players(players_list[0]) == 0) {
		player[0]->next = NULL;
		player[0]->prev = NULL;
		players_list[0] = player[0];
		return;
	}
	else {
		player[0]->next = NULL;
		player[0]->prev = NULL;
		Player *tmp = players_list[0];
		int i = 0, j;

		// gaseste pozitia pe care trebuie adugat jucatorul
		while (tmp != NULL && player[0]->score < tmp->score) {
			tmp = tmp->next;
			i++;
		}
		while (tmp != NULL && strcmp(player[0]->name, tmp->name) > 0 &&
			player[0]->score == tmp->score) {
			tmp = tmp->next;
			i++;
		}
		tmp = players_list[0];

		// caz 2: se adauga la inceput
		if (i == 0) {
			player[0]->next = tmp;
			player[0]->prev = NULL;
			tmp->prev = player[0];
			players_list[0] = player[0];
			return;
		}
		for (j = 1; j < i; j++) {
			tmp = tmp->next;
		}

		// caz 3: se adauga la sfarsit
		if (len_players(players_list[0]) == i) {
			player[0]->prev = tmp;
			player[0]->next = NULL;
			tmp->next = player[0];
			return;
		}
		// caz 4: se adauga in mijloc
		else {
			player[0]->prev = tmp;
			player[0]->next = tmp->next;
			tmp->next->prev = player[0];
			tmp->next = player[0];
			return;
		}
	}
}

// intoarcce cel mai mare scor dintr-o lista de jucatori
Player *get_best_score(Player *list) {
	// se aloca un element de tip *Player cu valoare scorului -101 care va fi 
	// schimbat in cazulin care mai exista jucatori 
	Player *aux = list;
	Player *max = malloc(sizeof(Player));
	int help = 1;
	
	if (max == NULL) {
		return NULL;
	}

	max->score = -101;

	// se parcurge lista in cautarea elementului maxim
	while (aux != NULL) {
		if ((max->score < aux->score) || (max->score == aux->score
			&& strcmp(aux->name, max->name) < 0)) {
				if (help) {
					free(max);
				}
				max = aux;
		}
		aux = aux->next;
	}

	if (max->score != -101) {
		return max;
	}
	else {
		free(max);
		return NULL;
	}
}

// returneaza o lista cu toti jucatorii dintr-un club, acestia avand
// adrese diferite
Player *union_players(FootballClub *clubs) {
	FootballClub *aux = clubs;
	Player *tmp = NULL, *a, *b;

		a = aux->players;
		while (a != NULL) {
			b = transfer_data(a);
			insert_player(&tmp, &b);
			b = b->next;
			a = a->next;
		}
	return  tmp;
}

// intoarce o lista de N jucatori neaccidentati cu scoruri maxime
Player *get_top_players(FootballClub *clubs, int N) {
	Player *aux, *tmp, *fin = NULL;
	FootballClub *club = clubs;
	int i;

	//se parcurg cluburile, creandu-se o copie a jucatorilor 
	while (club != NULL) {
		i = N;
		aux = union_players(club);
		while (i && aux != NULL) {
			tmp = get_best_score(aux);
			i--;
			if (tmp == aux) {
				extract_player(&aux, &tmp);
				insert_player(&fin, &tmp);
			}
			else {
				extract_player(&aux, &tmp);
				insert_player(&fin, &tmp);
				aux = aux->next;
			}
		}
		destroy_player_list(aux);
		club = club->next;
	}
	return fin;
}

// returneaza o lista de jucatori ce au scor cel putin egal cu score
Player *get_players_by_score(FootballClub *clubs, int score) {
	FootballClub *aux = clubs;
	Player *tmp = NULL, *a, *b;

	// se parcurg pe rand cluburile, jucatorii fiind selectati atat din lista 
	// jucatorilor accidentati cat si neaccidentati 
	while (aux != NULL) {
		a = aux->players;
		while (a != NULL) {
			if (score <= a->score) {
				b = transfer_data(a);
				insert_player(&tmp, &b);
				b = b->next;
			}
			a = a->next;
		}
		aux = aux->next; 
	}

	aux = clubs;
	while (aux != NULL) {
		a = aux->injured_players;
		while (a != NULL) {
			if (score <= a->score) {
				b = transfer_data(a);
				insert_player(&tmp, &b);
				b = b->next;
			}
			a = a->next;
		}
		aux = aux->next; 
	}
	return  tmp;
}

// returneaza o lista ordonata de jucatori ce au toti aceeasi pozitie
Player *get_players_by_position(FootballClub *clubs, char *position) {
	FootballClub *aux = clubs;
	Player *tmp = NULL, *a, *b;

	// se parcurg pe rand cluburile, jucatorii fiind selectati jucatorilor de pe 
	// pozitia position atat din lista accidentati cat si neaccidentati 
	while (aux != NULL) {
		a = aux->players;
		while (a != NULL) {
			if (strcmp(position, a->position) == 0) {
				b = transfer_data(a);
				insert_player(&tmp, &b);
				b = b->next;
			}
			a = a->next;
		}
		aux = aux->next; 
	}

	aux = clubs;
	while (aux != NULL) {
		a = aux->injured_players;
		while (a != NULL) {
			if (strcmp(position, a->position) == 0) {
				b = transfer_data(a);
				insert_player(&tmp, &b);
				b = b->next;
			}
			a = a->next;
		}
		aux = aux->next; 
	}
	// destroy_player_list(tmp->next);
	return  tmp;
}

// returneaza o lista ordonata de jucatori formata din cel mult: 1 portar, 4
// fundasi, 3 mijlocasi si 3 atacanti, toti avand scor maxim pentru pozitia lor
Player *get_best_team(FootballClub *clubs) {
	Player *aux = NULL, *tmp, *head, *ind;
	int x = 4, y = 3, z = 3;

	// din lista cu cei mai buni portari il selectez pe primul
	tmp = get_players_by_position(clubs,"portar");
	destroy_player_list(tmp->next);
	insert_player(&aux, &tmp);

	head = get_players_by_score(clubs, -100);
	tmp = head;

	// selectez pe rand x,y,z jucatori in corespondenta cu pozitie
	while (tmp != NULL) {
		if (strcmp(tmp->position, "fundas") == 0 && x > 0) {
			ind = transfer_data(tmp);
			insert_player(&aux, &ind);
			x--;
		}
		tmp = tmp->next;
	}

	tmp = head;
	while (tmp != NULL) {
		if (strcmp(tmp->position, "mijlocas") == 0 && y > 0) {
			ind = transfer_data(tmp);
			insert_player(&aux, &ind);
			y--;
		}
		tmp = tmp->next;
	}

	tmp = head;
	while (tmp != NULL) {
		if (strcmp(tmp->position, "atacant") == 0 && z > 0) {
			ind = transfer_data(tmp);
			insert_player(&aux, &ind);
			z--;
		}
		tmp = tmp->next;
	}
	destroy_player_list(head);
	return aux;
}

#endif // TEAM_EXTRACTOR_H_INCLUDED