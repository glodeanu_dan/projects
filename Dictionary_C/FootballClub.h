#ifndef FOOTBALL_CLUB_H_D
#define FOOTBALL_CLUB_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// List node which contains information about one football player.
typedef struct Player {
	char *name;					// player's name
	char *position;				// player's game position
	int score;					// player's score
	int injured;				// indicator for injury (1 true, 0 false)
	struct Player *next;		// next list node
	struct Player *prev;		// previous list node
} Player;

// Structure which contains information about one football club.
typedef struct FootballClub {
	char *name;					// club's name
	Player *players;			// list with players in good shape
	Player *injured_players;	// list with injured players
	struct FootballClub *next;	// next list node
} FootballClub;

// returneaza pointerul catre un club cu numele name din listade cluburi
FootballClub *find_club(FootballClub *clubs, char *name) {
	if (clubs == NULL) {
		return NULL;
	}

	FootballClub *aux = clubs;
	while (strcmp(aux->name, name) != 0 && aux != NULL) {
		if (aux->next == NULL && strcmp(aux->name, name) != 0) {
			return NULL;
		}
		aux = aux->next;
	}

	return aux;
}

// returneaza pointerul catre un jucator cu numele name din lista de jucatori
Player *find_player(Player *player, char *name) {
	if (player == NULL) {
		return NULL;
	}
	while (strcmp(player->name, name) != 0 && player != NULL) {
		if (player->next == NULL && strcmp(player->name, name) != 0) {
			return NULL;
		}
		player = player->next;
	}

	return player;
}

// calculeaza lungimea unei liste de jucatori
int len_players(Player *player){
	if(player == NULL) {
		return 0;
	}

	int len = 0;
	Player *aux = player;

	while (aux != NULL) {
		len++;
		aux=aux->next;
	}
	return len;
}

// initializeaza clubs_no cluburi asociindu-le cate un nume din vectorul de 
// stringuri names
FootballClub *initialize_clubs(int clubs_no, char **names) {
	if (clubs_no < 1) {
		return NULL;
	}

	FootballClub *clubs= malloc(sizeof(FootballClub));

	if (clubs == NULL) {
		return NULL;
	}

	FootballClub *aux = clubs, *head = clubs;
	int i;
	clubs->players = NULL;
	clubs->injured_players = NULL;

	for (i = 0; i < clubs_no; i++) {
		if(i > 0) {
			clubs = clubs->next;
			clubs = malloc(sizeof(FootballClub));

			if (clubs == NULL) {
				return NULL;
			}
			clubs->players = NULL;
			clubs->injured_players = NULL;
			aux->next = clubs;
			aux = aux->next;
		}
		clubs->name = malloc((strlen(names[i]) + 1) * sizeof(char));

		if (clubs->name == NULL) {
			return NULL;
		}

		strcpy(clubs->name, names[i]);
	}
	clubs->players = NULL;
			clubs->injured_players = NULL;
	clubs->next = NULL;
	return head;
}

//adauga un club cu numele name la sfarsit in lista de cluburi
FootballClub *add_club(FootballClub *clubs, char *name) {
	FootballClub *aux = initialize_clubs(1, &name);

	if (clubs == NULL) {
		return aux;
	}
	else {
		FootballClub *tmp = clubs;

		while (tmp->next != NULL) {
			tmp = tmp->next;
		}

		tmp->next = aux;

		return clubs;
	}
}

// scoate jucatorul tmp din lista de jucatori aux
void extract_player(Player **aux, Player **tmp) {
	if (len_players(aux[0]) == 1) {	
		aux[0] = NULL;
	}
	else if (tmp[0]->prev == NULL) {
		tmp[0]->next->prev = NULL;
		aux[0] = tmp[0]->next;
	}
	else if (tmp[0]->next == NULL) {
		tmp[0]->prev->next = NULL;
	}
	else {
		tmp[0]->prev->next = tmp[0]->next;
		tmp[0]->next->prev = tmp[0]->prev;
	}
}

// adauga ordonat jucatorul player in lista player_list
void help_add(Player **players_list, Player **player) {
	// caz 1: lista e goala
	if (len_players(players_list[0]) == 0) {
		player[0]->next = NULL;
		player[0]->prev = NULL;
		players_list[0] = player[0];
		return;
	}
	else {
		Player *tmp = players_list[0];
		int i = 0, j;
		// gaseste pozitia pe care trebuie adugat jucatorul
		while (tmp != NULL && strcmp(player[0]->position, tmp->position) > 0) {
			tmp = tmp->next;
			i++;
		}
		while (tmp != NULL && player[0]->score < tmp->score &&
			strcmp(player[0]->position, tmp->position) == 0) {
			tmp = tmp->next;
			i++;
		}
		while (tmp != NULL && strcmp(player[0]->name, tmp->name) > 0 &&
			strcmp(player[0]->position, tmp->position) == 0 &&
			player[0]->score == tmp->score) {
			tmp = tmp->next;
			i++;
		}
		tmp = players_list[0];

		// caz 2: se adauga la inceput
		if (i == 0) {
			player[0]->next = tmp;
			player[0]->prev = NULL;
			tmp->prev = player[0];
			(*players_list) = player[0];
			return;
		}
		for (j = 1; j < i; j++) {
			tmp = tmp->next;
		}

		// caz 3: se adauga la sfarsit
		if (len_players(players_list[0]) == i) {
			player[0]->prev = tmp;
			player[0]->next = NULL;
			tmp->next = player[0];
			return;
		}
		// caz 4: se adauga in mijloc
		else {
			player[0]->prev = tmp;
			player[0]->next = tmp->next;
			tmp->next->prev = player[0];
			tmp->next = player[0];
			return;
		}
	}
}

// adauga un jucator in lista de cluburi clubs, unctia primind doar date despre
// jucator
void add_player(FootballClub *clubs, char *club_name, 
				char *player_name, char *position, int score) {
	//se verifica cazuri de eroare
	if ((find_club(clubs, club_name)) == NULL || clubs == NULL) {
		return;
	} 
	//se aloca jucatorul si se scriu datele
	FootballClub *club = find_club(clubs, club_name);
	Player *aux = malloc(sizeof(Player));

	if (aux == NULL) {
		return;
	}

	aux->name = malloc((strlen(player_name) + 1) * sizeof(char));  

	if (aux->name == NULL) {
		return;
	}

	aux->position = malloc((strlen(position) + 1)* sizeof(char)); 

	if (aux->position == NULL) {
		free(aux);
		free(aux->name);
		return;
	}

	memcpy(aux->name, player_name, strlen(player_name) + 1);
	memcpy(aux->position, position, strlen(position) + 1);
	aux->injured = 0;
	aux->score = score;
	aux->next = NULL;
	aux->prev = NULL;

	// se adauga jucatorul
	help_add(&club->players, &aux);
}

// transfera jucatorul cu numele player_name din clubul cu numele old_club in
// clubul cu numele new_club
void transfer_player(FootballClub *clubs, char *player_name, 
					char *old_club, char *new_club) {
	// cazuri de eroare
	if (clubs == NULL) {
		return;
	}
	FootballClub *aux1 = find_club(clubs, old_club),
				 *aux2 = find_club(clubs, new_club);
	// cazuri de eroare
	if (!(find_player(aux1->players, player_name) == NULL || 
		find_player(aux1->injured_players, player_name) == NULL)) {
		return;
	}

	if(aux1 == NULL || aux2 == NULL) {
		return;
	}

	Player *tmp = find_player(aux1->players, player_name);
	// se cauta si in lista jucatorilor accidentati
	if(tmp == NULL) {
		tmp = find_player((find_club(clubs, old_club))->injured_players, 
			player_name);
		if (tmp == NULL) {
			return;
		}
		// se extrage jucatorul din vechiul club si se adauga in clubul nou
		extract_player(&aux1->injured_players, &tmp);
		help_add(&aux2->injured_players, &tmp);
		return;
	}
	// se extrage jucatorul din vechiul club si se adauga in clubul nou
	extract_player(&aux1->players, &tmp);
	help_add(&aux2->players, &tmp);
}

// se sterge un jucator cu numele player_name din clubul cu numele club_name
void remove_player(FootballClub *clubs, char *club_name, char *player_name) {
	//cazuri de eroare
	if (find_club(clubs, club_name) == NULL || 
		((find_club(clubs, club_name)->players == NULL) &&
		(find_club(clubs, club_name)->injured_players == NULL))) {
		return;
	}

	FootballClub *aux = find_club(clubs, club_name);

	if(aux == NULL) {
		return;
	}
	// se cauta in lista de jucatori neaccidentati
	Player *tmp = find_player(aux->players, player_name);
	// se cauta in lista de jucatori accidentati
	if  (tmp == NULL) {
		tmp = find_player(aux->injured_players, player_name);

		if(tmp == NULL) {
			return;
		}
		// se extrage jucatorul din lista clubului
		extract_player(&aux->injured_players, &tmp);
		free(tmp->name);
		free(tmp->position);
		free(tmp);
		return;
	}
	// se extrage jucatorul din lista clubului
	extract_player(&aux->players, &tmp);
	free(tmp->name);
	free(tmp->position);
	free(tmp);
}

// schimba scorul unui jucator cu numele player_name din clubul cu numele
// club_name
void update_score(FootballClub *clubs, char *club_name, 
					char *player_name, int score) {
	if (clubs == NULL || (find_club(clubs, club_name))->players == NULL) {
		return;
	}

	FootballClub *aux = find_club(clubs, club_name);

	if(aux == NULL) {
		return;
	}

	Player *tmp = find_player(aux->players, player_name);

	if(tmp == NULL) {
		tmp = find_player(aux->injured_players, player_name);
		if (tmp == NULL) {
			return;
		}
	}

	tmp->score = score;
}

// schimba pozitia unui jucator cu numele player_name din clubul cu numele
// club_name
void update_game_position(FootballClub *clubs, char *club_name, 
							char *player_name, char *position, int score) {
	// se gaseste clubul, jucatorul si se trateaza cazurile de eroare
	if (clubs == NULL || (find_club(clubs, club_name)->players == NULL)) {
		return;
	}

	FootballClub *aux = find_club(clubs, club_name);

	if(aux == NULL) {
		return;
	}

	Player *tmp = find_player(aux->players, player_name);

	if(tmp == NULL) {
		return;
	}

	free(tmp->position);
	tmp->position = malloc((strlen(position) + 1) * sizeof(char));

	// schimb pozitia
	if (tmp->position == NULL) {
		return;
	}

	strcpy(tmp->position, position);
	tmp->score = score;

	//extrag si adaug jucatorul pentru a-l pozitiona corect
	extract_player(&aux->players, &tmp);
	help_add(&aux->players, &tmp);
}

// se muta jucatorul cu numele player_name in lista de jucatori accidntati
void add_injury(FootballClub *clubs, char *club_name,
				char *player_name, int days_no) {
	// se gaseste clubul, jucatorul si se trateaza cazurile de eroare
	if (clubs == NULL || (find_club(clubs, club_name) == NULL)) {
		return;
	}

	FootballClub *aux = find_club(clubs, club_name);

	if(aux == NULL) {
		return;
	}

	Player *tmp = find_player(aux->players, player_name);

	if(tmp == NULL) {
		return;
	}

	// modific parametrii injured si score
	tmp->injured = 1;

	if (tmp->score - 0.1 * days_no < -100) {
		tmp->score = -100;
	}
	else {
		tmp->score = tmp->score - 0.1 * days_no;
	}

	// extrag jucatorul din lista de jucatori neaccidentati si il adaug in cea
	// de jucatori accidentati
	extract_player(&aux->players, &tmp);
	help_add(&aux->injured_players, &tmp);
}

// se muta jucatorul cu numele player_name in lista de jucatori neaccidntati
void recover_from_injury(FootballClub *clubs, char *club_name, 
							char *player_name) {
	// se gaseste clubul, jucatorul si se trateaza cazurile de eroare
	if (clubs == NULL || find_club(clubs, club_name) == NULL) {
		return;
	}

	FootballClub *aux = find_club(clubs, club_name);

	if(aux == NULL) {
		return;
	}

	Player *tmp = find_player(aux->injured_players, player_name);

	if(tmp == NULL) {
		return;
	}

	if (aux == NULL || tmp == NULL) {
		return;
	} 


	// modific parametriul injured
	tmp->injured = 0;

	// extrag jucatorul din lista de jucatori accidentati si il adaug in cea
	// de jucatori neaccidentati
	extract_player(&aux->injured_players, &tmp);
	help_add(&aux->players, &tmp);
}

// Frees memory for a list of Player.
void destroy_player_list(Player *player) {
	Player *aux;

	while (player != NULL) {
		aux = player->next;
		free(player->name);
		free(player->position);
		free(player);
		player = aux;
	}
}

// Frees memory for a list of FootballClub.
void destroy_club_list(FootballClub *clubs) {
	FootballClub *aux;

	while (clubs != NULL) {
		aux = clubs->next;
		destroy_player_list(clubs->players);
		destroy_player_list(clubs->injured_players);
		free(clubs->name);
		free(clubs);
		clubs = aux;
	}
}

// Displays a list of players.
void show_list(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	while (player) {
		fprintf(f, "(%s, %s, %d, %c) ", 
			player->name,
			player->position,
			player->score,
			player->injured ? 'Y' : '_');
		player = player->next;
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}

// Displays a list of players in reverse.
void show_list_reverse(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	if (player) {
		while (player->next) {
			player = player->next;
		}
		while (player) {
			fprintf(f, "(%s, %s, %d, %c) ", 
				player->name,
				player->position,
				player->score,
				player->injured ? 'Y' : '_');
			player = player->prev;
		}
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}


// Displays information about a football club.
void show_clubs_info(FILE *f, FootballClub *clubs) {
	fprintf(f, "FCs:\n");
	while (clubs) {
		fprintf(f, "%s\n", clubs->name);
		fprintf(f, "\t");
		show_list(f, clubs->players, 0);
		fprintf(f, "\t");
		show_list(f, clubs->injured_players, 0);
		clubs = clubs->next;
	}
}

#endif // FOOTBALL_CLUB_H_INCLUDED