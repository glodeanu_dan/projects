package graphalgo;

import graphs.Edge;
import graphs.UndirectedGraph;

import java.util.HashSet;

import java.util.Set;


public class Prim {

	private double dist[];
	private boolean mark[];
	private Edge[] parent;
	private int N;
	UndirectedGraph graph;
	public int usedMemory = 0;

	public UndirectedGraph FindMST(UndirectedGraph g) {
		long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		graph = new UndirectedGraph(g.getNrNodes());
		N = g.getNrNodes();
	
		mark = new boolean[N];
		dist = new double[N];

		parent = new Edge[N];
		for (int v = 0; v < N; v++) {
			parent[v] = null;
			dist[v] = Double.POSITIVE_INFINITY;
			mark[v] = false;
		}
		long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		int root=0;
		doPrim(g, root);

		Set<Edge> result = new HashSet<Edge>();
		for (int v = 0; v < N; v++) {
			if (parent[v] != null) {
				graph.addEdge(parent[v].source(), parent[v].destination(), parent[v].weight());
			}
		}

		int la = 1;
		for (int v = 0; v < N; v++) {
			if(!mark[v]) {
				la = 0;
			}
		}

		return graph;
	}

	
	 
	private int extractMin() {		
		double valmin = Double.POSITIVE_INFINITY;
		int indmin = -1;
		for (int i = 0; i < N; i++)
			if (!mark[i]) {
				if (dist[i] <= valmin) {
					valmin = dist[i];
					indmin = i;
				}
			}
		mark[indmin]=true;
		dist[indmin]=0;
		return indmin;
	}

	private void doPrim(UndirectedGraph g, Integer root) {
		long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		dist[root] = 0;
        int iter=0;
		while  (iter<N){ // iterate until all N nodes are added to the MST 
			iter++;
			
			int u = extractMin(); 
			
			for (Edge e : g.edgesOutgoingFrom(u)) {
				int v = e.other(u);
				if (dist[v] > e.weight()) {
					parent[v] = e;
					dist[v]= e.weight();
				}
			}
		}
		long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

	}
}
