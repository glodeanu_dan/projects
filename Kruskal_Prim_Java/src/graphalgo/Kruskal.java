package graphalgo;

import graphs.Edge;
import graphs.Node;
import graphs.UndirectedGraph;

import java.util.*;


public class Kruskal {
	private int N;
	private int M;
	UndirectedGraph graph;


	int partition(Edge arr[], int low, int high)
	{
		Edge pivot = arr[high];
		int i = (low-1);
		for (int j=low; j<high; j++)
		{
			if (arr[j].weight() < pivot.weight())
			{
				i++;

				Edge temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}

		Edge temp = arr[i+1];
		arr[i+1] = arr[high];
		arr[high] = temp;

		return i+1;
	}

	void sort(Edge arr[], int low, int high)
	{
		if (low < high)
		{
			int pi = partition(arr, low, high);

			sort(arr, low, pi-1);
			sort(arr, pi+1, high);
		}
	}

	public boolean notCycle(Edge edge, UndirectedGraph g) {

		if (g.getMap()[edge.source()].size() == 0 || g.getMap()[edge.destination()].size() == 0 || g.getNrNodes() == 0) {
			return true;
		}

		boolean visited[] = new boolean[g.getNrNodes()];

		for (int k = 0; k < g.getNrNodes(); k++) {
			visited[k] = false;
		}

		LinkedList<Integer> queue = new LinkedList<Integer>();
		int s = edge.source();

		visited[s] = true;
		queue.add(s);

		Map<Integer, Double> i;
		while (queue.size() != 0) {
			s = queue.poll();
			i = g.getMap()[s];

			for (Map.Entry<Integer, Double> aux : i.entrySet()) {
				if (aux.getKey() == edge.destination()) {
					return false;
				}
				if (!visited[aux.getKey()]) {
					visited[aux.getKey()] = true;
					queue.add(aux.getKey());
				}
			}
		}
		return true;
	}

	public UndirectedGraph FindMST(UndirectedGraph g) {

		N = g.getNrNodes();
		M = g.getNrEdges();
		Edge[] edges = new Edge[M];
		int i = 0;

		for (Edge e: g.allEdges()) {
			edges[i] = e;
			i++;
		}

		M = i;

		sort(edges, 0, M - 1);

		Node[] nodes = new Node[N];

		for (i = 0; i < N; i++) {
			nodes[i] = new Node(i, N);
		}

		graph = new UndirectedGraph(g.getNrNodes());

		for (i = 0; i < M; i++) {
			if (!nodes[edges[i].source()].isConnected[edges[i].destination()]) {
				graph.addEdge(edges[i].source(), edges[i].destination(), edges[i].weight());
				nodes[edges[i].source()].connect(nodes[edges[i].destination()], nodes);
			}
		}
		return graph;
	}


}
