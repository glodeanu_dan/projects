package test;

import graphalgo.Kruskal;
import graphalgo.Prim;
import graphs.Edge;
import graphs.UndirectedGraph;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		UndirectedGraph prim = null;
		UndirectedGraph kruskal = null;
		String test = "test10.in";

		try {
			prim = new UndirectedGraph("in/" + test);
			kruskal = new UndirectedGraph("in/" + test);
		} catch (IOException e) {
			e.printStackTrace();
		}

		UndirectedGraph resultP;
		double minP;
		System.out.println("\nMST of prim undirected graph:");
		Prim mstP = new Prim();
		long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		double startTime = System.nanoTime();
		resultP = mstP.FindMST(prim);
		double endTime=System.nanoTime();
		long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("Done in "+(endTime-startTime)/1000000 +" milisec");

		minP = 0;
//		resultP.printGraphAdjStructure();
		for (Edge e : resultP.allEdges()) {
			minP = minP + e.weight();
		}
		System.out.println("Min total weight is " + minP);

		UndirectedGraph resultK;
		double minK;
		System.out.println("\nMST of Kruskal undirected graph:");
		Kruskal mstK = new Kruskal();

		startTime = System.nanoTime();
		resultK = mstK.FindMST(kruskal);
		endTime=System.nanoTime();
		System.out.println("Done in "+(endTime-startTime)/1000000 +" milisec");


		minK = 0;
//		resultK.printGraphAdjStructure();
		for (Edge e : resultK.allEdges()) {
			minK = minK + e.weight();
		}
		System.out.println("Min total weight is " + minK);
	}
}
