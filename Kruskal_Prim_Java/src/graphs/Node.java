package graphs;

import java.util.ArrayList;

public class Node {
    public boolean[] isConnected;
    public int val;
    public int size;

    public Node(int val, int N) {
        this.val = val;
        size = N;

        isConnected = new boolean[N];

        for (int i = 0; i < N; i++) {
            isConnected[i] = false;
        }
    }

    public void print(Node[] nodes) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (nodes[i].isConnected[j])
                    System.out.println(nodes[i].val + " connected with " + j);
            }
        }
    }

    public void connect(Node node, Node[] nodes) {
        this.isConnected[node.val] = true;
        node.isConnected[this.val] = true;

        for (int i = 0; i < size; i++) {
            if (this.isConnected[i] && !node.isConnected[i] && node.val != i) {
                node.isConnected[i] = true;
                nodes[i].isConnected[node.val] = true;
                for (int j = 0; j < size; j++) {
                    if (node.isConnected[j] && !this.isConnected[j] && j != this.val) {
                        this.isConnected[j] = true;
                        nodes[j].isConnected[this.val] = true;
                    }

                    if (node.isConnected[j] && !nodes[i].isConnected[j] && i != j) {
                        nodes[i].isConnected[j] = true;
                        nodes[j].isConnected[i] = true;
                    }
                }
            }

            if (node.isConnected[i] && !this.isConnected[i] && this.val != i) {
                this.isConnected[i] = true;
                nodes[i].isConnected[this.val] = true;
                for (int j = 0; j < size; j++) {
                    if (this.isConnected[j] && !node.isConnected[j] && j != node.val) {
                        node.isConnected[j] = true;
                        nodes[j].isConnected[node.val] = true;
                    }

                    if (this.isConnected[j] && !nodes[i].isConnected[j] && i != j) {
                        nodes[i].isConnected[j] = true;
                        nodes[j].isConnected[i] = true;
                    }
                }
            }
        }
    }
}
