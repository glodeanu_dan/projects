package graphs;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class UndirectedGraph {

	protected int N;
	protected int M;
	protected Map<Integer, Double>[] g = null;

	public UndirectedGraph(int N) {
		this.N = N;
		M = 0;
		g = (Map<Integer, Double>[]) new Map[N];
		for (int v = 0; v < N; v++) {
			g[v] = new HashMap<Integer, Double>();
		}
	}

	public UndirectedGraph(String file) throws IOException {
		readGraph(file);
	}

	public void addEdge(int from, int to, double weight) {
		if (hasNode(from) && hasNode(to)) {
			g[from].put(to, weight);
			g[to].put(from, weight);
			M++;
		}
	}

	public int getNrNodes() {
		return N;
	}

	public int getNrEdges() {
		return M;
	}

	public boolean hasNode(int node) {
		if ((node >= 0) && (node < N))
			return true;
		else
			return false;
	}

	public void readGraph(String file) throws IOException {
		File input = new File(file);
		Scanner is = new Scanner(input);

		N = is.nextInt();
		M = 0;
		g = (Map<Integer, Double>[]) new Map[N];
		for (int v = 0; v < N; v++) {
			g[v] = new HashMap<Integer, Double>();
		}

		int from, to;
		double weight;

		while (is.hasNext()) {
			from = is.nextInt();
			to = is.nextInt();

			weight = is.nextFloat();

			addEdge(from, to, weight);

		}
	}

	public void printGraphAdjStructure() {
		for (int s = 0; s < N; s++) {
			System.out.print(s + " : ");
			for (Integer t : g[s].keySet()) {
				System.out.print(t + " ");
			}
			System.out.println();
		}
	}

	public Iterable<Edge> edgesOutgoingFrom(int node) {
		Set<Edge> edgeSet = new HashSet<Edge>();
		for (Map.Entry<Integer, Double> e : g[node].entrySet()) {
			Edge ed = new Edge(node, e.getKey(), e.getValue());
			edgeSet.add(ed);
		}
		return edgeSet;
	}

	public Iterable<Edge> allEdges() {
		Set<Edge> edgeSet = new HashSet<Edge>();
		for (int node = 0; node < N; node++)
			for (Map.Entry<Integer, Double> e : g[node].entrySet()) {
				if (e.getKey() > node) {
					Edge ed = new Edge(node, e.getKey(), e.getValue());
					edgeSet.add(ed);
				}
			}
		return edgeSet;
	}

	public Map<Integer, Double>[] getMap() {
		return g;
	}
}
