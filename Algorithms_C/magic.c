#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

int min = 101;
int min_i = -1;
int min_j = -1;

// citire
int *read_biletele(int *n) {
	FILE *fp;
	size_t len = 0;
	char *line = NULL;
	char *aux, *tmp;
	int i = 0;

    fp = fopen("magic.in", "r");
    if (fp == NULL)
        return NULL;

    getline(&line, &len, fp);
    aux = strtok_r(line, " \n", &tmp);
    *n = atoi(aux);

    int *v = malloc(sizeof(int) * (*n));
	getline(&line, &len, fp);
	aux = strtok_r(line, " \n", &tmp);
	v[i] = atoi(aux);

    if (abs(v[i]) < min && v[i] != 0) {
        min = abs(v[i]);
        min_i = i;
        min_j = i + 1;
        printf("%d %d\n", v[i], i);
    }

    for (i = 1; i < *n; i++) {
		aux = strtok_r(NULL, " \n", &tmp);
		v[i] = atoi(aux);

        // salvez diferenta minima
        if (abs(v[i]) < min && v[i] != 0) {
            min = abs(v[i]);
            min_i = i;
            min_j = i + 1;
        }
    }

    fclose(fp);
    if (line)
        free(line);

    return v;
}

// scrierea rezultatului
void write_biletele(int s, int d, int magic) {
    int i;
    FILE *fp;

    fp = fopen("magic.out", "w");

    if (fp == NULL) {
      return;
    }

    fprintf(fp, "%d\n", magic);
    fprintf(fp, "%d %d\n", s, d);

    fclose(fp);
}

// algoritmul propruzis
void biletele() {
	int n;
	int *v;
	v = read_biletele(&n);

    int magic_s = min_i;
    int magic_d = min_j;
    int magic = min * min + 1;
    int i, j, sum, diff, ss = 0, sd = 0, try;

    // iterez prin vector v[i] fiind suma din stanga
    for (i = 0; i < n - 1; i++) {
    	if (i > 0)
    		ss += v[i - 1];

    	sd = ss;

        // iterez de la i + 1, v[j] fiind suma dreapta
    	for (j = i + 1; j < n; j++) {
            // daca distanta e mai mare ca numarul magic minim
            // deja calculat merg la urmatoarea iteratie
            if ((j - i) * (j - i) >= magic) {
                break;
            }


    		sd += v[j - 1];
    		sum = ss - sd;
    		sum *= sum;

    		diff = j - i;
    		diff *= diff;

    		if (diff >= magic) {
    			break;
            }

            // verific daca am descoperit un numar magic mai bun
    		if (diff + sum < magic) {
    			magic = diff + sum;
    			magic_s = i;
    			magic_d = j;
    		}
    	}
    }
    // scriu rezultatul
    write_biletele(magic_s + 1, magic_d + 1, magic);
}

int main(int argc, char const *argv[]) {
    if (argc > 1) {
        printf("%s\n", argv[1]);
    }

    biletele();

    return 0;
}
