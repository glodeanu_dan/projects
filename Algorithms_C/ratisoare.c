#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "./queue.h"
#include "./queue.c"

// structura ratusca
typedef struct rnode {
	ratusca r;
	struct rnode *next;
	struct rnode *prev;
} rnode;

int *time;

// functie auxiliara de partitie pentru quick sort
int partition(ratusca *v, int begin, int end) {
	ratusca pivot = v[end];
	ratusca aux;
	int i = begin - 1;
	int j;
	for (j = begin; j < end; j++) {
		if (v[j].t < pivot.t ||
				(v[j].t == pivot.t && v[j].id < pivot.id)) {
			i++;
			aux = v[i];
			v[i] = v[j];
			v[j] = aux;
		}
	}

	aux = v[i + 1];
	v[i + 1] = v[end];
	v[end] = aux;


	return (i + 1);
}

// functiede sortare
void quickSort(ratusca *v, int begin, int end) {
	if (begin < end) {
		int pindex = partition(v, begin, end);

		quickSort(v, begin, pindex - 1);
		quickSort(v, pindex + 1, end);
	}
}

// functie ce insereaza ordonat (dupa pozitie) o ratusca in lista
rnode *insertNode(rnode *l, ratusca r) {
	rnode *node = malloc(sizeof(rnode));
	node->r = r;
	node->next = NULL;
	node->prev = NULL;

	if (l == NULL)
		return node;

	if (l->r.i > node->r.i) {
		l->prev = node;
		node->next = l;
		return node;
	}

	rnode *aux = l;

	while (aux->next && aux->next->r.i < node->r.i) {
		aux = aux->next;
	}

	if (aux->next)
		aux->next->prev = node;
	node->next = aux->next;
	aux->next = node;
	node->prev = aux;

	return l;
}

// functie de citire pentru ratuste, returneaza un vector de liste (lista e o linie)
rnode **read_ratuste(int *n, int *m, int *k, int *nr) {
	FILE *fp;
	size_t len = 0;
	int i;
	char *line = NULL;
	char *aux, *tmp_str;
	int max = 0;

    fp = fopen("ratisoare.in", "r");
    if (fp == NULL) {
        return NULL;
    }

    getline(&line, &len, fp);
    aux = strtok_r(line, " \n", &tmp_str);
    *n = atoi(aux);
    aux = strtok_r(NULL, " \n", &tmp_str);
    *m = atoi(aux);
    aux = strtok_r(NULL, " \n", &tmp_str);
    *k = atoi(aux);
    aux = strtok_r(NULL, " \n", &tmp_str);
    *nr = atoi(aux);

    time = malloc(sizeof(int) * (*nr));
    rnode **v = malloc(sizeof(rnode*) * (*n));

    for (i = 0; i < (*n); i++) {
    	v[i] = NULL;
    }

    ratusca tmp;
    int tmp_line;

    for (i = 0; i < *nr; i++) {
    	getline(&line, &len, fp);

    	aux = strtok_r(line, " \n", &tmp_str);
		tmp.id = atoi(aux);
		aux = strtok_r(NULL, " \n", &tmp_str);
		tmp_line = atoi(aux);
		aux = strtok_r(NULL, " \n", &tmp_str);
		tmp.i = atoi(aux) - 1;
		aux = strtok_r(NULL, " \n", &tmp_str);

		if (aux[0] == 'D') {
			tmp.d = 1;
			tmp.t = *m - tmp.i;
		} else {
			tmp.d = -1;
			tmp.t = tmp.i + 1;
		}

		time[tmp.id - 1] = tmp.t;
		v[tmp_line - 1] = insertNode(v[tmp_line - 1], tmp);
    }

    fclose(fp);
    if (line) {
        free(line);
    }

    return v;
}

// functie de scriere a rezultatului
void write_rat(int id) {
    int i;
    FILE *fp;

    fp = fopen("ratisoare.out", "w");

    if (fp == NULL) {
      return;
    }

    fprintf(fp, "%d\n", id);

    fclose(fp);
}

// algoritmul propriuzis
int ratuste() {
	int n, m, k, nr, i, head_time, tail_time, q_time;
	Node *current, *prev;
	rnode *aux, *tmp, *next;
	// citesc ratustele
	rnode **v = read_ratuste(&n, &m, &k, &nr);
	ratusca *fin = malloc(sizeof(ratusca) * nr);
	int id = 0, node, helper;

	// iterez prit fiecare linie a gridului
	for (i = 0; i < n; i++) {
		aux = v[i];
		// pastrez timpul pentru ratustele care sunt la inceput
		// si se duc la stanga
		while(aux && aux->r.d == -1) {
			aux = aux->next;
		}

		// daca toate se duc la stanga merg la urmatoarea linie
		if (aux == NULL) {
			continue;
		}

		// initializez o coada cu ratusca care merge la dreapta
		Queue q = initQueue(aux->r);
		// iterez prin toate ratustele ramase
		while (aux->next) {
			next = aux->next;

			// daca ratusca merge si ea la dreapta o adaug in coada
			if (next->r.d == 1) {
				q = enqueue(q, next->r);
			} else {
				/*
					daca ratusca merge in stanga:
					1) asociez timpul capului coadei cu cel al ratustei 
					2) asociez timpul ratustei cu coada coadei
					3) pentru toate ratustele ramase in coad timpul e egal
					   cu timpul predecesorului
				*/

				q = enqueue(q, next->r);
				head_time = time[next->r.id - 1];
				tail_time = time[last(q).id - 1];

				prev = q->head;
				current = prev->next;
				q_time = time[prev->data.id - 1];

				while(current) {
					helper = time[current->data.id - 1];
					time[current->data.id - 1] = q_time;
					q_time = helper;

					prev = current;
					current = current->next;
				}

				time[first(q).id - 1] = head_time;
				q = dequeue(q);
			}

			aux = aux->next;
		}
	}

	// scriu timpul nou pentru ratuste
	for (i = 0; i < nr; i++) {
		fin[i].id = i + 1;
		fin[i].t = time[i];
	}

	// sortez vectorul de ratuste si scriu rezultatul
	quickSort(fin, 0, nr - 1);
	write_rat(fin[k - 1].id);

	return 0;
}

int main(int argc, char const *argv[]) {
	ratuste();
	return 0;
}
