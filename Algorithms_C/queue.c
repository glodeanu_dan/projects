#include "./queue.h"

Node *initNode(T data) {
	Node *node;

	node = malloc(sizeof(struct node));
	node->data = data;
	node->next = NULL;
	return node;
}

Node *freeNode(Node *node) {
	if (node) {
		free(node);
	}
	return NULL;
}

Queue initQueue(T data) {
	Queue queue;

	queue = malloc(sizeof(struct queue));
	queue->head = queue->tail = initNode(data);
	queue->size = 1;
	return queue;
}

Queue initEmptyQueue() {
	Queue queue;

	queue = malloc(sizeof(struct queue));
	queue->head = queue->tail = NULL;
	queue->size = 0;
	return queue;
}

Queue enqueue(Queue queue, T data) {
	Node *node;

	if (isEmptyQueue(queue)) {
		if (queue == NULL)
			return initQueue(data);
		free(queue);
		return initQueue(data);
	}
	node = initNode(data);
	queue->tail->next = node;
	queue->tail = node;
	queue->size++;
	return queue;
}

Queue dequeue(Queue queue) {
	Node *tmp;

	if (!isEmptyQueue(queue)) {
		tmp = queue->head;
		queue->head = queue->head->next;
		tmp = freeNode(tmp);
		queue->size--;
	}
	return queue;
}

T first(Queue queue) {
	if (!isEmptyQueue(queue)) {
		return queue->head->data;
	} else {
		ratusca error;
		error.id = -1;
		return error;
	}
}

T last(Queue queue) {
	if (!isEmptyQueue(queue)) {
		return queue->tail->data;
	} else {
		ratusca error;
		error.id = -1;
		return error;
	}
}

int isEmptyQueue(Queue queue) {
	if (queue == NULL || queue->head == NULL || queue->size == 0)
		return 1;
	return 0;
}

Queue freeQueue(Queue queue) {
	while (!isEmptyQueue(queue)) {
		queue = dequeue(queue);
	}
	if (queue)
		free(queue);
	return NULL;
}
