#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

// strctura temelor
typedef struct tema {
	int id;
	int time;
	int score;
	int dline;
} tema;

// structura unui nod
typedef struct node {
	int val;
	struct node *prev;
} node;

// sortare dupa deadline
void sort(tema *v, int n) {
	int i, j;
	tema aux;

	for (i = 0; i < n - 1; i++) {
		for (j = i + 1; j < n; j++) {
			if (v[i].dline > v[j].dline) {
				aux = v[i];
				v[i] = v[j];
				v[j] = aux;
			}
		}
	}
}

// functie de citire
tema *read_teme(int *size) {
	FILE *fp;
	size_t len = 0;
    tema *v;
    int n = 0;
	int i;
	char *line = NULL;
	char *aux, *tmp;
	int max = 0;

    fp = fopen("teme.in", "r");
    if (fp == NULL)
        return NULL;

    getline(&line, &len, fp);
    n = atoi(line);
    v = malloc(n * sizeof(tema));

    for (i = 0; i < n; i++) {
    	getline(&line, &len, fp);

    	v[i].id = i + 1;
    	aux = strtok_r(line, " \n", &tmp);
		v[i].time = atoi(aux);
		aux = strtok_r(NULL, " \n", &tmp);
		v[i].score = atoi(aux);
		aux = strtok_r(NULL, " \n", &tmp);
		v[i].dline = atoi(aux);

		if (v[i].dline > max) {
			max = v[i].dline;
		}
    }

    fclose(fp);
    if (line)
        free(line);

    *size = n;
    return v;
}

// functie de scriere
void write_teme(int score, int n, int *teme) {
	int i;
	FILE *fp;
	fp = fopen("teme.out", "w");

	if(fp == NULL) {
	  return;
	}

	fprintf(fp, "%d %d\n", score, n);

	for (i = n - 1; i > 0; i--) {
		fprintf(fp, "%d ", teme[i]);
	}
	fprintf(fp, "%d\n", teme[0]);

	fclose(fp);
}

// algoritmul knapsack
void best_comb(tema *teme, int n, int timp) {
	int i, j;
	// matricea profitului
	int sem[n + 1][timp + 1];
	// matricea de noduri ce tine minte pasul anterior
	node ***solved = malloc(sizeof(node**) * (n + 1));

	// initializez nodurile
	for (i = 0; i <= n; i++) {
		solved[i] = malloc(sizeof(node*) * timp + 1);
	}

	node *aux = NULL;

	// initializez prima linie cu 0
	for (i = 0; i < timp + 1; i++) {
		sem[0][i] = 0;
		solved[0][i] = NULL;
	}

	// iterez prin linii
	for (i = 1; i < n + 1; i++) {
		// iterez prin coloane
		for (j = 0; j < timp + 1; j++) {
			solved[i][j] = malloc(sizeof(node));
			// daca deadlineul e trecut copii rezultatul de la saptamana trecuta
			if (teme[i - 1].dline < j) {
				sem[i][j] = sem[i][j - 1];
				solved[i][j]->val = 0;
				solved[i][j]->prev = solved[i][j - 1];
			} 
			// daca tema poate fi rezolvata verific daca e convenabil
			else if (teme[i - 1].time <= j && teme[i - 1].dline  >= j) {
				// nu e convenabil iau scorul de la tema trecuta
				if (sem[i - 1][j] > sem[i - 1][j - teme[i - 1].time] + teme[i - 1].score) {
					sem[i][j] = sem[i - 1][j];
					solved[i][j]->val = 0;
					solved[i][j]->prev = solved[i - 1][j];
				} 
				// e convenabil => scot scorul pentru tema cu care se suprapune
				// si adaug punctajul pentru aceasta tema
				else {
					sem[i][j] = sem[i - 1][j - teme[i - 1].time] + teme[i - 1].score;
					solved[i][j]->val = teme[i - 1].id;
					solved[i][j]->prev = solved[i - 1][j - teme[i - 1].time];
				}
			} 
			// daca tema nu poate fi rezolvata copii profitul de la tema trecuta
			else {
				sem[i][j] = sem[i - 1][j];
				solved[i][j]->val = 0;
				solved[i][j]->prev = solved[i - 1][j];
			}
		}
	}

	int max = 0;

	// gasesc maximul de pe ultima coloana
	for (i = 0; i < timp + 1; i++) {
		if (sem[n][i] > sem[n][max]) {
			max = i;
		}
	}


	int *fin = malloc(sizeof(int) * n);
	int fin_len = 0;

	// salvez drumul parcurs
	aux = solved[n][max];
	while (aux) {
		if (aux->val) {
			fin[fin_len++] = aux->val;
		}

		aux = aux->prev;
	}

	// scriu rezultatul
	write_teme(sem[n][max], fin_len, fin);
}

int main(int argc, char const *argv[]) {
	int n;
	tema *teme = read_teme(&n);
	sort(teme, n);
	best_comb(teme, n, teme[n - 1].dline);

	return 0;
}
