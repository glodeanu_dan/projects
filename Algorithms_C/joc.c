#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

// citesc inputul
int *read_imp(int *n, int *k, int *l) {
	FILE *fp;
	size_t len = 0;
	char *line = NULL;
	char *aux, *tmp;
	int i = 0;

    fp = fopen("joc.in", "r");
    if (fp == NULL) {
        return NULL;
    }

    getline(&line, &len, fp);
    aux = strtok_r(line, " \n", &tmp);
    *n = atoi(aux);
    aux = strtok_r(NULL, " \n", &tmp);
    *k = atoi(aux);
    aux = strtok_r(NULL, " \n", &tmp);
    *l = atoi(aux);

    int *v = malloc(sizeof(int) * (*n));
	getline(&line, &len, fp);
	aux = strtok_r(line, " \n", &tmp);
	v[i] = atoi(aux);

    for (i = 1; i < *n; i++) {
		aux = strtok_r(NULL, " \n", &tmp);
		v[i] = atoi(aux);
    }

    fclose(fp);
    if (line) {
        free(line);
    }

    return v;
}

// calculez rezultatul
int comp(int *v, int n, int k, int l, int s, int d, int curr_len,
	int *fin, int pos, int sum, int is_odd) {
	int aux, tmp_sum, tmp_odd, maxx = -1;

	// caz cand ajung la ultima secventa
	if (k == 1) {
		int len = l - k + 1 - curr_len, j;

		// misc inceputul subsecventei
		while (s + len - 1 <= d) {
			aux = pos;
			tmp_sum = sum;
			tmp_odd = is_odd;
			// variez lungimea
			for (j = 0; j < len; j++)  {
				fin[aux++] = v[j + s];
				tmp_sum += v[j + s];

				// actualizez paritatea
				if (v[j + s] % 2 == 1) {
					tmp_odd++;
				}
			}

			// calculez maximul pentru cazul de imparitate
			if (tmp_sum > maxx && tmp_odd % 2 == 1) {
				maxx = tmp_sum;
			}

			s++;
		}

		// returnez valoarea maxima impara
		return maxx;
	}

	int i, j, max_len, next_s, next_d, next_l, curr_s = s, tmp_max = 0;
	max_len = l - k + 1 - curr_len;
	k--;

	// procedez la fel ca mai sus doar ca e schimbat cu locul varotia
	// lungimei cu a inceputului
	for (i = 0; i < max_len; i++) {
		curr_s = s;
		while (curr_s + i <= d) {
			aux = pos;
			tmp_sum = sum;
			tmp_odd = is_odd;
			for (j = 0; j <= i; j++)  {
				fin[aux++] = v[j + curr_s];
				tmp_sum += v[j + curr_s];

				if (v[j + curr_s] % 2 == 1) {
					tmp_odd++;
				}
			}

			next_l = curr_len + i + 1;
			next_s = i + curr_s + 2;
			next_d = n - 2 * k + 1;

			// apelez pentru urmatoare subsecventa
			tmp_max = comp(v, n, k, l, next_s, next_d,
				next_l, fin, aux, tmp_sum, tmp_odd);

			if (tmp_max > maxx) {
				maxx = tmp_max;
			}

			curr_s++;
		}
	}

	return maxx;
}

// scriu rezultatul
void write_imp(int max) {
    int i;
    FILE *fp;

    fp = fopen("joc.out", "w");

    if (fp == NULL) {
      return;
    }

    fprintf(fp, "%d\n", max);

    fclose(fp);
}

void impare() {
	int n, k, l;
	int *v = read_imp(&n, &k, &l);
	int i, j, aux = 0, tmp = 0;
	int s, d, curr_len = 0, len = 0, max;
	s = 0;
	d = n - 2 * k + 1 + s;
	int *fin = malloc(n * sizeof(int));

	max = comp(v, n, k, l, s, d, curr_len, fin, len, 0, 0);
	write_imp(max);
}

int main(int argc, char const *argv[]) {
	impare();
	return 0;
}
