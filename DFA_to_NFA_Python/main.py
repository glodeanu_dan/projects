import sys
from typing import List, Tuple, Set, Dict

EPSILON = "eps"
alias = {}
current_state = 1

class FA:
    def __init__(self, nr: int, alphabet: Set[chr], finalStates: Set[int],
                 delta: Dict[Tuple[int, chr], List[int]]):
        self.nr = nr
        self.states = set(range(self.nr))
        self.alphabet = alphabet
        self.initialState = 0
        self.finalStates = finalStates
        self.delta = delta

def read_nfa(file):
	nr = int(fIn.readline()[:-1])
	fin = set()
	alph = set()
	delta = {}

	for i in fIn.readline()[:-1].split():
		fin.add(int(i))

	line = fIn.readline().rstrip('\n')

	while line:
		aux = line.split()

		if aux[1] != EPSILON:
			alph.add(aux[1])

		my_key = (int(aux[0]), aux[1])
		aux = aux[2:]

		delta[my_key] = [int(a) for a in aux]

		line = fIn.readline().rstrip('\n')

	fa = FA(nr, alph, fin, delta)
	return fa

def epsilonClosure(state, delta):
	mySet = set()
	mySet.add(state)

	n = 0

	while len(mySet) != n:
		n = len(mySet)
		aux = [delta[(s,c)] for (s,c) in delta.keys() if (c == EPSILON) and (s in mySet)]
		for i in aux:
			mySet.update([a for a in i])

	return list(mySet)

def step(states, tran, delta):
        aux = [d for (s,c), d in delta.items() if s in states and c == tran]
        dest = set()

        for i in aux:
            dest.update(i)

        mySet = [s for s in dest]

        for s in mySet:
        	dest.update(epsilonClosure(s, delta))

        return list(dest)

def get_key(states):
	states.sort()
	return tuple(states)

def convert(nfa):
	nr = nfa.nr
	fin = set()
	alph = set()
	delta = {}
	finalStates = set()

	init = epsilonClosure(nfa.initialState, nfa.delta)
	alias[get_key(init)] = 0
	current_state = 1
	visited = set()
	visited.add(get_key(init))

	for i in nfa.finalStates:
		if i in init:
			finalStates.add(alias[get_key(init)])

	q = [init]

	while len(q) != 0:
		state = q[0]
		q.pop(0)

		for c in nfa.alphabet:
			next_state = step(state, c, nfa.delta)


			if next_state and not (get_key(next_state) in visited):
				visited.add(get_key(next_state))
				q.append(next_state)
				alias[get_key(next_state)] = current_state
				current_state += 1
				
				for i in nfa.finalStates:
					if i in next_state:
						finalStates.add(alias[get_key(next_state)])
			
			if next_state:
				delta[(alias[get_key(state)], c)] = alias[get_key(next_state)]



	aux = 1
	for c in nfa.alphabet:
		for s in alias.keys():
			if not (alias[s],c) in delta.keys():
				if aux:
					aux = 0
					
					for i in nfa.alphabet:
						delta[(current_state, i)] = current_state
					current_state += 1

				delta[(alias[s],c)] = current_state - 1


	dfa = FA(current_state, nfa.alphabet, finalStates, delta)
	return dfa

def write_dfa(dfa, f):
	f.write(str(dfa.nr) + '\n')

	for i in dfa.finalStates:
		f.write(str(i))
		f.write(' ')

	f.write('\n')

	for (s, c) in dfa.delta.keys():
		f.write(str(s) + ' ' + c + ' ' + str(dfa.delta[(s, c)]) + '\n')



if __name__ == '__main__':
	fIn = open(sys.argv[1], 'r')
	fOut = open(sys.argv[2], 'w')
	nfa = read_nfa(fIn)
	dfa = convert(nfa)
	write_dfa(dfa, fOut)
	fIn.close();
	fOut.close();
