/*
 * APD - Tema 1
 * Octombrie 2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
 		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })

char *in_filename_julia;
char *in_filename_mandelbrot;
char *out_filename_julia;
char *out_filename_mandelbrot;
int P, widthJ, heightJ, widthM, heightM;
int **resultJ;
int **resultM;
pthread_barrier_t barrier;

// structura pentru un numar complex
typedef struct _complex {
	double a;
	double b;
} complex;

// structura pentru parametrii unei rulari
typedef struct _params {
	int is_julia, iterations;
	double x_min, x_max, y_min, y_max, resolution;
	complex c_julia;
} params;

params parJ, parM;

// citeste argumentele programului
void get_args(int argc, char **argv)
{
	if (argc < 6) {
		printf("Numar insuficient de parametri:\n\t"
				"./tema1_par fisier_intrare_julia fisier_iesire_julia "
				"fisier_intrare_mandelbrot fisier_iesire_mandelbrot P\n");
		exit(1);
	}

	in_filename_julia = argv[1];
	out_filename_julia = argv[2];
	in_filename_mandelbrot = argv[3];
	out_filename_mandelbrot = argv[4];
	P = atoi(argv[5]);
}

// citeste fisierul de intrare
void read_input_file(char *in_filename, params* par)
{
	FILE *file = fopen(in_filename, "r");
	if (file == NULL) {
		printf("Eroare la deschiderea fisierului de intrare!\n");
		exit(1);
	}

	fscanf(file, "%d", &par->is_julia);
	fscanf(file, "%lf %lf %lf %lf",
			&par->x_min, &par->x_max, &par->y_min, &par->y_max);
	fscanf(file, "%lf", &par->resolution);
	fscanf(file, "%d", &par->iterations);

	if (par->is_julia) {
		fscanf(file, "%lf %lf", &par->c_julia.a, &par->c_julia.b);
	}

	fclose(file);
}

// scrie rezultatul in fisierul de iesire
void write_output_file(char *out_filename, int **result, int width, int height)
{
	int i, j;

	FILE *file = fopen(out_filename, "w");
	if (file == NULL) {
		printf("Eroare la deschiderea fisierului de iesire!\n");
		return;
	}

	fprintf(file, "P2\n%d %d\n255\n", width, height);
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			fprintf(file, "%d ", result[i][j]);
		}
		fprintf(file, "\n");
	}

	fclose(file);
}

// aloca memorie pentru rezultat
int **allocate_memory(int width, int height)
{
	int **result;
	int i;

	result = malloc(height * sizeof(int*));
	if (result == NULL) {
		printf("Eroare la malloc!\n");
		exit(1);
	}

	for (i = 0; i < height; i++) {
		result[i] = malloc(width * sizeof(int));
		if (result[i] == NULL) {
			printf("Eroare la malloc!\n");
			exit(1);
		}
	}

	return result;
}

// elibereaza memoria alocata
void free_memory(int **result, int height)
{
	int i;

	for (i = 0; i < height; i++) {
		free(result[i]);
	}
	free(result);
}

void *thread_function(void *arg) {
	int thread_id = *(int *)arg;
	int w, h, i;

	int start = thread_id * widthJ / P;
	int end = min((thread_id + 1) * widthJ / P, widthJ);

	// paralelizare Julia
	for (w = start; w < end; w++) {
		for (h = 0; h < heightJ; h++) {
			int step = 0;
			complex z = { .a = w * parJ.resolution + parJ.x_min,
							.b = h * parJ.resolution + parJ.y_min };

			while (sqrt(pow(z.a, 2.0) + pow(z.b, 2.0)) < 2.0 && step < parJ.iterations) {
				complex z_aux = { .a = z.a, .b = z.b };

				z.a = pow(z_aux.a, 2) - pow(z_aux.b, 2) + parJ.c_julia.a;
				z.b = 2 * z_aux.a * z_aux.b + parJ.c_julia.b;

				step++;
			}

			resultJ[h][w] = step % 256;
		}
	}

	pthread_barrier_wait(&barrier);

	start = thread_id * (heightJ / 2) / P;
	end = min((thread_id + 1) * (heightJ / 2) / P, (heightJ / 2));

	// transforma rezultatul din coordonate matematice in coordonate ecran pentru Julia

	for (i = start; i < end; i++) {
		int *aux = resultJ[i];
		resultJ[i] = resultJ[heightJ - i - 1];
		resultJ[heightJ - i - 1] = aux;
	}

	start = thread_id * widthM / P;
	end = min((thread_id + 1) * widthM / P, widthM);

	// paralelizare Mandelbrot
	for (w = start; w < end; w++) {
		for (h = 0; h < heightM; h++) {
			complex c = { .a = w * parM.resolution + parM.x_min,
							.b = h * parM.resolution + parM.y_min };
			complex z = { .a = 0, .b = 0 };
			int step = 0;

			while (sqrt(pow(z.a, 2.0) + pow(z.b, 2.0)) < 2.0 && step < parM.iterations) {
				complex z_aux = { .a = z.a, .b = z.b };

				z.a = pow(z_aux.a, 2.0) - pow(z_aux.b, 2.0) + c.a;
				z.b = 2.0 * z_aux.a * z_aux.b + c.b;

				step++;
			}

			resultM[h][w] = step % 256;
		}
	}

	pthread_barrier_wait(&barrier);

	start = thread_id * (heightM / 2) / P;
	end = min((thread_id + 1) * (heightM / 2) / P, (heightM / 2));

	// transforma rezultatul din coordonate matematice in coordonate ecran pentru Mandelbrot
	for (i = start; i < end; i++) {
		int *aux = resultM[i];
		resultM[i] = resultM[heightM - i - 1];
		resultM[heightM - i - 1] = aux;
	}

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	// se citesc argumentele programului
	get_args(argc, argv);

	int i;
	pthread_t tid[P];
	int thread_id[P];

	// bariera pentru sincronizare threaduri
	pthread_barrier_init(&barrier, NULL, P);

	read_input_file(in_filename_julia, &parJ);
	widthJ = (parJ.x_max - parJ.x_min) / parJ.resolution;
	heightJ = (parJ.y_max - parJ.y_min) / parJ.resolution;
	resultJ = allocate_memory(widthJ, heightJ);

	read_input_file(in_filename_mandelbrot, &parM);
	widthM = (parM.x_max - parM.x_min) / parM.resolution;
	heightM = (parM.y_max - parM.y_min) / parM.resolution;
	resultM = allocate_memory(widthM, heightM);

	// se creeaza thread-urile
	for (i = 0; i < P; i++) {
		thread_id[i] = i;
		pthread_create(&tid[i], NULL, thread_function, &thread_id[i]);
	}

	// se asteapta thread-urile
	for (i = 0; i < P; i++) {
		pthread_join(tid[i], NULL);
	}


	write_output_file(out_filename_julia, resultJ, widthJ, heightJ);
	free_memory(resultJ, heightJ);

	write_output_file(out_filename_mandelbrot, resultM, widthM, heightM);
	free_memory(resultM, heightM);

	// distrugere bariera
	pthread_barrier_destroy(&barrier);

	return 0;
}
