import sys
import string

def get_delta(pattern):
	alph = string.ascii_uppercase
	delta = [[] for i in range(0, len(pattern) + 1)]
	partial = ''

	for i in range(0, len(pattern) + 1):
		state = pattern[:i]
		delta[i] = [0 for j in range(0, len(alph))]
		for j in range(0, len(alph)):
			if i != len(pattern) and alph[j] == pattern[i]:
				delta[i][j] = (i + 1)
			else:
				for k in range(1, i + 1):
					if state[:k] == (state + alph[j])[i-k+1:i+1]:
						delta[i][j] = k

	return delta

def get_indexes(pattern, text, delta):
	alph = string.ascii_uppercase
	indexes = []
	state = 0

	for c in range(0, len(text)):
		aux = alph.index(text[c])
		state = delta[state][aux]

		if state == len(pattern):
			indexes.append(c - len(pattern) + 1)

	return indexes


if __name__ == '__main__':

	fOut = open(sys.argv[2], 'w')
	fIn = open(sys.argv[1], 'r') 
	pattern = fIn.readline()[:-1]
	text = fIn.readline()[:-1]
	fIn.close()

	delta = get_delta(pattern)
	indexes = get_indexes(pattern, text, delta)

	for idx in indexes:
		fOut.write(str(idx))
		fOut.write(' ')

	fOut.write('\n')
	fOut.close();
